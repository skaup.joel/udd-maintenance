import reducers from "state/reducers";
import { createStore } from "redux";

export const store = createStore(
  reducers, // Reducers
  {}, // Estado inicial
  window.__REDUX_DEVTOOLS_EXTENSION__ && window.__REDUX_DEVTOOLS_EXTENSION__() // middelware
);
