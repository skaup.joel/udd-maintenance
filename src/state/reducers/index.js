import { combineReducers } from "redux";
import sessionReducer from "./sessionReducer";
import generalLayout from "./generalLayout";
import usersAdmin from "./usersAdmin";
import roles from "./roles";
import clienteSucursal from "./clienteSucursal";
import allGerencia from "./allGerencia";
import allArea from "./allArea";
import allUbicacion from "./allUbicacion";
import tipoEquipo from "./TypeEquipo";
import areaGeneral from "./areaGeneral";
import tiposProtocolos from "./TiposProtocolos";
import equipoParaAsignarTarea from "./equipoParaAsignarTarea";
import allProtocolos from "./allProtocolos";
import stateTableFilterAssingWork from "./stateTableFilterAssingWork";
import listarEquiposParaAdministrarlos from "./listarEquiposParaAdministrarlos";
import AllMarcaModelo from "./AllMarcaModelo";
import protocolo from "./protocolo";
import ClienteSelected from "./ClienteSelected";

export default combineReducers({
  sessionReducer,
  ClienteSelected,
  roles,
  generalLayout,
  clienteSucursal,
  allUbicacion,
  tiposProtocolos,
  allGerencia,
  usersAdmin,
  allArea,
  tipoEquipo,
  areaGeneral,
  equipoParaAsignarTarea,
  allProtocolos,
  stateTableFilterAssingWork,
  listarEquiposParaAdministrarlos,
  AllMarcaModelo,
  protocolo,
});
