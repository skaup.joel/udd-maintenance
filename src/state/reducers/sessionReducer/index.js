import { LOADING, ERROR, USER_DATA, CHANGEUSER } from "../../types/sessionType";

const INITIAL_STATE = {
  userData: null,
  isLoading: false,
  error: "",
};

export default (state = INITIAL_STATE, action) => {
  switch (action.type) {
    case USER_DATA:
      return {
        ...state,
        userData: action.payload,
        isLoading: false,
        error: "",
      };

    case LOADING:
      return { ...state, isLoading: true };

    case ERROR:
      return { ...state, error: action.payload, isLoading: false };

    case CHANGEUSER:
      return { ...state, userData: action.payload };

    default:
      return state;
  }
};
