import { SET_TITLE_PAGE } from "../../types/generalLayout";

const INITIAL_STATE = {
  titlePage: "Default Title",
  pathComponent: "Path Componente",
};

export default (state = INITIAL_STATE, action) => {
  switch (action.type) {
    case SET_TITLE_PAGE:
      return {
        ...action.payload,
      };
    default:
      return state;
  }
};
