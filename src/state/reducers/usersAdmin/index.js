import { LOADING, ERROR, USERS_DATA } from "../../types/usersAdmin";

const INITIAL_STATE = {
	data: null,
	isLoading: false,
	error: ''
};

export default (state = INITIAL_STATE, action) => {
	switch (action.type) {
		case USERS_DATA:
			return {
				...state,
				data: action.payload,
				isLoading: false,
				error: ''
			};

		case LOADING:
			return { ...state, isLoading: true };

		case ERROR:
			return { ...state, error: action.payload, isLoading: false };

		default: return state;
	};
};