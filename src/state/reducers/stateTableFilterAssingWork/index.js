import { DATA } from "../../types/stateTableFilterAssingWork";

export default (state = {}, action) => {
	switch (action.type) {
		case DATA:
			return {
				...state,
				...action.payload,
			};
		default: return state;
	};
};