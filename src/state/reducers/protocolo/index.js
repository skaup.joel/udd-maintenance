export const resetTodo = "resetTodo";
export const setInfoGeneral = "setInfoGeneral";
export const setCaps = "setCaps";

const INITIAL_STATE = {
  capitulos: null,
  descripcionProtocolo: null,
  tipoDeProtocolo: null,
  tipeDeEquipo: null,
  cliente: null,
};
export default (state = INITIAL_STATE, action) => {
  switch (action.type) {
    case resetTodo:
      return INITIAL_STATE;
    
    case setInfoGeneral:
      return {...state, ...action.payload};
    
    case setCaps:
      return {...state, capitulos:action.payload};

    default:
      return state;
  }
};

export const addNew = async (dispatch, payload) => {
  dispatch({
    type: resetTodo,
    payload,
  });
};
