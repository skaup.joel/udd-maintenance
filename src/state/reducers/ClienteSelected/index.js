export const setCliente = "clientesselected";

const INITIAL_STATE = null
export default (state = INITIAL_STATE, action) => {
  switch (action.type) {
    case setCliente:
      return action.payload;
    default:
      return state;
  }
};

export const setClient = async (dispatch, payload) => {
  dispatch({
    type: setCliente,
    payload,
  });
};
