import { LOADING, ERROR, DATA } from "../types/clientesucursal";
import { GetClientesSucursales } from "../../api/index";

export const GetClientesSucursalesAction = async (dispatch, datos) => {
  const { userSession } = datos;
  dispatch({
    type: LOADING,
  });
  try {
    let data = await GetClientesSucursales(userSession)
      .then((response) => response.json())
      .then((data) => data);
    if (data.Status === "FAIL") {
      throw new Error("Login Fail");
    }
    dispatch({
      type: DATA,
      payload: data,
    });
  } catch (err) {
    console.log(err.message);
    dispatch({
      type: ERROR,
      payload: "Información de roles no disponible.",
    });
  }
};
