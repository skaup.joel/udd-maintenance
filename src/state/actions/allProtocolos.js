import { LOADING, ERROR, DATA } from "../types/allProtocolos";
import { getAllProtocolos } from "../../api/index";
import { FiltroProtocolo } from "../../api/filtros_api";

export const GetAllProtocolosAction = async (dispatch, datos) => {
  const { userSession, isFilter = false, gerencia = "1,2", area = "10,6", sector = "3,48,56", tipoprotocolo = "1" } = datos;
  dispatch({
    type: LOADING,
  });
  try {
    if (isFilter) {
      let data = await FiltroProtocolo(userSession, { gerencia, area, sector, tipoprotocolo })
        .then((response) => response.json())
        .then((data) => data);
      if (data.Status === "FAIL") {
        throw new Error("Login Fail");
      }
      dispatch({
        type: DATA,
        payload: {
          Status: "OK",
          Data: data.Data.map((item) => {
            return { ...item, Descripcion: item.Protocolo };
          }),
        },
      });
    } else {
      let data = await getAllProtocolos(userSession)
        .then((response) => response.json())
        .then((data) => data);
      if (data.Status === "FAIL") {
        throw new Error("Login Fail");
      }
      dispatch({
        type: DATA,
        payload: { ...data },
      });
    }
  } catch (err) {
    console.log(err.message);
    dispatch({
      type: ERROR,
      payload: "Información de Area no disponible.",
    });
  }
};
