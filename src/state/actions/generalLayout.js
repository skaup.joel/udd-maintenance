import {SET_TITLE_PAGE} from '../types/generalLayout'

export const setTitlePage = async (dispatch, data = 'title dafault setted') => {
  dispatch({
    type: SET_TITLE_PAGE,
    payload: data
  });
  
};