import { LOADING, ERROR, DATA } from "../types/typeEquipo";
import { TipoEquipo } from "../../api/index";

export const TipoEquipoAction = async (dispatch, datos) => {
  const { userSession } = datos;
  dispatch({
    type: LOADING,
  });
  try {
    let data = await TipoEquipo(userSession)
      .then((response) => response.json())
      .then((data) => data);
    if (data.Status === "FAIL") {
      throw new Error("Login Fail");
    }
    dispatch({
      type: DATA,
      payload: {...data},
    });
  } catch (err) {
    console.log(err.message);
    dispatch({
      type: ERROR,
      payload: "Información de Area no disponible.",
    });
  }
};
