import { DATA } from "../types/stateTableFilterAssingWork";

export const setStatTableFilterAssingWork_Action = async (dispatch, payload) => {
  dispatch({
    type: DATA,
    payload,
  });
};
