import { LOADING, ERROR, ROLES } from "../types/rolestype";
import { GetRoles } from "../../api/index";

export const GetRolesAction = async (dispatch, datos) => {
  const { userSession } = datos;
  dispatch({
    type: LOADING,
  });
  try {
    let data = await GetRoles(userSession)
      .then((response) => response.json())
      .then((data) => data);
    if (data.Status === "FAIL") {
      throw new Error("Login Fail");
    }
    dispatch({
      type: ROLES,
      payload: data,
    });
  } catch (err) {
    console.log(err.message);
    dispatch({
      type: ERROR,
      payload: "Información de roles no disponible.",
    });
  }
};
