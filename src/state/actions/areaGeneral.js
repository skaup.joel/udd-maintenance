import { LOADING, ERROR, DATA } from "../types/areaGeneral";
import { GetAreaGeneral } from "../../api/index";
import { FiltroAreaGeneral } from "../../api/filtros_api";

export const GetAreaGeneralAction = async (dispatch, datos) => {
  const { userSession, isFilter = false, gerencia = "1,2" } = datos;
  dispatch({
    type: LOADING,
  });
  try {
    if (isFilter) {
      let data = await FiltroAreaGeneral(userSession, { gerencia })
        .then((response) => response.json())
        .then((data) => data);
      if (data.Status === "FAIL") {
        throw new Error("Login Fail");
      }
      dispatch({
        type: DATA,
        payload: { ...data },
      });
    } else {
      let data = await GetAreaGeneral(userSession)
        .then((response) => response.json())
        .then((data) => data);
      if (data.Status === "FAIL") {
        throw new Error("Login Fail");
      }
      dispatch({
        type: DATA,
        payload: { ...data },
      });
    }
  } catch (err) {
    console.log(err.message);
    dispatch({
      type: ERROR,
      payload: "Información de Area no disponible.",
    });
  }
};
