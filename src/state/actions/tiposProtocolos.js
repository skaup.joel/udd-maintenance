import { LOADING, ERROR, DATA } from "../types/TiposProtocolos";
import { TiposProtocolos } from "../../api/index";

export const TiposProtocolosAction = async (dispatch, datos) => {
  const { userSession } = datos;
  dispatch({
    type: LOADING,
  });
  try {
    let data = await TiposProtocolos(userSession)
      .then((response) => response.json())
      .then((data) => data);
    if (data.Status === "FAIL") {
      throw new Error("Login Fail");
    }
    dispatch({
      type: DATA,
      payload: {...data},
    });
  } catch (err) {
    console.log(err.message);
    dispatch({
      type: ERROR,
      payload: "Información de Area no disponible.",
    });
  }
};
