import { LOADING, ERROR, DATA } from "../types/allGerencia";
import { GetAllGerencia } from "../../api/index";

export const GetAllGerenciaAction = async (dispatch, datos) => {
  const { userSession } = datos;
  dispatch({
    type: LOADING,
  });
  try {
    let data = await GetAllGerencia(userSession)
      .then((response) => response.json())
      .then((data) => data);
    if (data.Status === "FAIL") {
      throw new Error("Login Fail");
    }
    dispatch({
      type: DATA,
      payload: {...data},
    });
  } catch (err) {
    console.log(err.message);
    dispatch({
      type: ERROR,
      payload: "Información de Gerencia no disponible.",
    });
  }
};
