import { LOADING, ERROR, DATA } from "../types/allArea";
import { GetAllArea } from "../../api/index";
import { FiltroArea } from "../../api/filtros_api";

export const GetAllAreaAction = async (dispatch, datos) => {
  const { userSession, isFilter = false, gerencia = "1,2", area = "10,6" } = datos;
  dispatch({
    type: LOADING,
  });
  try {
    if (isFilter) {
      let data = await FiltroArea(userSession, { gerencia, area })
        .then((response) => response.json())
        .then((data) => data);
      if (data.Status === "FAIL") {
        throw new Error("Login Fail");
      }
      dispatch({
        type: DATA,
        payload: { ...data },
      });
    } else {
      let data = await GetAllArea(userSession)
        .then((response) => response.json())
        .then((data) => data);
      if (data.Status === "FAIL") {
        throw new Error("Login Fail");
      }
      dispatch({
        type: DATA,
        payload: { ...data },
      });
    }
  } catch (err) {
    console.log(err.message);
    dispatch({
      type: ERROR,
      payload: "Información de Area no disponible.",
    });
  }
};
