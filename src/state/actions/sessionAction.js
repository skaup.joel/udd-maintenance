import { LOADING, ERROR, USER_DATA } from "../types/sessionType";
import { LOGIN } from "../../api/index";

export const login = async ({dispatch, credentials}) => {
  const { userText, passwordText} = credentials;
  dispatch({
    type: LOADING
  });
  try {
    let userData = await LOGIN({ userText, passwordText })
    .then( response => response.json() )
    .then( data => data)
    if(userData.Status === 'FAIL'){
      throw new Error("Login Fail");
    }
    dispatch({
      type: USER_DATA,
      payload: userData
    });
  } catch (err) {
    console.log(err.message);
    dispatch({
      type: ERROR,
      payload: "Información de usuario no disponible."
    });
  }
};
