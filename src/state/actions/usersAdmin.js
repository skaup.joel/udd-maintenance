import { LOADING, ERROR, USERS_DATA } from "../types/usersAdmin";
import { GetUsers } from "../../api/index";

export const getUsersDbs = async (dispatch, datos) => {
  const { userSession, filters = null } = datos;
  dispatch({
    type: LOADING,
  });
  try {
    let data = await GetUsers(userSession, filters)
      .then((response) => response.json())
      .then((data) => data);
    if (data.Status === "FAIL") {
      throw new Error("Login Fail");
    }
    dispatch({
      type: USERS_DATA,
      payload: data,
    });
  } catch (err) {
    console.log(err.message);
    dispatch({
      type: ERROR,
      payload: "Información de usuario no disponible.",
    });
  }
};
