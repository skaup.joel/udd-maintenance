import { URL_BASE } from "./config";

export const handlerFetch = async url => {
  try {
    const rq = await fetch(url);
    const response = await rq.json();
    return response;
  } catch (error) {
    return error;
  }
};

export const LOGIN = async ({ userText, passwordText }) =>
  fetch(`${URL_BASE}/svc/login.py?login=${userText}&passwd=${passwordText}`);

export const GetUsers = async (userSession, filters = null) => {
  const filter = filters
    ? `&cliente=${filters.cliente}&rol=${filters.rol}`
    : `&cliente=${0}&rol=${0}`;
  return fetch(
    `${URL_BASE}/svc/listar_usuarios.py?login=${userSession}${filter}`
  );
};

export const GetRoles = async userSession => {
  return fetch(`${URL_BASE}/svc/listar_rol.py?login=${userSession}`);
};

export const GetClientesSucursales = async userSession => {
  return fetch(`${URL_BASE}/svc/listar_cliente.py?login=${userSession}`);
};

export const deleteUser = async (userSession, data = {}) => {
  const { id } = data;
  // https://sapma.sercoing.cl/svc/desactiva_usuario.py?login=admin&id=8
  // return fetch(`${URL_BASE}/svc/desactiva_usuario.py?login=${userSession}&id=${id}`);
  const response = await handlerFetch(
    `${URL_BASE}/svc/desactiva_usuario.py?login=${userSession}&id=${id}`
  );
  return response;
};

export const UpdateUser = async (userSession, data = {}) => {
  const { Cliente, Email, Id, Nombre, Password, Rol, Telefono } = data;
  console.log({ Cliente, Email, Id, Nombre, Password, Rol, Telefono });
  //https://sapma.sercoing.cl/svc/graba_usuario.py?id=9&login=rtv&nombre="R. Trigo"&cliente=2&rol=3&telefono="+561234446"&email="rtv@udd.cl"
  let datos = `${URL_BASE}/svc/graba_usuario.py?id=${Id}&login=${Email}&nombre=${Nombre}&cliente=${Cliente}&c=${userSession}&rol=${Rol}&telefono=${Telefono}&email=${Email}`;
  if (Rol === 5) {
    datos = `${datos}&filtro=${data.gerenciaSelected.Id}`;
  }
  if (Rol === 4) {
    datos = `${datos}&filtro=${data.areaSelected.Id}`;
  }
  try {
    const rq = await fetch(datos);
    const response = await rq.json();
    const [idUsearCreated] = response.Data;
    if (!Password) return response;

    const urlChanguePassword = `${URL_BASE}/svc/cambia_clave.py?login=${userSession}&id=${
      idUsearCreated || Id
    }&passwd=${Password}`;
    const rqChanguePassword = await fetch(urlChanguePassword);
    const responseFinal = await rqChanguePassword.json();
    return responseFinal;
  } catch (error) {
    return error;
  }
};

export const PostCreateUser = async (userSession = "admin", datos = {}) => {
  // (((((((((((((((((((((ESTO CREA AL USUARIO)))))))))))))))))))))

  // https://sapma.sercoing.cl/svc/graba_usuario.py?id=0&login=rtv&nombre="Rodrigo Trigo"&cliente=2&rol=3&telefono="+561234444"&email="rtv@sercoing.cl"
  const { id = 0, Nombre, Cliente, Email, Telefono, Rol, Password } = datos;

  let data = `id=${id}&login=${Email}&nombre=${Nombre}&cliente=${Cliente}&c=${userSession}&rol=${Rol}&telefono=${Telefono}&email=${Email}`; //falta EMAIL, TELEFONO, ROL
  if (Rol === 5) {
    data = `${data}&filtro=${datos.gerenciaSelected.Id}`;
  }
  if (Rol === 4) {
    data = `${data}&filtro=${datos.areaSelected.Id}`;
  }
  const url = `${URL_BASE}/svc/graba_usuario.py?${data}`;

  // (((((((((((((((((((((ESTO CAMBIA LA CONTRASEÑA DEL USUARIO CREADO)))))))))))))))))))))
  // https://sapma.sercoing.cl/svc/cambia_clave.py?login=admin&id=8&passwd=admin

  try {
    const rq = await fetch(url);
    const { Data } = await rq.json();
    const [idUsearCreated] = Data;

    const urlChanguePassword = `${URL_BASE}/svc/cambia_clave.py?login=${userSession}&id=${idUsearCreated}&passwd=${Password}`;
    const rqChanguePassword = await fetch(urlChanguePassword);
    const responseFinal = await rqChanguePassword.json();
    return responseFinal;
  } catch (error) {
    return error;
  }
};

export const TiposProtocolos = async userSession => {
  // https://sapma.sercoing.cl/svc/listar_tipoprotocolo.py?login=tecnico1
  return fetch(`${URL_BASE}/svc/listar_tipoprotocolo.py?login=${userSession}`);
};

export const GetAllGerencia = async userSession => {
  // https://sapma.sercoing.cl/svc/listar_gerencia.py?login=admin
  return fetch(`${URL_BASE}/svc/listar_gerencia.py?login=${userSession}`);
};

export const GetAreaGeneral = async userSession => {
  // https://sapma.sercoing.cl/svc/listar_area.py?login=admin
  return fetch(`${URL_BASE}/svc/listar_area.py?login=${userSession}`);
};

export const GetAllArea = async userSession => {
  // https://sapma.sercoing.cl/svc/listar_sector.py?login=admin
  return fetch(`${URL_BASE}/svc/listar_sector.py?login=${userSession}`);
};

export const getAllProtocolos = async (userSession, extra = "") => {
  // https://sapma.sercoing.cl/svc/listar_protocolos.py?login=tecnico1
  return fetch(
    `${URL_BASE}/svc/listar_protocolos.py?login=${userSession}${extra}`
  );
};

export const getAllProtocolos2 = async userSession => {
  // https://sapma.sercoing.cl/svc/listar_tipoequipo.py?login=tecnico1
  return fetch(`${URL_BASE}/svc/listar_tipoequipo.py?login=${userSession}`);
};

export const getAllMarcaModelo = async userSession => {
  //https://sapma.sercoing.cl/svc/listar_mm.py?login=tecnico1
  return fetch(`${URL_BASE}/svc/listar_mm.py?login=${userSession}`);
};

export const TiposDeRespuestas = async userSession => {
  // https://sapma.sercoing.cl/svc/listar_tiporespuesta.py?login=admin
  return fetch(`${URL_BASE}/svc/listar_tiporespuesta.py?login=${userSession}`);
};

export const TipoEquipo = async userSession => {
  // https://sapma.sercoing.cl/svc/listar_tipoequipo.py?login=admin
  return fetch(`${URL_BASE}/svc/listar_tipoequipo.py?login=${userSession}`);
};

export const getEquiposParaAsignarTareas = async (userSession = "admin") => {
  // https://sapma.sercoing.cl/svc/listar_tecnicos.py?login=admin
  return fetch(`${URL_BASE}/svc/listar_tecnicos.py?login=${userSession}`);
};

export const getAllidEquipos = async (userSession = "admin", filters = {}) => {
  const {
    gerencia = "1,2",
    area = "10,6",
    sector = "3",
    tipoprotocolo = "1",
    protocolo = "6",
  } = filters;
  //https://sapma.sercoing.cl/svc/filtro_equipo.py?login=tecnico1&gerencia=1,2&area=10,6&sector=3,48,56&tipoprotocolo=1&protocolo=6
  return fetch(
    `${URL_BASE}/svc/filtro_equipo.py?login=${userSession}&gerencia=${gerencia}&area=${area}&sector=${sector}&tipoprotocolo=${tipoprotocolo}&protocolo=${protocolo}`
  );
};

export const getAllidEquipos2 = async (userSession, filters = {}) => {
  const { sector, protocolo } = filters;
  //https://sapma.sercoing.cl/svc/filtro_equipo.py?login=admin&idCliente=1&sector=34&tipoequipo=1,2,3,4
  return fetch(
    `${URL_BASE}/svc/filtro_equipo.py?login=${userSession}&sector=${sector}&tipoequipo=${protocolo}&este=holaaa2`
  );
};

export const saveTarea = async (userSession, data = {}) => {
  const {
    tecnico = null,
    fecha = null,
    es_contingencia = null,
    equipos = null,
    tipoprotocolo = null,
  } = data;
  // https://sapma.sercoing.cl/svc/grabar_tareas.py?login=admin&tecnico=3&fecha=2020-07-15&es_contingencia=0&equipos=1,2&tipoproto=1
  const response = await handlerFetch(
    // `${URL_BASE}/svc/grabar_tareas.py?tecnico=${tecnico}&fecha=${fecha}&es_contingencia=${es_contingencia}&equipos=${equipos}&protocolos=${protocolos}`
    `${URL_BASE}/svc/grabar_tareas.py?login=${userSession}&tecnico=${tecnico}&fecha=${fecha}&es_contingencia=${es_contingencia}&equipos=${equipos}&tipoproto=${tipoprotocolo}&este=aquiiiiiii`
  );
  return response;
};

export const listarEquiposParaAdministrarlos = async (
  userSession,
  filters = {}
) => {
  const {
    tipo = 0,
    gerencia = 0,
    area = 0,
    sector = 0,
    desde = 0,
    hasta = 50,
  } = filters;
  //https://sapma.sercoing.cl/svc/listar_equipos.py?login=tecnico1&tipo=0&gerencia=0&area=0&sector=0&desde=10&hasta=20
  return fetch(
    `${URL_BASE}/svc/listar_equipos.py?login=${userSession}&tipo=${tipo}&gerencia=${gerencia}&area=${area}&sector=${sector}&desde=${desde}&hasta=${hasta}`
  );
};

export const getOneEquitmentDetail = async (userSession, filters = {}) => {
  const { id } = filters;
  //https://sapma.sercoing.cl/svc/obtener_equipo.py?login=admin&id=1
  return fetch(
    `${URL_BASE}/svc/obtener_equipo.py?login=${userSession}&id=${id}`
  );
};

export const createOrUpdateEquipo = async (datos = {}) => {
  const {
    detalle, // input // NOMBRE
    id = 0, //num // 0 Crea otro actualiza
    codigo, //input text
    nombre, //input text
    serie, //input
    idtipo, //lista select  listar_tipoequi  /L36 TIPO nsita: nada
    idmm, // lista  select ??? listar_mm / L19 MARCA MODELO nsita: nada
    idpi, // id protocolo select inspeccion () Id_Tipo 1 /listar protocolo / L30
    idpm, // id protocolo select mantencion () Id_Tipo  2 / listar protocolo / L30
    idpp = 100000000,
    idg, // id select/ L12 GERENCIA / nsita: nada
    ida, //id area select / L22 AREA / nsita: +gerencia+
    ids, // id     select  / L23 SECTOR / nsita: +gerencia+ +area+
  } = datos;
  //https://sapma.sercoing.cl/svc/graba_equipo.py?id=1&codigo=Chile-GADM-0001&nombre=%22ATAQUE%20RAPIDO%20AGUA%20(H2O)%22&idtipo=1&idmm=30&serie=%22**%22&idg=1&ida=10&ids=48&detalle=%22KM%207%20-%20ENTRE%20BODEGA%20B%20Y%20E%22&idpi=6&idpm=5
  return await handlerFetch(
    `${URL_BASE}/svc/graba_equipo.py?id=${id}&codigo=${codigo}&nombre=${nombre}&idtipo=${idtipo}&idmm=${idmm}&serie=${serie}&idg=${idg}&ida=${ida}&ids=${ids}&detalle=${detalle}&idpi=${idpi}&idpm=${idpm}&idpp=${idpp}`
  );
};

export const GetTareaPorFecha = async (userSession, filters = {}) => {
  const { fecha } = filters;
  //https://sapma.sercoing.cl/svc/lista_tareas_realizadas.py?login=planp&fecha=2020-03-02
  const response = await fetch(
    `${URL_BASE}/svc/lista_tareas_realizadas.py?login=${userSession}&fecha=${fecha}`
  );
  const data = await response.json();
  return data;
};

export const GetTareaPorId = async (userSession, filters = {}) => {
  const { id_tarea } = filters;
  //https://sapma.sercoing.cl/svc/ver_tarea.py?login=tecnico1&tarea=872
  const response = await fetch(
    `${URL_BASE}/svc/ver_tarea.py?login=${userSession}&tarea=${id_tarea}`
  );
  const data = await response.json();
  return data;
};

export const listarClientes = async (userSession, data = {}) => {
  // https://sapma.sercoing.cl/svc/clientes.py?login=admin
  const response = await handlerFetch(
    `${URL_BASE}/svc/clientes.py?login=${userSession}`
  );
  return response;
};

export const borrarClientes = async (userSession, data = {}) => {
  const { id } = data;
  // https://sapma.sercoing.cl/svc/borra_cliente.py?login=admin&id=10000
  const response = await handlerFetch(
    `${URL_BASE}/svc/borra_cliente.py?login=${userSession}&id=${id}`
  );
  return response;
};

export const actualizarCrearCliente = async (userSession, data = {}) => {
  const {
    Id: id = 0,
    Cliente: descripcion,
    RUT: rut,
    Contacto: contacto,
    Telefono: telefono,
    Email: email,
    Direccion: direccion,
  } = data;

  // https://sapma.sercoing.cl/svc/graba_cliente.py?login=admin&id=1&descripcion="ANDINA"&rut="1-9"&contacto="Juan Perez"&telefono="(56)12345"&email="juan.perez@gmail.com"&direccion="La calle no.1"
  const response = await handlerFetch(
    `${URL_BASE}/svc/graba_cliente.py?login=${userSession}&id=${id}&descripcion=${descripcion}&rut=${rut}&contacto=${contacto}&telefono=${telefono}&email=${email}&direccion=${direccion}`
  );
  return response;
};

export const actualizarGerencia_USER = async (userSession, data = {}) => {
  const {
    Id = 0,
    Gerencia,
    Detalle,
    Contacto,
    Telefono,
    Email,
    Referencia,
    Id_Cliente,
  } = data;

  //https://sapma.sercoing.cl/svc/graba_gerencia.py?login=admin&id=0&descripcion=%22GPRUEBA%22&idcliente=2&detalle=%22Gerencia%20de%20Prueba%22&contacto=%22Juan%20Perez%22&telefono=%22(569)%20123-3445%22&email=%22juan.perez@gmail.com%22&referencia=%22No%20se%20donde%22
  const response = await handlerFetch(
    `${URL_BASE}/svc/graba_gerencia.py?login=${userSession}&id=${Id}&descripcion=${Gerencia}&idcliente=${Id_Cliente}&detalle=${Detalle}&contacto=${Contacto}&telefono=${Telefono}&email=${Email}&referencia=${Referencia}`
  );
  // idcliente //NO ENTIENDO
  return response;
};

export const mantenerdorGerencia_USER = async (userSession, data = {}) => {
  const { cliente = 0 } = data;
  // https://sapma.sercoing.cl/svc/gerencias.py?login=admin&cliente=0
  const response = await handlerFetch(
    `${URL_BASE}/svc/gerencias.py?login=${userSession}&cliente=${cliente}`
  );
  return response;
};

export const eliminarGerencia_USER = async (userSession, data = {}) => {
  const { id } = data;

  //https://sapma.sercoing.cl/svc/borra_gerencia.py?login=admin&id=10000
  const response = await handlerFetch(
    `${URL_BASE}/svc/borra_gerencia.py?login=${userSession}&id=${id}`
  );
  return response;
};

export const actualizarArea_USER = async (userSession, data = {}) => {
  const { Id = 0, Area, idgerencia = 1, codigo } = data;

  //https://sapma.sercoing.cl/svc/graba_area.py?login=admin&id=0&descripcion="SANTIAGO"
  const response = await handlerFetch(
    `${URL_BASE}/svc/graba_area.py?login=${userSession}&id=${Id}&descripcion=${Area}&idgerencia=${idgerencia}&codigo=${codigo}`
  );
  // idcliente //NO ENTIENDO
  return response;
};

export const mantenerdorArea_USER = async (userSession, data = {}) => {
  //https://sapma.sercoing.cl/svc/areas.py?login=admin
  const response = await handlerFetch(
    `${URL_BASE}/svc/areas.py?login=${userSession}`
  );
  return response;
};

export const eliminarArea_USER = async (userSession, data = {}) => {
  const { id } = data;
  //https://sapma.sercoing.cl/svc/borra_area.py?login=admin&id=10000
  const response = await handlerFetch(
    `${URL_BASE}/svc/borra_area.py?login=${userSession}&id=${id}`
  );
  return response;
};

export const mantenerdorSectores_USER = async (userSession, data = {}) => {
  //https://sapma.sercoing.cl/svc/sectores.py?login=admin
  const response = await handlerFetch(
    `${URL_BASE}/svc/sectores.py?login=${userSession}`
  );
  return response;
};

export const actualizarSectores_USER = async (userSession, data = {}) => {
  const { Id = 0, Sector, idarea } = data;
  //https://sapma.sercoing.cl/svc/graba_sector.py?login=admin&id=0&descripcion="PARQUE"
  const response = await handlerFetch(
    `${URL_BASE}/svc/graba_sector.py?login=${userSession}&id=${Id}&descripcion=${Sector}&idarea=${idarea}`
  );
  // idcliente //NO ENTIENDO
  return response;
};

export const eliminarSectores_USER = async (userSession, data = {}) => {
  const { id } = data;
  //https://sapma.sercoing.cl/svc/borra_sector.py?login=admin&id=10000
  const response = await handlerFetch(
    `${URL_BASE}/svc/borra_sector.py?login=${userSession}&id=${id}`
  );
  return response;
};

export const detalle_protocolo = async (userSession, data = {}) => {
  const { id } = data;
  //https://sapma.sercoing.cl/svc/detalle_protocolo.py?login=admin&protocolo=1
  const response = await handlerFetch(
    `${URL_BASE}/svc/detalle_protocolo.py?login=${userSession}&protocolo=${id}`
  );
  return response;
};

export const excel_cumpliger = async (userSession, data = {}) => {
  const { gerencias, mes, anno } = data;
  //`https://sapma.sercoing.cl/svc/rep_cumpliger.py?login=admin&gerencias=0&mes=${mesAnno.mes}&anno=${mesAnno.anno}`
  return fetch(
    `${URL_BASE}/svc/rep_cumpliger.py?login=${userSession}&gerencias=${
      gerencias || 0
    }&mes=${mes}&anno=${anno}`
  );
};

export const rep_operatividad = async (userSession, data = {}) => {
  const { fechaFetch } = data;
  //`https://sapma.sercoing.cl/svc/rep_operatividad.py?login=admin&fecha=${fechaFetch}`)
  return fetch(
    `${URL_BASE}/svc/rep_operatividad.py?login=${userSession}&fecha=${fechaFetch}`
  );
};

export const ListarTecnicos2 = async (userSession, data = {}) => {
  //https://sapma.sercoing.cl/svc/listar_tecnicos.py?login=planp
  const response = await handlerFetch(
    `${URL_BASE}/svc/listar_tecnicos.py?login=${userSession}`
  );
  return response;
};

export const listarTareasPorFechasyTecnicos = async (
  userSession,
  data = {}
) => {
  const {
    stateDataSelectedDesde: desde = "2020-02-01",
    stateDataSelectedHasta: hasta = "2020-03-01",
    tecnicos,
  } = data;
  //https://sapma.sercoing.cl/svc/lista_tareas.py?login=planp&desde=2020-02-01&hasta=2020-03-01&tecnicos=3,4
  const response = await handlerFetch(
    `${URL_BASE}/svc/lista_tareas.py?login=${userSession}&desde=${desde}&hasta=${hasta}&tecnicos=${
      tecnicos ? tecnicos : 0
    }`
  );
  return response;
};

export const modificarTarea = async (userSession, data = {}) => {
  const { idTarea, fecha, idTecnicos } = data;
  //https://sapma.sercoing.cl/svc/modificar_tarea.py?login=planp&tarea=4&fecha=2020-02-01&tecnico=3
  const response = await handlerFetch(
    `${URL_BASE}/svc/modificar_tarea.py?login=${userSession}&tarea=${idTarea}&fecha=${fecha}&tecnico=${idTecnicos}`
  );
  return response;
};

export const listar_tipocont = async (userSession, data = {}) => {
  const response = await handlerFetch(
    `${URL_BASE}/svc/listar_tipocont.py?login=${userSession}`
  );
  return response;
};

export const duplicaProtocolo = async (userSession, data = {}) => {
  const { idprotocolo, cliente } = data;
  //https://sapma.sercoing.cl/svc/duplica_protocolo.py?login=admin&idprotocolo=1&cliente=15
  const response = await handlerFetch(
    `${URL_BASE}/svc/duplica_protocolo.py?login=${userSession}&idprotocolo=${idprotocolo}&cliente=${cliente}`
  );
  return response;
};

export const callCrearProtocolo = async (userSession, data = {}) => {
  const {
    id,
    tipoprotocolo,
    descripcion,
    tipoequipo,
    cliente,
    ncapitulos,
    desccapitulo,
    capitulo_esvarios,
    ncapturas,
    desccaptura,
    capitulocaptura,
    respuestacaptura,
  } = data;
  //https://sapma.sercoing.cl/svc/graba_protocolo.py?login=admin&id=0&tipoprotocolo=1&descripcion="Protocolo de Prueba"&tipoequipo=1&cliente=1&ncapitulos=3&desccapitulo="SIN TITULO|Detalle Equipos|Resumen Final"&capitulo_esvarios="0|0|0"&ncapturas=7&desccaptura="Fecha|Tecnico|Descripcion Equipo|Estado Valvula|Estado Espuma|Litros Espuma|Observaciones"&capitulocaptura="1|1|2|2|2|2|3"&respuestacaptura="2|1|1|5|11|6|2"
  // const response = await handlerFetch(
  //   `${URL_BASE}/svc/graba_protocolo.py?login=${userSession}&id=${id}&tipoprotocolo=${tipoprotocolo}&descripcion=${descripcion}&tipoequipo=${tipoequipo}&cliente=${cliente}&ncapitulos=${ncapitulos}&desccapitulo=${desccapitulo}&capitulo_esvarios=${capitulo_esvarios}&ncapturas=${ncapturas}&desccaptura=${desccaptura}&capitulocaptura=${capitulocaptura}&respuestacaptura=${respuestacaptura}`
  // );

  var url = `${URL_BASE}/svc/graba_protocolo.py`;
  // var url = `${URL_BASE}/svc/graba_protocolo.py?login=${userSession}&id=${id}&tipoprotocolo=${tipoprotocolo}&descripcion=${descripcion}&tipoequipo=${tipoequipo}&cliente=${cliente}&ncapitulos=${ncapitulos}&desccapitulo=${desccapitulo}&capitulo_esvarios=${capitulo_esvarios}&ncapturas=${ncapturas}&desccaptura=${desccaptura}&capitulocaptura=${capitulocaptura}&respuestacaptura=${respuestacaptura}`;

  function utf8_to_b64() {
    return window.btoa(
      unescape(
        encodeURIComponent(
          JSON.stringify({
            login: userSession,
            id,
            tipoprotocolo,
            descripcion,
            tipoequipo,
            cliente,
            ncapitulos,
            desccapitulo,
            capitulo_esvarios,
            ncapturas,
            desccaptura,
            capitulocaptura,
            respuestacaptura,
          })
        )
      )
    );
  }

  return fetch(url, {
    method: "POST", // or 'PUT'
    body: utf8_to_b64(), // data can be `string` or {object}!
    // headers: {
    //   "Content-Type": "application/json",
    //   "Referer": "application/json",
    //   "Content-Type": "application/json"
    // },
  })

};
