import { URL_BASE } from "./config";
import { handlerFetch } from "./index";
export const FiltroAreaGeneral = async (userSession, filters) => {
  const { gerencia } = filters;
  //https://sapma.sercoing.cl/svc/filtrar_area.py?login=tecnico1&gerencia=1,2
  return handlerFetch(`${URL_BASE}/svc/filtrar_area.py?login=${userSession}&gerencia=${gerencia}`);
};

export const FiltroArea = async (userSession, filters) => {
  const { gerencia, area } = filters;
  //https://sapma.sercoing.cl/svc/filtrar_sector.py?login=admin&gerencia=1,2&area=10,6
  return handlerFetch(`${URL_BASE}/svc/filtrar_sector.py?login=${userSession}&gerencia=${gerencia}&area=${area}`);
};

export const FiltroProtocolo = async (userSession, filters) => {
  const { gerencia, area, sector, tipoprotocolo } = filters;
  //https://sapma.sercoing.cl/svc/filtrar_protocolo.py?login=tecnico1&gerencia=1,2&area=10,6&sector=3,48,56&tipoprotocolo=1
  return handlerFetch(`${URL_BASE}/svc/filtrar_protocolo.py?login=${userSession}&gerencia=${gerencia}&area=${area}&sector=${sector}&tipoprotocolo=${tipoprotocolo}`);
};

export const listarTipoDeEquipo = async (userSession, filters) => { // este es el ultimo endpoint para filtrar equipos "ULTIMOS CAMBIOS"
  const { sector, } = filters;
  //https://sapma.sercoing.cl/svc/filtrar_tipoequipo.py?login=tecnico1&sector=3,48,56
  return handlerFetch(`${URL_BASE}/svc/filtrar_tipoequipo.py?login=${userSession}&sector=${sector}`);
};

export const ResultadoFiltroPlanificacion = async (userSession, filters) => {
  const { gerencia, area, sector, tipoprotocolo, protocolo } = filters;
  //https://sapma.sercoing.cl/svc/filtro_equipo.py?login=tecnico1&gerencia=1,2&area=10,6&sector=3,48,56&tipoprotocolo=1&protocolo=6
  return handlerFetch(
    `${URL_BASE}/svc/filtrar_protocolo.py?login=${userSession}&gerencia=${gerencia}&area=${area}&sector=${sector}&tipoprotocolo=${tipoprotocolo}&protocolo=${protocolo}&este=holaaa`
  );
};
