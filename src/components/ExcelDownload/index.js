import React from "react";
import ReactExport from "react-export-excel";

const ExcelFile = ReactExport.ExcelFile;
const ExcelSheet = ReactExport.ExcelFile.ExcelSheet;
const ExcelColumn = ReactExport.ExcelFile.ExcelColumn;

export const ExcelDownload = (props) => {
  props = { ...mockData, ...props };
  const { rows, headers, TextButton } = props;
  return (
    <ExcelFile element={<button className='btn btn-success'>{TextButton}</button>}>
      <ExcelSheet data={rows} name="Leaves">
        {headers.map(({ label, value }) => (
          <ExcelColumn label={label} value={value} />
        ))}
      </ExcelSheet>
    </ExcelFile>
  );
};

const mockData = {
  TextButton: "Descarga Excel",
  headers: [
    { label: "Name", value: "name" },
    { label: "Total Leaves", value: "total" },
    { label: "Remaining Leaves", value: "r" },
  ],
  rows: [
    {
      name: "Johnson",
      total: 25,
      r: 16,
    },
    {
      name: "Josef",
      total: 25,
      r: 7,
    },
    {
      name: "Johnson",
      total: 25,
      r: 16,
    },
    {
      name: "Josef",
      total: 25,
      r: 7,
    },
    {
      name: "Johnson",
      total: 25,
      r: 16,
    },
    {
      name: "Josef",
      total: 25,
      r: 7,
    },
  ],
};
