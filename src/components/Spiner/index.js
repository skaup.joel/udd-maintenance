import React from "react";
import CircularProgress from "@material-ui/core/CircularProgress";

export const Spiner = () => {
  return <CircularProgress style={{ position: "absolute", right: "50%", bottom: "50%" }} />;
};
