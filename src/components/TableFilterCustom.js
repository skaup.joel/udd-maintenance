import React from "react";
import styled from "styled-components";

const CheckboxesGroupStyled = styled.div`
  width: 17em;
  height: 40em;
  overflow: auto;
  display: flex;
  flex-direction: column;
  border: solid white 1px;
  padding: 0em 2em;
  .TableFulterCustom-wrapper-Label {
    display: flex;
    flex-direction: column;
  }
  .TableFulterCustom-wrapper-One-Checkbox {
    display: flex;
    width: inherit;
    flex-direction: row-reverse;
    align-items: baseline;
    justify-content: flex-end;
  }
`;

export default function CheckboxesGroup(props) {
  const getData = ({ data }, name) =>
    data.Data.map((item) => {
      return { ...item, label: item[name] || name, isChecked: false };
    });

  const dataProps = { ...Mocks, ...props };
  const { data, title, fn, nameTable } = dataProps;
  const [state, setState] = React.useState(getData(data, nameTable));

  const handleChange = (oneObject) => {
    oneObject.isChecked = !oneObject.isChecked;
    const newState = state.map((item) => {
      if (item.Id === oneObject.Id) return oneObject;
      return item;
    });
    setState(newState);
  };
  React.useEffect(() => {
    setState(getData(data, nameTable));
  // eslint-disable-next-line react-hooks/exhaustive-deps
  }, [props.data]);

  React.useEffect(() => {
    fn({
      [nameTable]: state.filter((item) => item.isChecked && item),
    });
  // eslint-disable-next-line react-hooks/exhaustive-deps
  }, [nameTable, state]);

  return (
    <CheckboxesGroupStyled className="TableFulterCustom-container-component">
      <div>{title}</div>
      <div className="TableFulterCustom-wrapper-Label">
        {state.map((item, i) => (
          <div key={`${item.Id}${i}`} className="TableFulterCustom-wrapper-One-Checkbox">
            <label htmlFor={`${item.Id}${item.label}`}>
              {item.label}
            </label>
            <input
              defaultChecked={item.isChecked}
              onChange={(e) => handleChange(JSON.parse(e.target.value))}
              type="checkbox"
              value={JSON.stringify(item)}
              id={`${item.Id}${item.label}`}
            />
          </div>
        ))}
      </div>
    </CheckboxesGroupStyled>
  );
}

const Mocks = {
  title: "Default-Title",
  isDisabled: false,
  data: { data: { Data: [{ label: "joelasdasdasd" }, { label: "abrahasdasda" }, { label: "wilo" }, { label: "vale" }, { label: "iasdasdasdsi" }] } },
  fn: () => null,
  nameTable: "default-nameTable",
};
