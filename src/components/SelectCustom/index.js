import InputLabel from "@material-ui/core/InputLabel";
import FormControl from "@material-ui/core/FormControl";
import Select from "@material-ui/core/Select";
import React, { useState, useEffect } from "react";
import MenuItem from "@material-ui/core/MenuItem";
import { makeStyles } from "@material-ui/core/styles";

export const SelectCustom = (props) => {
  const data = { ...Mocks, ...props };
  const [state, setstate] = useState(props.defaultValue || {});
  const classes = useStyles();
  useEffect(() => {
    data.handleChange(state);
    // eslint-disable-next-line react-hooks/exhaustive-deps
  }, [state]);
  return (
    <FormControl uncontrolled="true" variant="outlined" className={classes.formControl} style={{ ...data.style }}>
      <InputLabel id={`${data.label}`}>{`${data.label}`}</InputLabel>
      <Select
        labelId={`${data.label}`}
        id={`${data.label}${Math.random()}`}
        value={state[data.objKey] || ""}
        onChange={(e) => {
          setstate(e.target.value);
        }}
        label={`${data.label}`}
      >
        <MenuItem value={state[data.objKey] || ""}>
          <em>{state[data.objKey] || "None"}</em>
        </MenuItem>
        {data.options.map((item) => {
          return (
            <MenuItem key={`${Math.random()}${Math.random()}`} value={item}>
              {item[data.objKey]}
            </MenuItem>
          );
        })}
      </Select>
    </FormControl>
  );
};

const Mocks = {
  style: {},
  value: "default-value",
  handleChange: () => null,
  label: "default-label",
  objKey: "randomData",
  options: [
    {
      id: 1,
      randomData: "default-data1",
    },
    {
      id: 2,
      randomData: "default-data2",
    },
  ],
};

const useStyles = makeStyles((theme) => ({
  formControl: {
    margin: theme.spacing(1),
    minWidth: 120,
  },
  selectEmpty: {
    marginTop: theme.spacing(2),
  },
}));
