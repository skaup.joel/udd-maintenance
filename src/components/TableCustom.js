import React from "react";
import { makeStyles } from "@material-ui/core/styles";
import Table from "@material-ui/core/Table";
import TableBody from "@material-ui/core/TableBody";
import TableCell from "@material-ui/core/TableCell";
import TableContainer from "@material-ui/core/TableContainer";
import TableHead from "@material-ui/core/TableHead";
import TableRow from "@material-ui/core/TableRow";
import Paper from "@material-ui/core/Paper";
import Button from "@material-ui/core/Button";

const useStyles = makeStyles({
  table: {
    minWidth: 650,
  },
});

const TableComponent = (props) => {
  props = { ...mock, ...props };
  const classes = useStyles();

  return (
    <TableContainer component={Paper}>
      <Table className={classes.table} aria-label="simple table">
        <TableHead>
          <TableRow>
            {props.headers.map((header) => (
              <TableCell>{header}</TableCell>
            ))}
            <TableCell>{"Accion"}</TableCell>
          </TableRow>
        </TableHead>
        <TableBody>
          {props.data.map((obj, i) => (
            <TableRow key={props.headers[i]}>
              {props.headers.map((item, i) => (
                <TableCell>{obj[item] || "No-Data"}</TableCell>
              ))}
              <TableCell>
                <Button onClick={() => props.fnEditar(obj)} variant="outlined">
                  {props.textToButton}
                </Button>
              </TableCell>
            </TableRow>
          ))}
        </TableBody>
      </Table>
    </TableContainer>
  );
};

const mock = {
  style: {},
  fnEditar: (datos) => alert(JSON.stringify(datos)),
  headers: ["example1", "example2", "example3", "example4"],
  data: [
    {
      example1: "th example1",
      example2: "th example2",
      example3: "th example3",
      example4: "th example4",
    },
    {
      example1: "th example1",
      example2: "th example2",
      example3: "th example3",
      example4: "th example4",
    },
  ],
  textToButton: "Editar",
};

export { TableComponent as Table };
