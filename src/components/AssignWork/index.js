import React, { useState, useEffect } from "react";
import { TiposProtocolos, GetAllGerencia, GetAreaGeneral, GetAllArea, getAllProtocolos2 } from "api";
import { FiltroAreaGeneral, FiltroArea, listarTipoDeEquipo, ResultadoFiltroPlanificacion } from "api/filtros_api";
import { useSelector } from "react-redux";
import SelectRatio from "components/SelectRatio";
import styled from "styled-components";
import { SetWorkModal } from "./SetWorkModal";
const Container = styled.div`
  display: flex;
`;

const StateColumnsInitialState = [
  /*0*/ { title: "Tipo de servicio", key: "TipoProtocolo", value: null, desableAll: false, filterFunction: () => null, optionsFiltraded: null },
  /*1*/ { title: "Gerencia", key: "Gerencia", value: null, desableAll: true, filterFunction: FiltroAreaGeneral, optionsFiltraded: null },
  /*2*/ { title: "Area general", key: "Area", value: null, desableAll: true, filterFunction: FiltroArea, optionsFiltraded: null },
  /*3*/ { title: "Area", key: "Sector", value: null, desableAll: true, filterFunction: listarTipoDeEquipo, optionsFiltraded: null },
  /*4*/ { title: "Tipo de Equipo", key: "TipoEquipo", value: null, desableAll: true, filterFunction: ResultadoFiltroPlanificacion, optionsFiltraded: null },
  /*4*/ { title: "Tipo de otro protocolo2", key: "TipoEquipo", value: null, desableAll: true, filterFunction: ResultadoFiltroPlanificacion, optionsFiltraded: null },

];
const initialStateFilters = { tipoprotocolo: null, gerencia: null, area: null, sector: null, protocolo: null };

export const AssignWork = () => {
  const [stateColumns, setStateColumns] = useState(StateColumnsInitialState);
  const [Filters, setFilters] = useState(initialStateFilters);
  const [Arrays, setArrays] = useState({
    isLoading: true,
    data: null,
    error: null,
  });

  const userSession = useSelector((state) => state.sessionReducer.userData.User);

  const handlerResetData = () => {
    setStateColumns(StateColumnsInitialState);
    setFilters(initialStateFilters);
  };

  const handlerChange = async (data, i) => {
    //Cambia el estado de los filtros a medida que se seleeciona opcion de la columna
    let keyFilter = Object.keys(Filters)[i];
    let newFilters = { ...Filters, [keyFilter]: data };
    setFilters(newFilters);
    //FIN de => Cambia el estado de los filtros a medida que se seleeciona opcion de la columna

    if (i !== 5) {
      //Llamada a filtro y seteo de nuevo array de opciones
      const response = await stateColumns[i].filterFunction(userSession, newFilters);
      const options = response
        ? response.Data.map((option) => {
            const value = `${option.Id}`;
            const label = option[stateColumns[i + 1].key] ? option[stateColumns[i + 1].key] : option["Protocolo"];
            return { value, label, disabled: stateColumns[i].desableAll };
          })
        : null;
      //FIN de =>Llamada a filtro y seteo de nuevo array de opciones

      // Desactiva columna seleccionada y seta Valor de la columna
      let newstateColumns = stateColumns.map((item, index) => {
        if (i === index) return { ...item, value: data, desableAll: true };
        if (i + 1 === index) return { ...item, desableAll: false, optionsFiltraded: options };
        return item;
      });
      setStateColumns(newstateColumns);
      //FIN de => Desactiva columna seleccionada y seta Valor de la columna
    }
  };

  useEffect(() => {
    let llamadasIniciales = [TiposProtocolos, GetAllGerencia, GetAreaGeneral, GetAllArea, getAllProtocolos2];
    let promesas = llamadasIniciales.map(async (promesa) => {
      let response = await promesa(userSession);
      return response.json();
    });
    Promise.all(promesas)
      .then((arraysData) => setArrays({ error: null, isLoading: false, data: arraysData.map((item) => item.Data) }))
      .catch((error) => setArrays({ isLoading: false, error, data: null }));
  }, [userSession]);

  if (Arrays.isLoading) return <h1>Cargando</h1>;
  if (Arrays.error) return <h1>Error con el servidor {Arrays.error}</h1>;
  return (
    <>
      <div>
        <button className={'btn btn-primary mb-2'} onClick={handlerResetData}>Reset Selects</button>
      </div>
      <Container>
        {Arrays.data.map((options, i) => {
          options = options.map((option) => {
            const value = `${option.Id}`;
            const label = option[stateColumns[i].key];
            return { value, label, disabled: stateColumns[i].desableAll };
          });
          return (
            <SelectRatio
              selectOneAndDesabled={stateColumns[i].desableAll}
              defaultSelected={stateColumns[i].value}
              title={stateColumns[i].title}
              handlerChange={(data) => handlerChange(data, i)}
              options={stateColumns[i].optionsFiltraded || options}
            />
          );
        })}
      </Container>
      <div
        style={{
          position: "absolute",
          bottom: "1em",
        }}
      >
        {Filters.protocolo && <SetWorkModal Filters={Filters} />}
      </div>
    </>
  );
};
