import React, { useState, useEffect } from "react";
import { Modal } from "../Modal";
import { useSelector } from "react-redux";
import { useFetch } from "./../../hooks/callApi";
import { listar_tipocont } from "api";
import { ClienteC, ClienteB } from "routes/allRoles";
import {
  getAllidEquipos2,
  getEquiposParaAsignarTareas as getTecnicos,
  saveTarea,
} from "../../api";
import ReactSelect from "../ReactSelect";
import { DatePicker } from "../DatePicker";
import { useHistory } from "react-router-dom";
let dateNow = new Date();
dateNow = `${dateNow.getFullYear()}-${
  dateNow.getMonth() + 1
}-${dateNow.getDate()}`;

const TransformFecha = (fecha) => {
  let dateNow = new Date(fecha);
  dateNow = `${dateNow.getFullYear()}-${
    dateNow.getMonth() + 1
  }-${dateNow.getDate()}`;
  return dateNow;
};

export const SetWorkModal = (props) => {
  let history = useHistory();
  const userSession = useSelector(
    (state) => state.sessionReducer.userData.User
  );
  const allDataUserSession = useSelector(
    (state) => state.sessionReducer.userData
  );
  const Perfil = useSelector((state) => state.sessionReducer.userData.Perfil);
  // const { TipoProtocolo = [], Gerencia = [], Area = [], Sector = [], Descripcion: equipos = [] } = useSelector((state) => state.stateTableFilterAssingWork);
  const {
    tipoprotocolo: TipoProtocolo,
    gerencia: Gerencia,
    area: Area,
    sector: Sector,
    protocolo: equipos,
  } = props.Filters;

  const [sellectAllTecnicos, setSellectAllTecnicos] = useState(false);
  const [stateTegnico, setstateTecnico] = useState(null);
  const [stateEsContingencia, setstateEsContingencia] = useState(false);
  const [tipoDeContingencia, settipoDeContingencia] = useState(1);
  const [stateDesde, setstateDesde] = useState([dateNow]);
  const [protocolos, setstateProtocolo] = useState(null);
  const [tipoContigencia, setTipoContigencia] = useState(null);

  useEffect(() => {
    setstateDesde([dateNow]);
  }, [
    sellectAllTecnicos,
    stateTegnico,
    stateEsContingencia,
    tipoDeContingencia,
    protocolos,
    tipoContigencia,
  ]);
  const handlerDeleteFecha = () => {
    setstateDesde([dateNow]);
  };
  useEffect(() => {
    listar_tipocont(userSession).then((res) => setTipoContigencia(res.Data));
    // eslint-disable-next-line react-hooks/exhaustive-deps
  }, []);

  const [dataAllEquipos, loadingDataAllEquipos] = useFetch(() =>
    getAllidEquipos2(userSession, {
      gerencia: `${Gerencia}`,
      area: `${Area}`,
      sector: `${Sector}`,
      tipoprotocolo: `${TipoProtocolo}`,
      protocolo: `${equipos}`,
    })
  );
  const [dataTecnicos, loadingDataTecnicos] = useFetch(() =>
    getTecnicos(userSession)
  );
  const handlerCheckBoxSellectAllTecnicos = (e) => {
    setSellectAllTecnicos(!sellectAllTecnicos);
  };

  const fnParaAsignar = async () => {
    let todoBn = false;
    const AllEquipos = dataAllEquipos.Data;
    // const { Status = "fail" } = await saveTarea({
    const allPromises = stateDesde.map(async (fecha) => {
      const res = await saveTarea(userSession, {
        tipoprotocolo: `${TipoProtocolo}`,
        tecnico: `${[stateTegnico].map((i) => i.Id)}`,
        fecha: TransformFecha(fecha),
        es_contingencia: `${stateEsContingencia ? tipoDeContingencia : 0}`,
        protocolos: sellectAllTecnicos
          ? `${AllEquipos.map((i) => i.Id_Protocolo)}`
          : `${protocolos.map((i) => i.Id_Protocolo)}`,
        equipos: sellectAllTecnicos
          ? `${AllEquipos.map((i) => i.Id)}`
          : `${protocolos.map((i) => i.Id)}`,
      });
      return res;
    });
    Promise.all(allPromises)
      .then((data) => {
        console.log(data);

        todoBn = data;
      })
      .catch((err) => {
        console.log({ err });
        todoBn = false;
      })
      .finally(() => {
        history.push({
          pathname: "/work",
          state: {
            isOpen: true,
            type: todoBn ? "success" : "error",
            message: todoBn
              ? `Tarea asignada con exito Id(s): ${todoBn.map(({ Data }) => {
                  return Data;
                })}`
              : "Tarea no asignada intente mas tarde",
          },
        });
      });
  };
  const fnParaEnviarMail = async () => {
    const AllEquipos = dataAllEquipos.Data;
    const fechasTransformadas = stateDesde.map((fecha) =>
      TransformFecha(fecha)
    );
    console.log({
      AllEquipos,
      userSession,
      stateDesde,
      allDataUserSession,
      TipoProtocolo,
      protocolos,
    });
    fetch(
      `https://sapma.sercoing.cl/svc/grabar_tareas_email.py?login=${userSession}&idCliente=${
        allDataUserSession.Id_Cliente
      }&fecha=${fechasTransformadas}&equipos=${protocolos.map(
        ({ Id }) => Id
      )}&tipoproto=${TipoProtocolo}`
    )
      .then(() => {
        history.push({
          pathname: "/work",
          state: {
            isOpen: true,
            type: "success",
            message: "Correo enviado con exito",
          },
        });
      })

      .catch((err) => {
        history.push({
          pathname: "/work",
          state: {
            isOpen: true,
            type: "error",
            message: "Envio fallido",
          },
        });
      });
  };
  return (
    <div>
      <Modal
        className="btn btn-primary"
        textButton="Determinar equipo, Fecha y Grupo Tecnico"
        ModalTitle="Asignar Tarea"
        TextToDoBtn="Asignar Tarea"
        fn={
          ClienteC !== Perfil && ClienteB !== Perfil
            ? fnParaAsignar
            : fnParaEnviarMail
        }
      >
        {!loadingDataAllEquipos && !loadingDataTecnicos && (
          <div
            id={"asignarTareas"}
            style={{ maxHeight: "20em", overflow: "auto", minHeight: "70vh" }}
          >
            {!sellectAllTecnicos && (
              <ReactSelect
                isMulti
                fn={(datos) => setstateProtocolo(datos)}
                options={dataAllEquipos.Data.map((item) => {
                  return { ...item, label: item.Equipo, value: item.Equipo };
                })}
                defaultOptions={dataAllEquipos.Data.map((item) => {
                  return { ...item, label: item.Equipo, value: item.Equipo };
                })}
                placeholder={"Selecciona Equipo(s)"}
              />
            )}
            <label>
              <input
                defaultChecked={sellectAllTecnicos}
                onChange={handlerCheckBoxSellectAllTecnicos}
                type="checkbox"
                id="cbox1"
                value="first_checkbox"
              />
              Seleccionar todos los Equipos
            </label>
            <br />
            {ClienteC !== Perfil && ClienteB !== Perfil && (
              <React.Fragment>
                <span>Es Contingencia:</span>
                <div
                  style={{
                    display: "flex",
                    margin: "1em 0em",
                    alignItems: "baseline",
                  }}
                >
                  <input
                    onChange={(e) => {
                      setstateEsContingencia(!stateEsContingencia);
                    }}
                    type="radio"
                    id="male"
                    checked={stateEsContingencia}
                    value={1}
                    name="isContingencia"
                  />
                  <label htmlFor="male">Si</label>
                  <br />
                  <input
                    onChange={(e) => {
                      setstateEsContingencia(!stateEsContingencia);
                    }}
                    type="radio"
                    id="female"
                    checked={!stateEsContingencia}
                    value={0}
                    name="isContingencia"
                  />
                  <label htmlFor="female">No</label>
                </div>
              </React.Fragment>
            )}
            {tipoContigencia && stateEsContingencia && (
              <ReactSelect
                fn={(datos) => settipoDeContingencia(datos.value)}
                options={tipoContigencia.map((item) => {
                  return { ...item, label: item.Tipo, value: item.Id };
                })}
                defaultOptions={tipoContigencia.map((item) => {
                  return { ...item, label: item.Tipo, value: item.Id };
                })}
                placeholder={"Selecciona el Tipo de contingencia"}
              />
            )}

            {ClienteC !== Perfil && ClienteB !== Perfil && (
              <ReactSelect
                fn={(datos) => setstateTecnico(datos)}
                options={dataTecnicos.Data.map((item) => {
                  return { ...item, label: item.Nombre, value: item.Nombre };
                })}
                defaultOptions={dataTecnicos.Data.map((item) => {
                  return { ...item, label: item.Nombre, value: item.Nombre };
                })}
                placeholder={"Selecciona Grupo Tecnico"}
              />
            )}
            {
              <div
                style={{
                  margin: "1em 0em",
                  display: "flex",
                  alignItems: "flex-start",
                  flexDirection: "column",
                }}
              >
                <span style={{ width: "5em" }}>Fecha:</span>
                {stateDesde.length !== 1 && (
                  <button onClick={() => handlerDeleteFecha()}>
                    Eliminar fechas
                  </button>
                )}
                {stateDesde.map((item, index) => {
                  return (
                    <div>
                      <div key={`fetcha-${item}-${index}`}>
                        <DatePicker
                          defaultData={item}
                          fn={(data) => {
                            let prevState = stateDesde;
                            prevState[index] = data;
                            setstateDesde(prevState);
                          }}
                        />
                      </div>
                    </div>
                  );
                })}
                <button
                  className="btn btn-warning"
                  onClick={() => {
                    setstateDesde([
                      ...stateDesde,
                      stateDesde[stateDesde.length - 1],
                    ]);
                  }}
                >
                  Añadir fecha
                </button>
              </div>
            }
          </div>
        )}
      </Modal>
    </div>
  );
};
