import React, { useState, useEffect } from "react";
import { useDispatch, useSelector } from "react-redux";
import { ButtonCustom } from "./../ButtonCustom";
import { InputCustom } from "../InputCustom";
import { SelectCustom } from "../SelectCustom";
import { FormContainer } from "./styled";
import * as actionsRoles from "../../state/actions/roles";
import * as actionsClientesSucursales from "../../state/actions/clientesSucursal";
import { PostCreateUser, deleteUser, UpdateUser } from "../../api/index";
import { withRouter } from "react-router";
import { Switches } from "../Switch";
import { Spiner } from "./../../components/Spiner";
import Othercomponent from "screens/UsersActions/Othercomponent";
import { ClienteA, ClienteB } from "routes/allRoles";
export const UserFormCreate = withRouter(props => {
  const [showInputPassword, setshowInputPassword] = useState(
    props.type === "UPDATE" ? false : true
  );
  const { title = "Default-title" } = props;
  const [formCreateUser, setInputsFormCreateUser] = useState(
    props.location.state ? props.location.state : {}
  );
  const setDataInput = (key, value) => {
    setInputsFormCreateUser({ ...formCreateUser, [`${key}`]: value });
  };
  useEffect(() => {
    setDataInput("Password", "");
    // eslint-disable-next-line react-hooks/exhaustive-deps
  }, [showInputPassword]);
  const dispatch = useDispatch();
  const userSession = useSelector(state => state.sessionReducer.userData.User);
  const stateRequesRoles = useSelector(state => state.roles);
  const stateClienteSucursal = useSelector(state => state.clienteSucursal);
  const { data: dataRoles, isLoading, error } = stateRequesRoles;
  const {
    data: dataClientesSucursales,
    isLoading: isLoadingClientesSucursales,
    error: errorClientesSucursales,
  } = stateClienteSucursal;
  useEffect(() => {
    actionsRoles.GetRolesAction(dispatch, { userSession });
    actionsClientesSucursales.GetClientesSucursalesAction(dispatch, {
      userSession,
    });
  }, [dispatch, userSession]);

  const format = form => {
    form.Rol =
      typeof form.Rol === "number"
        ? form.Rol
        : dataRoles.Data.filter(item => form.Rol === item.Rol)[0].Id;
    // form.Cliente =
    //   typeof form.Cliente === "number"
    //     ? form.Cliente
    //     : dataClientesSucursales.Data.filter(
    //         item => form.Cliente === item.Cliente
    //       )[0].Id;

    return form;
  };

  if (isLoading && isLoadingClientesSucursales) return <Spiner />;
  if (error && errorClientesSucursales) return <h1>tenermos un error</h1>;
  return (
    <FormContainer>
      {dataRoles && dataClientesSucursales && (
        <React.Fragment>
          <h1>{title}</h1>
          <div>
            <InputCustom
              onChange={e => {
                setDataInput("Nombre", e.target.value);
              }}
              label="Nombre"
              variant="outlined"
              value={formCreateUser["Nombre"] || ""}
            />
          </div>
          <div>
            <InputCustom
              onChange={e => {
                setDataInput("Telefono", e.target.value);
              }}
              label="Telefono"
              variant="outlined"
              value={formCreateUser["Telefono"] || ""}
            />
          </div>
          <div>
            <InputCustom
              onChange={e => {
                setDataInput("Email", e.target.value);
              }}
              label="Email"
              variant="outlined"
              value={formCreateUser["Email"] || ""}
            />
          </div>
          <div>
            {props.type === "UPDATE" && (
              <Switches
                text="Cambiar Contraseña"
                fn={bool => setshowInputPassword(bool)}
              />
            )}
            {showInputPassword && (
              <InputCustom
                onChange={e => {
                  setDataInput("Password", e.target.value);
                }}
                label="Password"
                variant="outlined"
              />
            )}
          </div>
          <div>
            {dataRoles && (
              <SelectCustom
                defaultValue={
                  props.location.state
                    ? dataRoles.Data.filter(
                        item => props.location.state.Rol === item.Rol
                      )[0]
                    : null
                }
                objKey="Rol"
                label="Rol"
                options={dataRoles.Data}
                handleChange={datos => {
                  setDataInput("Rol", datos.Rol);
                }}
              />
            )}
          </div>
          <div>
            {Othercomponent && formCreateUser.Rol === ClienteA && (
              <Othercomponent
                cb={(gerenciaSelected, areaSelected) => {
                  console.log(gerenciaSelected, areaSelected);
                  setDataInput("gerenciaSelected", gerenciaSelected);
                  setDataInput("areaSelected", areaSelected);
                }}
                gerneciaAndArea
              />
            )}
            {Othercomponent && formCreateUser.Rol === ClienteB && (
              <Othercomponent
                cb={gerenciaSelected => {
                  console.log(gerenciaSelected);
                  setDataInput("gerenciaSelected", gerenciaSelected);
                }}
              />
            )}
          </div>
          {/* <div>
            {dataClientesSucursales && (
              <SelectCustom
                defaultValue={
                  props.location.state
                    ? dataClientesSucursales.Data.filter(
                        item => props.location.state.Cliente === item.Cliente
                      )[0]
                    : null
                }
                objKey="Cliente"
                label="Cliente/Sucursal"
                options={dataClientesSucursales.Data}
                handleChange={datos => {
                  setDataInput("Cliente", datos.Cliente);
                }}
              />
            )}
          </div> */}
          <div className="conatinerButton">
            {props.showBtn[0] && (
              <ButtonCustom
                onClick={async () => {
                  const { Status = "fail" } = await UpdateUser(
                    userSession,
                    format(formCreateUser)
                  );
                  props.history.push({
                    pathname: "/administrar/usuarios",
                    state: {
                      isOpen: true,
                      type: Status === "OK" ? "success" : "error",
                      message:
                        Status === "OK"
                          ? "Usuario actualizado con exito"
                          : "Usuario no actualizado intente mas tarde",
                    },
                  });
                }}
              >
                Realizar Cambios
              </ButtonCustom>
            )}
            {props.showBtn[1] && (
              <ButtonCustom
                onClick={async () => {
                  const { Status = "fail" } = await PostCreateUser(
                    userSession,
                    format(formCreateUser)
                  );
                  props.history.push({
                    pathname: "/administrar/usuarios",
                    state: {
                      isOpen: true,
                      type: Status === "OK" ? "success" : "error",
                      message:
                        Status === "OK"
                          ? "Usuario creado con exito"
                          : "Usuario no creado intente mas tarde",
                    },
                  });
                }}
              >
                Crear Usuario
              </ButtonCustom>
            )}
            {props.showBtn[2] && (
              <ButtonCustom
                onClick={async () => {
                  const { Status = "fail" } = await deleteUser(userSession, {
                    id: props.location.state.Id,
                  });
                  props.history.push({
                    pathname: "/administrar/usuarios",
                    state: {
                      isOpen: true,
                      type: Status === "OK" ? "success" : "error",
                      message:
                        Status === "OK"
                          ? "Usuario eliminado con exito"
                          : "Usuario no eliminado intente mas tarde",
                    },
                  });
                }}
              >
                Eliminar Usuario
              </ButtonCustom>
            )}
          </div>
        </React.Fragment>
      )}
    </FormContainer>
  );
});
