import styled from "styled-components";

export const FormContainer = styled.form`
  display: flex;
  flex-direction: column;
  align-items: center;
  justify-content: center;
  & > div {
    padding: 1em 0em;
    & > div {
      width: 100%;
    }
    width: 30em;
  }
  & > .conatinerButton {
    width: 30em;
    display: flex;
    justify-content: space-around;
  }
`;
