import React from "react";
import TextField from "@material-ui/core/TextField";
// import { Input as TextField } from '@material-ui/core';

export const InputCustom = (props) => {
  return <TextField autoFocus={false} {...props} />;
};
