import React, { useState } from "react";
import { Container } from './styles'
export function LoginForm({ submit = () => alert("Default Fn") }) {
  const [userText, setUserTex] = useState("");
  const [passwordText, setPasswordText] = useState("");
  return (
    <Container>
      <h4 style={{ ...styles.headerText }}>{'Ingrese sus credenciales'}</h4>
      <input style={{ ...styles.inputs }} className="form-control form-control-lg" type="text" placeholder="Usuario" onChange={(e) => setUserTex(e.target.value)} />
      <input style={{ ...styles.inputs }} className="form-control form-control-lg" type="password" placeholder="Contraseña" onChange={(e) => setPasswordText(e.target.value)} />
      <button style={{ ...styles.button }} onClick={() => submit({ userText, passwordText })} className={`btn btn-outline-light btn-lg `}>
        Ingresar
      </button>
    </Container>
  );
}

const styles = {
  divContainer: {
    borderRadius: "20px",
    flexDirection: "column",
    textAlign: "center",
    justifyContent: "center",
    alignItems: "center",
    maxWidth: "32em",
    maxHeight: "60%",
    border: "solid 2px rgb(246, 244, 235)",
  },
  headerText: {
    color: "white",
    width: "55%",
    // backgroundColor:'red'
  },
  inputs: {
    margin: "1em",
    width: "20em",
    borderRadius: "15px",
    textAlign: "center",
  },
  button: {
    width: "8em",
    marginTop: "2em",
  },
};
