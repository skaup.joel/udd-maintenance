import styled from "styled-components";

export const Container = styled.form`
display:flex;
padding: 1em;
  border-radius: 20px;
  flex-direction: column;
  text-align: center;
  justify-content: center;
  align-items: center;
  max-width: 100%;
  max-height: 100%;
  border: 2px solid rgb(246, 244, 235);
`;
