import React from "react";
import ReactSelect from "./../../components/ReactSelect";
import styled from "styled-components";
import Button from "@material-ui/core/Button";

const Container = styled.div`
  display: flex;
  width: 100%;
  & > div {
    width: 20em;
    margin: 0em 1em;
  }
`;
export const Filters = (props) => {
  return (
    <Container>
      <ReactSelect
        isMulti
        placeholder={"Seleccione Gerencias a filtrar"}
        options={props.allGerencia.Data.map((item) => {
          return { ...item, label: item.Gerencia, value: JSON.stringify(item) };
        })}
        defaultOptions={props.allGerencia.Data.map((item) => {
          return { ...item, label: item.Gerencia, value: JSON.stringify(item) };
        })}
        fn={(value) => props.allGerenciasetFiltros(value || [])}
      />
      <ReactSelect
        isMulti
        placeholder={"Seleccione Area General"}
        options={props.allArea.Data.map((item) => {
          return { ...item, label: item.Area, value: JSON.stringify(item) };
        })}
        defaultOptions={props.allArea.Data.map((item) => {
          return { ...item, label: item.Area, value: JSON.stringify(item) };
        })}
        fn={(value) => props.allAreasetFiltros(value || [])}
      />

      <ReactSelect
        isMulti
        placeholder={"Seleccione Area"}
        options={props.allSector.Data.map((item) => {
          return { ...item, label: item.Sector, value: JSON.stringify(item) };
        })}
        defaultOptions={props.allSector.Data.map((item) => {
          return { ...item, label: item.Sector, value: JSON.stringify(item) };
        })}
        fn={(value) => props.allSectorsetFiltros(value || [])}
      />
      <div>
        <Button
          onClick={() => props.goTo("/equipment/crearEquipos/0")}
          variant="outlined"
        >
          Crear Equipo
        </Button>
      </div>
    </Container>
  );
};
