import React, { useEffect, useState } from "react";
import { Table } from "./../../components/TableCustom";
import { BasicPagination } from "./../../components/PaginationMaterial";
import { useHistory } from "react-router-dom";
import { useDispatch, useSelector } from "react-redux";
import * as actions from "../../state/actions/listarEquiposParaAdministrarlos";
import { Filters } from "./Filters";
import { Spiner } from "./../../components/Spiner";

export const EquipmentList = () => {
  let history = useHistory();
  const dispatch = useDispatch();

  const [statePagination, setStatePage] = useState({ page: 1, totalPagination: 10, desde: 0, hasta: 50 });

  const { data: dataListaDeEquipos, isLoading: isLoadingListaDeEquipos, error: errorListaDeEquipos } = useSelector((state) => state.listarEquiposParaAdministrarlos);
  const userSession = useSelector((state) => state.sessionReducer.userData.User);

  const { data: allGerencia, isLoading: isLoadingAllGerencia, error: errorAllGerencia } = useSelector((state) => state.allGerencia);
  const { data: allArea, isLoading: isLoadingallArea, error: errorallArea } = useSelector((state) => state.areaGeneral);
  const { data: allSector, isLoading: isLoadingAllSector, error: errorAllSector } = useSelector((state) => state.allArea);

  const [allGerenciaFiltros, allGerenciasetFiltros] = useState([]);
  const [allAreaFiltros, allAreasetFiltros] = useState([]);
  const [allSectorFiltros, allSectorsetFiltros] = useState([]);

  const goTo = (ruta, data) =>
    history.push({
      pathname: ruta,
      state: data,
    });

  useEffect(() => {
    dataListaDeEquipos && dataListaDeEquipos.Data.length !== 0 && setStatePage({ ...statePagination, totalPagination: Math.ceil(dataListaDeEquipos.Data[0].Cantidad / 50) });
    // eslint-disable-next-line react-hooks/exhaustive-deps
  }, [dataListaDeEquipos]);

  useEffect(() => {
    setStatePage({ page: 1, totalPagination: 10, desde: 0, hasta: 50 });
    actions.ListarEquiposParaAdministrarlosAction(dispatch, {
      userSession,
      gerencia: `${allGerenciaFiltros.length !== 0 ? allGerenciaFiltros.map((item) => item.Id) : 0}`,
      area: `${allAreaFiltros.length !== 0 ? allAreaFiltros.map((item) => item.Id) : 0}`,
      sector: `${allSectorFiltros.length !== 0 ? allSectorFiltros.map((item) => item.Id) : 0}`,
      ...statePagination,
    });
    // eslint-disable-next-line react-hooks/exhaustive-deps
  }, [allGerenciaFiltros, allAreaFiltros, allSectorFiltros, userSession]);

  return (
    <React.Fragment>
      {[isLoadingAllGerencia, isLoadingallArea, isLoadingAllSector, isLoadingListaDeEquipos].every((item) => item) && <Spiner />}
      {[errorAllGerencia, errorallArea, errorAllSector, errorListaDeEquipos].every((item) => item !== "") && <h1>Tenemos un error {errorAllGerencia}</h1>}
      {[allGerencia, allArea, allSector, dataListaDeEquipos].every((item) => item !== null) && (
        <React.Fragment>
          <Filters
            allSector={allSector}
            allSectorsetFiltros={allSectorsetFiltros}
            allAreasetFiltros={allAreasetFiltros}
            allArea={allArea}
            allGerencia={allGerencia}
            allGerenciasetFiltros={allGerenciasetFiltros}
            goTo={goTo}
          />
          {dataListaDeEquipos.Data.length !== 0 ? (
            <React.Fragment>
              <BasicPagination
                page={statePagination.page}
                count={statePagination.totalPagination}
                onChange={(e, p) => {
                  actions.ListarEquiposParaAdministrarlosAction(dispatch, {
                    userSession,
                    gerencia: `${allGerenciaFiltros.length !== 0 ? allGerenciaFiltros.map((item) => item.Id) : 0}`,
                    area: `${allAreaFiltros.length !== 0 ? allAreaFiltros.map((item) => item.Id) : 0}`,
                    sector: `${allSectorFiltros.length !== 0 ? allSectorFiltros.map((item) => item.Id) : 0}`,
                    desde: (p - 1) * 50,
                    hasta: p * 50 + 50,
                  });
                  setStatePage({ ...statePagination, page: p });
                }}
                // onChange={(e, p) => }
              />
              <Table textToButton="Editar" fnEditar={(data) => goTo(`/equipment/actualizarEquipos/${data.Id}`, { idSelected: data.Id })} {...{ headers, data: dataListaDeEquipos.Data }} />
            </React.Fragment>
          ) : (
            <h1>no hay datos con estos filtros</h1>
          )}
        </React.Fragment>
      )}
    </React.Fragment>
  );
};

const headers = [ "Codigo", "Descripcion", "Serie", "Detalle", "Tipo", "Gerencia", "Area", "Sector"];
