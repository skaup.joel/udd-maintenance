import React, { useEffect, useState } from "react";
import styled from "styled-components";
import ReactSelect from "../ReactSelect";
import { useSelector, useDispatch } from "react-redux";
import { FiltroAreaGeneral as filtrarArea, FiltroArea as filtrarSector } from "../../api/filtros_api";
import { createOrUpdateEquipo } from "../../api";
import { useParams } from "react-router-dom";
import { Spiner } from "./../../components/Spiner";
import { InputIterator } from "./InputIterator";
import { useHistory } from "react-router-dom";
import Button from "@material-ui/core/Button";
const Container = styled.div`
  width: inherit;
  height: inherit;
  display: flex;
  justify-content: center;
  align-items: center;
  flex-direction: column;
  .esteDiv {
    border: 1px solid;
    border-radius: 10px;
    padding: 2em;
  }
  .wrapper_selectInput_gestionDeEquipo {
    padding: 0.5em;
    width: 100%;
    display: flex;
    justify-content: space-between;
    & input {
      width: 100%;
    }
    & label {
      width: 50%;
      width: 25em;
    }
  }
`;
export const FormUpdateEquipment = (props) => {
  const dispatch = useDispatch()
  let history = useHistory();
  const { idEquipment } = useParams();
  const { detailEquitment } = props;

  const [lastState, setlastState] = useState({});
  const stateRedux = useSelector((state) => state);
  const userSession = useSelector((state) => state.sessionReducer.userData.User);

  const searchIdAllCalls = (stateInRedux, idInDetail, dataToLabel) => {
    const data = stateRedux[stateInRedux].data.Data.filter(({ Id }) => Id === detailEquitment[0][idInDetail]).map((item) => {
      return { ...item, label: item[dataToLabel], value: item[dataToLabel] };
    });
    return data;
  };

  const [idtipo, setidtipo] = useState(searchIdAllCalls("tipoEquipo", "Id_Tipo", "TipoEquipo"));
  const [idmm, setidmm] = useState(searchIdAllCalls("AllMarcaModelo", "Id_MM", "MarcaModelo"));
  const [idpi, setidpi] = useState(searchIdAllCalls("allProtocolos", "Id_PInsp", "Descripcion"));
  const [idpp, setidpp] = useState(searchIdAllCalls("allProtocolos", "Id_PPrue", "Descripcion"));
  const [idpm, setidpm] = useState(searchIdAllCalls("allProtocolos", "Id_PMant", "Descripcion"));
  const [idg, setidg] = useState(searchIdAllCalls("allGerencia", "Id_PMant", "Gerencia"));
  const [ida, setida] = useState([]);
  const [ids, setids] = useState([]);
  
  const [isLoading, setIsloading] = useState(false);
  
  useEffect(() => {
      // fetch(`https://sapma.sercoing.cl/svc/listar_protocolosportipo.py?login=${userSession}&tipoequipo=${idtipo[0].Id}`)
      setTimeout(() => {
        setIsloading(true)

      fetch(`https://sapma.sercoing.cl/svc/listar_protocolosportipo.py?login=${userSession}&tipoequipo=${3}`)
        .then(res=> res.json())
        .then(data =>{
            dispatch({ type: 'GET_ALL_PROTOCOLOS_DATA',payload: data })
            setIsloading(false)
          })
          .catch(err => {
            setIsloading(true)
            console.log(err)})
          }, 400);
            
  }, [idtipo, userSession, dispatch])

  const [allAreasOption, setAllAreasOption] = useState([]); // seudo estado de redux
  const [allSectorOption, setAllSectorOption] = useState([]); // seudo estado de redux

  const [state, setstate] = useState({
    DetalleEquipo: detailEquitment[0].Detalle || "",
    codigoEquipo: detailEquitment[0].Codigo || "",
    nombreEquipo: detailEquitment[0].Equipo || "",
    serielEquipo: detailEquitment[0].Serie || "",
  });

  useEffect(() => {
    setlastState({ ...state, idtipo, idmm, idpi, idpp, idpm, idg, ida, ids });
  }, [state, idtipo, idmm, idpi, idpp, idpm, idg, ida, ids]);

  useEffect(() => {
    setAllAreasOption([]);
    setAllSectorOption([]);
    try {
      idg.length !== 0 &&
        filtrarArea(userSession, { gerencia: `${idg.map((item) => item.Id)}` }).then((datos) => {
          setAllAreasOption(datos.Data);
          setida(
            datos.Data.filter(({ Id }) => Id === detailEquitment[0].Id_Area).map((item) => {
              return { ...item, label: item.Area, value: item.Area };
            })
          );
        });
    } catch (error) {
      console.log(error);
    }
    // eslint-disable-next-line react-hooks/exhaustive-deps
  }, [idg]);

  useEffect(() => {
    setAllSectorOption([]);
    try {
      ida.length !== 0 &&
        filtrarSector(userSession, { gerencia: `${idg.map((item) => item.Id)}`, area: `${ida.map((item) => item.Id)}` }).then((datos) => {
          setAllSectorOption(datos.Data);
          setids(
            datos.Data.filter(({ Id }) => Id === detailEquitment[0].Id_Sector).map((item) => {
              return { ...item, label: item.Sector, value: item.Sector };
            })
          );
        });
    } catch (error) {
      console.log(error);
    }
    // eslint-disable-next-line react-hooks/exhaustive-deps
  }, [ida]);

  const changueState = (datos, name) => {
    setstate({ ...state, [name]: datos });
  };

  const crearEquipo = async () => {
    const { DetalleEquipo, codigoEquipo, nombreEquipo, serielEquipo, ida, idg, idpi, idpp, idmm, idpm, ids, idtipo } = lastState;
    const { Status = "fail" } = await createOrUpdateEquipo({
      detalle: DetalleEquipo, // input // NOMBRE
      id: idEquipment, //num // 0 Crea otro actualiza
      codigo: codigoEquipo, //input text
      nombre: nombreEquipo, //input text
      serie: serielEquipo, //input
      idtipo: idtipo.map(({ Id }) => Id), //lista select  listar_tipoequi  /L36 TIPO nsita: nada
      idmm: idmm.map(({ Id }) => Id), // lista  select ??? listar_mm / L19 MARCA MODELO nsita: nada
      idpi: idpi.map(({ Id }) => Id), // id protocolo select inspeccion () Id_Tipo 1 /listar protocolo / L30
      idpp: idpp.map(({ Id }) => Id), // id protocolo select inspeccion () Id_Tipo 1 /listar protocolo / L30
      idpm: idpm.map(({ Id }) => Id), // id protocolo select mantencion () Id_Tipo  2 / listar protocolo / L30
      idg: idg.map(({ Id }) => Id), // id select/ L12 GERENCIA / nsita: nada
      ida: ida.map(({ Id }) => Id), //id area select / L22 AREA / nsita: +gerencia+
      ids: ids.map(({ Id }) => Id), // id     select  / L23 SECTOR / nsita: +gerencia+ +area+
    });
    history.push({
      pathname: "/work",
      state: {
        isOpen: true,
        type: Status === "OK" ? "success" : "error",
        message: Status === "OK" ? "Equipo actualizado con exito" : "Equipo no actualizado intente mas tarde",
      },
    });
  };

  return (
    <Container>
      {[stateRedux.tipoEquipo, stateRedux.AllMarcaModelo, stateRedux.allProtocolos, stateRedux.allGerencia].map(({ error }) => error).every((item) => item !== "") && (
        <h1>Tenemos un error en los datos</h1>
      )}
      {[
        stateRedux.tipoEquipo,
        stateRedux.AllMarcaModelo,
        stateRedux.allProtocolos,
        stateRedux.allGerencia,
        // loadingDataDeatilEquipo
      ]
        .map(({ isLoading }) => isLoading)
        .every((item) => item === true) && <Spiner />}
      <div className="esteDiv">
        {!isLoading && <InputIterator
          setidtipo={setidtipo}
          setidmm={setidmm}
          setidpi={setidpi}
          setidpp={setidpp}
          setidpm={setidpm}
          setidg={setidg}
          state={state}
          stateRedux={stateRedux}
          changueState={changueState}
          idtipo={idtipo}
          idmm={idmm}
          idpi={idpi}
          idpp={idpp}
          idpm={idpm}
          idg={idg}
          ida={ida}
          ids={ids}
        />}
        {allAreasOption.length !== 0 && (
          <div className="wrapper_selectInput_gestionDeEquipo">
            <label>Area:</label>
            <ReactSelect
              value={ida}
              Searchable={false}
              isMulti={false}
              fn={(datos) => setida([datos])}
              options={allAreasOption.map((item) => {
                return { ...item, label: item.Area, value: item.Area };
              })}
              defaultOptions={allAreasOption.map((item) => {
                return { ...item, label: item.Area, value: item.Area };
              })}
              placeholder={`Seleccione uno`}
            />
          </div>
        )}
        {allSectorOption.length !== 0 && (
          <div className="wrapper_selectInput_gestionDeEquipo">
            <label>Sector</label>
            <ReactSelect
              value={ids}
              Searchable={false}
              isMulti={false}
              fn={(datos) => setids([datos])}
              options={allSectorOption.map((item) => {
                return { ...item, label: item.Sector, value: item.Sector };
              })}
              defaultOptions={allSectorOption.map((item) => {
                return { ...item, label: item.Sector, value: item.Sector };
              })}
              placeholder={`Seleccione uno`}
            />
          </div>
        )}
        {lastState.DetalleEquipo &&
          lastState.codigoEquipo &&
          lastState.nombreEquipo &&
          lastState.serielEquipo &&
          lastState.ida.length !== 0 &&
          lastState.idg.length !== 0 &&
          lastState.idpi.length !== 0 &&
          lastState.idpp.length !== 0 &&
          lastState.idmm.length !== 0 &&
          lastState.idpm.length !== 0 &&
          lastState.ids.length !== 0 &&
          lastState.idtipo.length !== 0 && (
            <div style={{ display: "flex", justifyContent: "center", alignItems: "center", padding: "1em 0em 0em 0em" }}>
              <Button onClick={crearEquipo} variant="contained" color="primary">
                Actualizar equipo
              </Button>
            </div>
          )}
      </div>
    </Container>
  );
};
