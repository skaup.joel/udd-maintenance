import React from "react";
import ReactSelect from "../ReactSelect";

export const InputIterator = (props) => {
  const { stateRedux } = props;
  return (
    <>
      {[stateRedux.tipoEquipo, stateRedux.AllMarcaModelo, stateRedux.allProtocolos, stateRedux.allGerencia].map(({ data }) => data).every((item) => item !== null) &&
        [
          {
            label: "Detalle",
            type: "input",
            stateName: "DetalleEquipo",
          },
          {
            label: "Codigo",
            type: "input",
            stateName: "codigoEquipo",
          },
          {
            label: "Nombre",
            type: "input",
            stateName: "nombreEquipo",
          },
          {
            label: "Serie",
            type: "input",
            stateName: "serielEquipo",
          },
          {
            label: "Tipo",
            type: "select",
            setState: (data) => props.setidtipo([data]),
            defaultValue: props.idtipo,
            stateName: "idtipoEquipo",
            optionsInRedux: "TipoEquipo",
            reduxStateName: "tipoEquipo",
          },
          {
            label: "MARCA MODELO",
            type: "select",
            setState: (data) => props.setidmm([data]),
            defaultValue: props.idmm,
            stateName: "idmmEquipo",
            optionsInRedux: "MarcaModelo",
            reduxStateName: "AllMarcaModelo",
          },
          {
            label: "Protocolo Inspeccion",
            type: "select",
            setState: (data) => props.setidpi([data]),
            defaultValue: props.idpi,
            stateName: "idpiEquipo",
            optionsInRedux: "Descripcion",
            reduxStateName: "allProtocolos",
            tipoProtrotocolo: 1,
          },
          {
            label: "Protocolo Mantencion",
            type: "select",
            setState: (data) => props.setidpm([data]),
            defaultValue: props.idpm,
            stateName: "idpmEquipo",
            optionsInRedux: "Descripcion",
            reduxStateName: "allProtocolos",
            tipoProtrotocolo: 2,
          },
          {
            label: "Protocolo prueba",
            type: "select",
            setState: (data) => props.setidpm([data]),
            defaultValue: props.idpp,
            stateName: "idppEquipo",
            optionsInRedux: "Descripcion",
            reduxStateName: "allProtocolos",
            tipoProtrotocolo: 3,
          },
          {
            label: "Gerencia",
            type: "select",
            setState: (data) => props.setidg([data]),
            defaultValue: props.idg,
            stateName: "idgEquipo",
            optionsInRedux: "Gerencia",
            reduxStateName: "allGerencia",
          },
        ].map((element) => {
          const { label, type, stateName } = element;
          if (type === "input") {
            return (
              <div key={element.stateName} className="wrapper_selectInput_gestionDeEquipo">
                <label htmlFor={label}>{label}:</label>
                <input id={label} onChange={(e) => props.changueState(e.target.value, stateName)} value={props.state[stateName]} type="text" />
              </div>
            );
          }
          if (type === "select") {
            return (
              <div key={element.stateName} className="wrapper_selectInput_gestionDeEquipo">
                <label htmlFor={label}>{label}:</label>
                <ReactSelect
                  Searchable={false}
                  isMulti={false}
                  value={element.defaultValue}
                  fn={(datos) => element.setState(datos)}
                  options={
                    element.tipoProtrotocolo
                      ? stateRedux[element.reduxStateName].data.Data.filter((i) => i.Id_Tipo === element.tipoProtrotocolo)
                          .filter((it) => it.Id_Tipo === element.tipoProtrotocolo)
                          .map((item) => {
                            return { ...item, label: item[element.optionsInRedux], value: item[element.optionsInRedux] };
                          })
                      : stateRedux[element.reduxStateName].data.Data.map((item) => {
                          return { ...item, label: item[item.optionsInRedux], value: item[item.optionsInRedux] };
                        })
                  }
                  defaultOptions={
                    element.tipoProtrotocolo
                      ? stateRedux[element.reduxStateName].data.Data.filter((i) => i.Id_Tipo === element.tipoProtrotocolo)
                          .filter((it) => it.Id_Tipo === element.tipoProtrotocolo)
                          .map((item) => {
                            return { ...item, label: item[element.optionsInRedux], value: item[element.optionsInRedux] };
                          })
                      : stateRedux[element.reduxStateName].data.Data.map((item) => {
                          return { ...item, label: item[element.optionsInRedux], value: item[element.optionsInRedux] };
                        })
                  }
                  placeholder={`Seleccione uno`}
                />
              </div>
            );
          }
          return "";
        })}
    </>
  );
};
