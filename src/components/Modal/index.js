import React, { useState } from "react";

export const Modal = (props) => {
  props = { ...mockData, ...props };
  const { textButton, ModalTitle, TextToDoBtn, TextToCancel, children, fn, showBtn, btnIsDisabled, showCancelBtn, btnOpenModalIsDisabled } = props;
  const [stateModal, setStateModal] = useState(props.inicialStateModal || false);
  return (
    <div>
      {showBtn && (
        <button disabled={btnOpenModalIsDisabled}  type="button" className={props.className} data-toggle="modal" onClick={() => setStateModal(!stateModal)}>
          {textButton}
        </button>
      )}

      <div
        className={`modal fade ${stateModal && "show"}`}
        style={{ display: `${stateModal ? "block" : "none"}`, background: "#0006" }}
        id="exampleModal"
        tabIndex="-1"
        role="dialog"
        aria-labelledby="exampleModalLabel"
        aria-hidden="true"
      >
        <div className="modal-dialog" role="document">
          <div className="modal-content" style={{minWidth:'50em'}}>
            <div className="modal-header">
              <h5 className="modal-title" id="exampleModalLabel">
                {ModalTitle}
              </h5>
              <button onClick={() => setStateModal(!stateModal)} type="button" className="close" aria-label="Close" style={{ display: `${showCancelBtn ? "block" : "none"}` }}>
                <span aria-hidden="true">&times;</span>
              </button>
            </div>
            <div className="modal-body">{children}</div>
            <div className="modal-footer">
              {props.subButtonReplicar}
              <button onClick={() => setStateModal(!stateModal)} type="button" className="btn btn-secondary " style={{ display: `${showCancelBtn ? "block" : "none"}` }}>
                {TextToCancel}
              </button>
              <button
                disabled={btnIsDisabled}
                onClick={() => {
                  setStateModal(!stateModal);
                  fn();
                }}
                type="button"
                className="btn btn-primary"
              >
                {TextToDoBtn}
              </button>
            </div>
          </div>
        </div>
      </div>
    </div>
  );
};

const mockData = {
  fn: () => null,
  textButton: "default-textButton",
  ModalTitle: "default-ModalTitle",
  TextToDoBtn: "default-TextToDoBtn",
  TextToCancel: "Cancelar",
  children: <h1>Cuerpo del Modal</h1>,
  showBtn: true,
  showCancelBtn: true,
  btnIsDisabled: false,
  btnOpenModalIsDisabled:false
};
