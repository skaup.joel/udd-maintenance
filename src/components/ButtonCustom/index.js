import React from 'react';
import Button from '@material-ui/core/Button';

export const ButtonCustom = (props) => {
  return (
    <Button  {...{variant:"outlined", ...props}}/>
  );
}

