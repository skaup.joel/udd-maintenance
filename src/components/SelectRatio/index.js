import React, { useEffect } from "react";
import Radio from "@material-ui/core/Radio";
import RadioGroup from "@material-ui/core/RadioGroup";
import FormControlLabel from "@material-ui/core/FormControlLabel";
import FormControl from "@material-ui/core/FormControl";
import FormLabel from "@material-ui/core/FormLabel";

export default function RadioButtonsGroup(props) {
  props = { ...mocks, ...props };
  const { options, handlerChange, title, defaultSelected, selectOneAndDesabled, ...res } = props;
  const [value, setValue] = React.useState(defaultSelected);

  useEffect(() => {
    setValue(defaultSelected);
  }, [defaultSelected]);
  const handleChange = (event) => {
    setValue(event.target.value);
    handlerChange(event.target.value);
  };

  return (
    <FormControl {...res} style={{ width: "100%" }} component="fieldset">
      <FormLabel component="legend">{title}</FormLabel>
      <RadioGroup aria-label="gender" name="gender1" value={value} onChange={handleChange}>
        {options.map(({ disabled, label, value }, index) => {
          disabled = selectOneAndDesabled;
          return <FormControlLabel key={label + index} value={value} disabled={disabled || false} control={<Radio />} label={label} />;
        })}
      </RadioGroup>
    </FormControl>
  );
}

const mocks = {
  selectOneAndDesabled: false,
  defaultSelected: "",
  title: "title",
  handlerChange: () => null,
  options: [
    { disabled: false, value: "Female1", label: "Female" },
    { disabled: false, value: "Male2", label: "Male" },
    { disabled: false, value: "Other3", label: "Other" },
    { disabled: true, value: "disabled4", label: "(Disabled option)" },
  ],
};
