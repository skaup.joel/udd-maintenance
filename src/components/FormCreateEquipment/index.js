import React, { useEffect, useState } from "react";
import styled from "styled-components";
import ReactSelect from "../ReactSelect";
import { useSelector } from "react-redux";
import { FiltroAreaGeneral as filtrarArea, FiltroArea as filtrarSector } from "../../api/filtros_api";
import { createOrUpdateEquipo } from "../../api";
import { useHistory } from "react-router-dom";
import { Spiner } from "./../../components/Spiner";

const Container = styled.div`
  width: inherit;
  height: inherit;
  display: flex;
  justify-content: center;
  align-items: center;
  flex-direction: column;
  .esteDiv {
    border: 1px solid;
    border-radius: 10px;
    padding: 2em;
  }
  .wrapper_selectInput_gestionDeEquipo {
    padding: 0.5em;
    width: 100%;
    display: flex;
    justify-content: space-between;
    & input {
      width: 100%;
    }
    & label {
      width: 50%;
      width: 25em;
    }
  }
`;
export const FormCreateEquipment = () => {
  let history = useHistory();
  const [lastState, setlastState] = useState({});
  const stateRedux = useSelector((state) => state);
  const userSession = useSelector((state) => state.sessionReducer.userData.User);

  const [idtipo, setidtipo] = useState([]);
  const [idmm, setidmm] = useState([]);
  const [idpi, setidpi] = useState([]);
  const [idpp, setidpp] = useState([]);
  const [idpm, setidpm] = useState([]);
  const [idg, setidg] = useState([]);
  const [ida, setida] = useState([]);
  const [ids, setids] = useState([]);

  const [allAreasOption, setAllAreasOption] = useState([]); // seudo estado de redux
  const [allSectorOption, setAllSectorOption] = useState([]); // seudo estado de redux
  const form = [
    {
      label: "Detalle",
      type: "input",
      stateName: "DetalleEquipo",
    },
    {
      label: "Codigo",
      type: "input",
      stateName: "codigoEquipo",
    },
    {
      label: "Nombre",
      type: "input",
      stateName: "nombreEquipo",
    },
    {
      label: "Serie",
      type: "input",
      stateName: "serielEquipo",
    },
    {
      label: "Tipo",
      type: "select",
      setState: (data) => setidtipo([data]),
      stateName: "idtipoEquipo",
      optionsInRedux: "TipoEquipo",
      reduxStateName: "tipoEquipo",
    },
    {
      label: "MARCA MODELO",
      type: "select",
      setState: (data) => setidmm([data]),
      stateName: "idmmEquipo",
      optionsInRedux: "MarcaModelo",
      reduxStateName: "AllMarcaModelo",
    },
    {
      label: "Protocolo Inspeccion",
      type: "select",
      setState: (data) => setidpi([data]),
      stateName: "idpiEquipo",
      optionsInRedux: "Descripcion",
      reduxStateName: "allProtocolos",
      tipoProtrotocolo: 1,
    },
    {
      label: "Protocolo Mantencion",
      type: "select",
      setState: (data) => setidpm([data]),
      stateName: "idpmEquipo",
      optionsInRedux: "Descripcion",
      reduxStateName: "allProtocolos",
      tipoProtrotocolo: 2,
    },
    {
      label: "Protocolo prueba",
      type: "select",
      setState: (data) => setidpp([data]),
      stateName: "idppEquipo",
      optionsInRedux: "Descripcion",
      reduxStateName: "allProtocolos",
      tipoProtrotocolo: 3,
    },
    {
      label: "Gerencia",
      type: "select",
      setState: (data) => setidg([data]),
      stateName: "idgEquipo",
      optionsInRedux: "Gerencia",
      reduxStateName: "allGerencia",
    },
  ];
  const [state, setstate] = useState({});

  useEffect(() => {
    setlastState({ ...state, idtipo, idmm, idpi, idpp, idpm, idg, ida, ids });
  }, [state, idtipo, idmm, idpi, idpm, idg, ida, idpp, ids]);

  useEffect(() => {
    setAllAreasOption([]);
    setAllSectorOption([]);
    try {
      idg.length !== 0 && filtrarArea(userSession, { gerencia: `${idg.map((item) => item.Id)}` }).then((datos) => setAllAreasOption(datos.Data));
    } catch (error) {
      console.log(error);
    }
    // eslint-disable-next-line react-hooks/exhaustive-deps
  }, [idg]);

  useEffect(() => {
    setAllSectorOption([]);
    try {
      ida.length !== 0 &&
        filtrarSector(userSession, { gerencia: `${idg.map((item) => item.Id)}`, area: `${ida.map((item) => item.Id)}` }).then((datos) => setAllSectorOption(datos.Data));
    } catch (error) {
      console.log(error);
    }
    // eslint-disable-next-line react-hooks/exhaustive-deps
  }, [ida]);

  const changueState = (datos, name) => {
    setstate({ ...state, [name]: datos });
  };

  const crearEquipo = async () => {
    const { DetalleEquipo, codigoEquipo, nombreEquipo, serielEquipo, ida, idg, idpi, idpp,idmm, idpm, ids, idtipo } = lastState;
    const { Status = "fail" } = await createOrUpdateEquipo({
      detalle: DetalleEquipo, // input // NOMBRE
      id: 0, //num // 0 Crea otro actualiza
      codigo: codigoEquipo, //input text
      nombre: nombreEquipo, //input text
      serie: serielEquipo, //input
      idtipo: idtipo.map(({ Id }) => Id), //lista select  listar_tipoequi  /L36 TIPO nsita: nada
      idmm: idmm.map(({ Id }) => Id), // lista  select ??? listar_mm / L19 MARCA MODELO nsita: nada
      idpi: idpi.map(({ Id }) => Id), // id protocolo select inspeccion () Id_Tipo 1 /listar protocolo / L30
      idpp: idpp.map(({ Id }) => Id), // id protocolo select inspeccion () Id_Tipo 1 /listar protocolo / L30
      idpm: idpm.map(({ Id }) => Id), // id protocolo select mantencion () Id_Tipo  2 / listar protocolo / L30
      idg: idg.map(({ Id }) => Id), // id select/ L12 GERENCIA / nsita: nada
      ida: ida.map(({ Id }) => Id), //id area select / L22 AREA / nsita: +gerencia+
      ids: ids.map(({ Id }) => Id), // id     select  / L23 SECTOR / nsita: +gerencia+ +area+
    });
    history.push({
      pathname: "/work",
      state: {
        isOpen: true,
        type: Status === "OK" ? "success" : "error",
        message: Status === "OK" ? "Equipo creado con exito" : "Equipo no creado intente mas tarde",
      },
    });
  };

  return (
    <Container>
      <div className="esteDiv">
        {[stateRedux.tipoEquipo, stateRedux.AllMarcaModelo, stateRedux.allProtocolos, stateRedux.allGerencia].map(({ error }) => error).every((item) => item !== "") && (
          <h1>Tenemos un error en los datos</h1>
        )}
        {[stateRedux.tipoEquipo, stateRedux.AllMarcaModelo, stateRedux.allProtocolos, stateRedux.allGerencia].map(({ isLoading }) => isLoading).every((item) => item === true) && (
          <Spiner />
        )}
        {[stateRedux.tipoEquipo, stateRedux.AllMarcaModelo, stateRedux.allProtocolos, stateRedux.allGerencia].map(({ data }) => data).every((item) => item !== null) &&
          form.map((element) => {
            const { label, type, stateName } = element;
            if (type === "input") {
              return (
                <div key={element.stateName} className="wrapper_selectInput_gestionDeEquipo">
                  <label htmlFor={label}>{label}:</label>
                  <input id={label} onChange={(e) => changueState(e.target.value, stateName)} type="text" />
                </div>
              );
            }
            if (type === "select") {
              return (
                <div key={element.stateName} className="wrapper_selectInput_gestionDeEquipo">
                  <label htmlFor={label}>{label}:</label>
                  <ReactSelect
                    Searchable={false}
                    isMulti={false}
                    fn={(datos) => element.setState(datos)}
                    options={
                      element.tipoProtrotocolo
                        ? // ? console.log(stateRedux[element.reduxStateName].data.Data.filter((i) => i.Id_Tipo === element.tipoProtrotocolo))
                          stateRedux[element.reduxStateName].data.Data.filter((i) => i.Id_Tipo === element.tipoProtrotocolo)
                            .filter((it) => it.Id_Tipo === element.tipoProtrotocolo)
                            .map((item) => {
                              return { ...item, label: item[element.optionsInRedux], value: item[element.optionsInRedux] };
                            })
                        : stateRedux[element.reduxStateName].data.Data.map((item) => {
                            return { ...item, label: item[item.optionsInRedux], value: item[item.optionsInRedux] };
                          })
                    }
                    defaultOptions={
                      element.tipoProtrotocolo
                        ? stateRedux[element.reduxStateName].data.Data.filter((i) => i.Id_Tipo === element.tipoProtrotocolo)
                            .filter((it) => it.Id_Tipo === element.tipoProtrotocolo)
                            .map((item) => {
                              return { ...item, label: item[element.optionsInRedux], value: item[element.optionsInRedux] };
                            })
                        : stateRedux[element.reduxStateName].data.Data.map((item) => {
                            return { ...item, label: item[element.optionsInRedux], value: item[element.optionsInRedux] };
                          })
                    }
                    placeholder={`Seleccione uno`}
                  />
                </div>
              );
            }
            return "";
          })}
        {allAreasOption.length !== 0 && (
          <div className="wrapper_selectInput_gestionDeEquipo">
            <label>Area:</label>
            <ReactSelect
              Searchable={false}
              isMulti={false}
              fn={(datos) => setida([datos])}
              options={allAreasOption.map((item) => {
                return { ...item, label: item.Area, value: item.Area };
              })}
              defaultOptions={allAreasOption.map((item) => {
                return { ...item, label: item.Area, value: item.Area };
              })}
              placeholder={`Seleccione uno`}
            />
          </div>
        )}
        {allSectorOption.length !== 0 && (
          <div className="wrapper_selectInput_gestionDeEquipo">
            <label>SECTOR</label>
            <ReactSelect
              Searchable={false}
              isMulti={false}
              fn={(datos) => setids([datos])}
              options={allSectorOption.map((item) => {
                return { ...item, label: item.Sector, value: item.Sector };
              })}
              defaultOptions={allSectorOption.map((item) => {
                return { ...item, label: item.Sector, value: item.Sector };
              })}
              placeholder={`Seleccione uno`}
            />
          </div>
        )}
        {lastState.DetalleEquipo &&
          lastState.codigoEquipo &&
          lastState.nombreEquipo &&
          lastState.serielEquipo &&
          lastState.ida.length !== 0 &&
          lastState.idg.length !== 0 &&
          lastState.idpi.length !== 0 &&
          lastState.idpp.length !== 0 &&
          lastState.idmm.length !== 0 &&
          lastState.idpm.length !== 0 &&
          lastState.ids.length !== 0 &&
          lastState.idtipo.length !== 0 && (
            <div className="wrapper_selectInput_gestionDeEquipo">
              <button onClick={crearEquipo}>Crear equipo</button>
            </div>
          )}
      </div>
    </Container>
  );
};
