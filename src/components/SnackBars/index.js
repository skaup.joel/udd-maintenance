import React from "react";
import Snackbar from "@material-ui/core/Snackbar";
import MuiAlert from "@material-ui/lab/Alert";

function Alert(props) {
  return <MuiAlert elevation={6} variant="filled" {...props} />;
}

export const SnackBars = (props) => {
  props = { ...mockData, ...props };
  const [open, setOpen] = React.useState(props.isOpen);

  const handleClose = (event, reason) => {
    if (reason === "clickaway") {
      return;
    }
    setOpen(false);
  };

  return (
    <React.Fragment>
      <Snackbar
      open={open} autoHideDuration={6000} onClose={handleClose}>
        <Alert onClose={handleClose} severity={props.type}>
          {/* error, warning, info, success */}
          {props.message}
        </Alert>
      </Snackbar>
    </React.Fragment>
  );
};

const mockData = {
  isOpen: true,
  type: "info",
  message: "Mensaje por defecto !",
};
