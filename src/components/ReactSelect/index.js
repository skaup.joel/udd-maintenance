import React, { Component } from "react";
import styled from "styled-components";
import AsyncSelect from "react-select/async";

const optionsExpamle = [
  { value: "ocean", label: "Ocean", color: "#00B8D9", isFixed: true },
  { value: "blue", label: "Blue", color: "#0052CC", isDisabled: true },
  { value: "slate", label: "Slate", color: "#253858" },
  { value: "silver", label: "Silver", color: "#666666" },
];
const defaultOptionsExample = [
  { value: "slate", label: "Slate", color: "#253858" },
  { value: "silver", label: "Silver", color: "#666666" },
];
const AsyncSelectStyked = styled(AsyncSelect)`
  width: 100%;
`;

export default class WithPromises extends Component {
  state = {
    inputValue: "",
    value: null,
    options: this.props.options || optionsExpamle,
    defaultOptions:
      this.props.defaultOptions || this.props.options || defaultOptionsExample,
    placeholder: this.props.placeholder || "Select...",
    fn: this.props.fn || null,
  };
  filter = (inputValue) => {
    return this.state.options.filter((i) =>
      i.label.toLowerCase().includes(inputValue.toLowerCase())
    );
  };

  promiseOptions = (inputValue) =>
    new Promise((resolve) => {
      setTimeout(() => {
        resolve(this.filter(inputValue));
      }, 500);
    });

  render() {
    return (
      <AsyncSelectStyked
        value={this.state.value || this.props.value}
        isSearchable={this.props.Searchable}
        isMulti={this.props.isMulti}
        placeholder={this.state.placeholder}
        cacheOptions
        defaultOptions={this.state.defaultOptions}
        onChange={(data) => {
          this.state.fn(data);
          this.setState({ value: data });
        }}
        loadOptions={this.promiseOptions}
      />
    );
  }
}
