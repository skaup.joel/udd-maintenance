import React, { useState, useEffect } from "react";
import { KeyboardDatePicker } from "@material-ui/pickers";

export const DatePicker = (props) => {
  props = { ...mocks, ...props };
  const { fn, disableFuture, disablePast, defaultData } = props;
  const [selectedDate, handleDateChange] = useState(defaultData);
  useEffect(() => {
    // eslint-disable-next-line react-hooks/exhaustive-deps
    fn(selectedDate);
    // eslint-disable-next-line react-hooks/exhaustive-deps
  }, [selectedDate]);
  return (
    <KeyboardDatePicker
      onChange={(date) => handleDateChange(date)}
      disableFuture={disableFuture}
      disablePast={disablePast}
      clearable
      placeholder="2018/10/10"
      value={selectedDate}
      format="dd/MM/yyyy"
    >
      {props.children}
    </KeyboardDatePicker>
  );
};

const mocks = { fn: (data) => null, disableFuture: false, disablePast: false, defaultData: new Date() };
