import React, { Fragment, useEffect } from "react";
import { Table } from "./../../components/TableCustom";
import { Spiner } from "./../../components/Spiner";
import { useHistory } from "react-router-dom";
import { useDispatch, useSelector } from "react-redux";
import * as actions from "../../state/actions/usersAdmin";
import Button from "@material-ui/core/Button";

export const UsersList = () => {
  let history = useHistory();
  const userSession = useSelector(state => state.sessionReducer.userData.User);
  const stateRequesListUsers = useSelector(state => state.usersAdmin);
  const dispatch = useDispatch();
  const goTo = (pathname, state) =>
    history.push({
      pathname,
      state,
    });
  useEffect(() => {
    actions.getUsersDbs(dispatch, { userSession });
  }, [dispatch, userSession]);
  const { data, isLoading, error } = stateRequesListUsers;

  if (isLoading) return <Spiner />;
  if (error) return <h1>tenermos un error</h1>;
  return (
    <Fragment>
      <Button
        style={{ margin: "1em 0em" }}
        onClick={() => goTo("/administrar/usuarios/userCreate")}
        variant="outlined"
      >
        Crear Usuario
      </Button>
      <div>
        {data && (
          <Table
            fnEditar={data => {
              console.log({ data });
              goTo("/administrar/usuarios/upDataUser", data);
            }}
            {...{ headers, data: data.Data }}
          />
        )}
      </div>
    </Fragment>
  );
};

const headers = ["Nombre", "Email", "Telefono", "Rol", "Cliente"];
