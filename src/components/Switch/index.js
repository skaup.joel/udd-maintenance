import React, { useEffect } from "react";
import Switch from "@material-ui/core/Switch";

export const Switches = (props) => {
  props = { ...mockData, ...props };
  const { fn, text } = props;
  const [state, setState] = React.useState({
    checkedB: false,
  });
  useEffect(() => {
    fn(state.checkedB);
  }, [fn, state]);
  const handleChange = (event) => {
    setState({ ...state, [event.target.name]: event.target.checked });
  };

  return (
    <div>
      <span>{text}</span>
      <Switch checked={state.checkedB} onChange={handleChange} color="primary" name="checkedB" inputProps={{ "aria-label": "primary checkbox" }} />
    </div>
  );
}

const mockData = {
  fn: () => null,
  text:''
};
