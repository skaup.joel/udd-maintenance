import ReactDOM from "react-dom";
const $portal = document.getElementById("portals");

export default function Portla(props) {
  const { children } = props;
  return ReactDOM.createPortal(children, $portal);
}
