import React, { useState } from "react";
import TextField from "@material-ui/core/TextField";
import { useHistory } from "react-router-dom";
import styled from "styled-components";
import { v4 as uuidv4 } from "uuid";
import { useSelector } from "react-redux";
import Button from "@material-ui/core/Button";
const FormContainer = styled.form`
  display: flex;
  flex-direction: column;
  width: 35em;
  padding: 1em 2em 2em 2em;
  border: 1px solid;
  border-radius: 10px;
  & > button {
    margin-top: 1em;
  }
  .input-form {
    padding: 1em 0em;
  }
`;
const options = {
  defaultValue: null,
  autoFocus: false,
  required: true,
  type: "text",
  label: "hola",
  name: "holi",
};

export const Forms = props => {
  const userSession = useSelector(state => state.sessionReducer.userData.User);
  const { Other } = props;
  const [actualizarCrearCliente, borrarClientes] = props.functions;
  let history = useHistory();

  const [stateForm, setStateForm] = useState(
    props.fromBase ? props.fromBase : history.location.state
  );
  const hadlerForm = (name, data) => {
    setStateForm({ ...stateForm, [name]: data });
  };

  const handlerSubmit = e => {
    e.preventDefault();
  };

  const processFunction = async fn => {
    const res = await fn;
    history.push({
      pathname: "/administrar/usuarios",
      state: {
        isOpen: true,
        type: res.Status === "OK" ? "success" : "error",
        message:
          res.Status === "OK"
            ? "La operación fue un exito"
            : "Tuvimos problemas con la operación",
      },
    });
  };

  return (
    <div
      style={{
        height: "100%",
        width: "100%",
        justifyContent: "center",
        alignItems: "center",
        display: "flex",
      }}
    >
      <FormContainer onSubmit={handlerSubmit} noValidate autoComplete="off">
        {Object.keys(stateForm).map((item, i) => {
          if (props.formNotShow.indexOf(item) !== -1) return "";
          return (
            <TextField
              className={"input-form"}
              {...options}
              onChange={e => hadlerForm(item, e.target.value)}
              label={item}
              name={item}
              defaultValue={stateForm[item]}
              id={uuidv4()}
              key={`${i}input`}
            />
          );
        })}
        {Other ? <Other stateForm={stateForm} /> : props.children}
        {props.type === "CREATE" && (
          <Button
            variant="contained"
            color="primary"
            onClick={() => {
              if (props.newFunctionMaster)
                return props.newFunctionMaster({
                  finalFunction: actualizarCrearCliente,
                  stateForm,
                });
              processFunction(actualizarCrearCliente(userSession, stateForm));
            }}
          >
            Crear
          </Button>
        )}
        {props.type === "UPDATE" && (
          <Button
            variant="contained"
            color="primary"
            onClick={() => {
              if (props.newFunctionMaster)
                return props.newFunctionMaster({
                  finalFunction: actualizarCrearCliente,
                  stateForm,
                });
              processFunction(actualizarCrearCliente(userSession, stateForm));
            }}
          >
            Actualizar
          </Button>
        )}
        {props.type === "UPDATE" && (
          <Button
            variant="contained"
            color="primary"
            onClick={() =>
              processFunction(
                borrarClientes(userSession, { id: history.location.state.Id })
              )
            }
          >
            Eliminar
          </Button>
        )}
      </FormContainer>
    </div>
  );
};
