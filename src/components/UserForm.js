import React, { useState } from "react";

export const UserForm = props => {
  let {
    fnCancel= ()=>alert('default'),
    fn = data => {
      alert(data);
    },
    username,
    password,
    isCreating = true
  } = props;
  const [state, setstate] = useState({ username, password });
  const hadlerSubmit = e => {
    e.preventDefault();
    fn(state);
    setstate({ username: "", password: "" });
  };
  return (
    <div style={{ justifyContent: "center", flexDirection: "column" }}>
      <h4 style={{ paddingBottom: "1.3em", textAlign: "center" }}>{isCreating ? "Creando Usuario" : "Modificando Usuario"}</h4>
      <form onSubmit={hadlerSubmit} style={{ ...styles.form }}>
        <input
        autocomplete="off"
          value={username ? username : state.username}
          placeholder="Username"
          type="text"
          onChange={e => setstate({ ...state, username: e.target.value })}
          style={{ ...styles.input }}
        />
        <input
        autocomplete="off"
          value={state.password}
          placeholder="Password"
          type="password"
          onChange={e => setstate({ ...state, password: e.target.value })}
          style={{ ...styles.input, margin: "1em" }}
        />
        <span style={{display:'flex'}}>
          <button style={{ marginRight: "1em" }} className="btn btn-outline-light btn-lg">
            {isCreating ? "Crear Usuario" : "Modificar credenciales"}
          </button>
          <span className="btn btn-outline-light btn-lg" onClick={() => {
            setstate({ username: "", password: "" })
            fnCancel(false)
          } }>
            Cancelar
          </span>
        </span>
      </form>
    </div>
  );
};

const styles = {
  form: { flexDirection: "column", display: "flex", justifyContent: "center", alignItems: "center" },
  input: {
    background: "transparent",
    border: "solid 1px white",
    borderRadius: "5px",
    color: "white",
    fontSize: "1.7em"
  }
};
