import React from "react";

export const Table = (props) => {
  const data = { ...mock, ...props };
  return (
    <div style={{ ...data.style }}>
      <table className="table table-striped">
        <thead>
          <tr>
            {data.headers.map((header) => (
              <th style={{ ...styles.allText }} scope="col">
                {header}
              </th>
            ))}
            <th style={{ ...styles.allText }} scope="col">
              Accion
            </th>
          </tr>
        </thead>
        <tbody>
          {data.data.map((datos) => (
            <tr>
              {data.headers.map((item, i) => (
                <td style={{ ...styles.allText }}>{datos[item] || "No-Data"}</td>
              ))}
              <td style={{ ...styles.allText }}>
                <button className=" btn btn-outline-light" onClick={() => data.fnEditar(datos)}>
                  {data.textToButton}
                </button>
              </td>
            </tr>
          ))}
        </tbody>
      </table>
    </div>
  );
};

const styles = {
  allText: { textAlign: "center", color: "rgb(246, 244, 235)" },
};

const mock = {
  style: {},
  fnEditar: (datos) => alert(JSON.stringify(datos)),
  headers: ["example1", "example2", "example3", "example4"],
  data: [
    {
      example1: "th example1",
      example2: "th example2",
      example3: "th example3",
      example4: "th example4",
    },
    {
      example1: "th example1",
      example2: "th example2",
      example3: "th example3",
      example4: "th example4",
    },
  ],
  textToButton: "action Btn",
};
