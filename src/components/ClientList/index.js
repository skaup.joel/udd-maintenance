import React, { Fragment } from "react";
import { Table } from "./../../components/TableCustom";
import { useHistory } from "react-router-dom";
import { useHandlerApiFunction } from "../../hooks/stateCalled";
import Button from "@material-ui/core/Button";
import { Spiner } from "./../../components/Spiner";

export const ClientList = (props) => {
  let history = useHistory();
  const goTo = (pathname, state = {}) =>
    history.push({
      pathname,
      state,
    });

  const [error, data, isLoading] = useHandlerApiFunction(props.fn);

  return (
    <Fragment>
      {isLoading && <Spiner/>}
      {error && <h1>{error}</h1>}
      {data && (
        <>
          <Button style={{margin:'1em 0em'}} onClick={() => goTo(props.pathCreate)} variant="outlined">
            Crear
          </Button>
          <Table fnEditar={(data) => goTo(props.pathUpdate, data)} {...{ headers: props.headers, data: data.Data }} />
        </>
      )}
    </Fragment>
  );
};
