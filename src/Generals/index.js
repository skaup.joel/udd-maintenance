export const Meses = [
  {
    mes: "Enero",
    number: 1,
  },
  {
    mes: "Febrero",
    number: 2,
  },
  {
    mes: "Marzo",
    number: 3,
  },
  {
    mes: "Abril",
    number: 4,
  },
  {
    mes: "Mayo",
    number: 5,
  },
  {
    mes: "Junio",
    number: 6,
  },
  {
    mes: "Julio",
    number: 7,
  },
  {
    mes: "Agosto",
    number: 8,
  },
  {
    mes: "Septiembre",
    number: 9,
  },
  {
    mes: "Octubre",
    number: 10,
  },
  {
    mes: "Noviembre",
    number: 11,
  },
  {
    mes: "Diciembre",
    number: 12,
  },
];

export const annos = [
  {
    anno: "2020",
    number: 2020,
  },
  {
    anno: "2021",
    number: 2021,
  },
  {
    anno: "2022",
    number: 2022,
  },
  {
    anno: "2023",
    number: 2023,
  },
];

export const getJsonFromUrl = (url) => {
  if (!url) url = window.location.search;
  var query = url.substr(1);
  var result = {};
  query.split("&").forEach(function (part) {
    var item = part.split("=");
    result[item[0]] = decodeURIComponent(item[1]);
  });
  return result;
};

export const dateSplit = () => {
  const DataActual = new Date();
  const dia = DataActual.getDate();
  const mes = DataActual.getMonth() + 1;
  const anno = DataActual.getFullYear();
  return {
    dia,
    mes,
    anno,
  };
};
