import { createGlobalStyle } from "styled-components";

export const GlobalStyle = createGlobalStyle`
  .Global_ContainerRoute{
    overflow: auto;
    position: relative;
    width: 100%;
    height: 95%;
    border: 1.5px solid rgb(0,0,0,0.2);;
    border-radius: 10px;
    padding: 1em 2em;
  }
  [type=button]:focus{
  outline:none;
}
  
`;
