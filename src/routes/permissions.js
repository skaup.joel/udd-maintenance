import {
  ADMIN,
  PLANER,
  ClienteA,
  ClienteB,
  ClienteC,
  Tecnico,
} from "./allRoles";

const planerRoutes = [
  "/",
  "/estadisticas",
  // "/administrar/usuarios",
  "/administrar/:typeAdministration/:toDo",
  "/work",
  "/work/:toDo",
  "/equipment/:toDo",
  "/equipment/:toDo/:idEquipment",
  "/reportes",
  "/reportes/AdminTarea",
  "/reportes/TareasRealizadas",
  "/protocolos",
  "/protocolos/create",
  "/protocolos/update",
];

const clientARoutes = [
  "/",
  "/estadisticas",
  // "/administrar/usuarios",
  // "/administrar/:typeAdministration/:toDo",
  // "/work",
  // "/work/:toDo",
  // "/equipment/:toDo",
  // "/equipment/:toDo/:idEquipment",
  "/reportes",
  // "/protocolos",
  // "/protocolos/create",
  // "/protocolos/update",
];

const clientBRoutes = [
  "/",
  "/estadisticas",
  // "/administrar/usuarios",
  // "/administrar/:typeAdministration/:toDo",
  "/work",
  "/work/:toDo",
  // "/equipment/:toDo",
  // "/equipment/:toDo/:idEquipment",
  "/reportes",
  // "/protocolos",
  // "/protocolos/create",
  // "/protocolos/update",
];

const clientCRoutes = [
  "/",
  "/estadisticas",
  // "/administrar/usuarios",
  // "/administrar/:typeAdministration/:toDo",
  "/work",
  "/work/:toDo",
  // "/equipment/:toDo",
  // "/equipment/:toDo/:idEquipment",
  "/reportes",
  // "/protocolos",
  // "/protocolos/create",
  // "/protocolos/update",
];

const TecnicoRoutes = [
  "/",
  // "/estadisticas",
  // "/administrar/usuarios",
  // "/administrar/:typeAdministration/:toDo",
  // "/work",
  // "/work/:toDo",
  // "/equipment/:toDo",
  // "/equipment/:toDo/:idEquipment",
  // "/reportes",
  // "/protocolos",
  // "/protocolos/create",
  // "/protocolos/update",
];

export const getRoutesByRole = (routes, role) => {
  switch (role) {
    case ADMIN:
      return routes; //.filter(route => adminRoutes.includes(route.path));
    case PLANER:
      return routes.filter(route => planerRoutes.includes(route.path));
    case ClienteA:
      return routes.filter(route => clientARoutes.includes(route.path));
    case ClienteB:
      return routes.filter(route => clientBRoutes.includes(route.path));
    case ClienteC:
      return routes.filter(route => clientCRoutes.includes(route.path));
    case Tecnico:
      return routes.filter(route => TecnicoRoutes.includes(route.path));
    default:
      return [];
  }
};
