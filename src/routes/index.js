import { allRoutes } from './allRoutes'
import { getRoutesByRole } from './permissions'

export const getRoutes = role => getRoutesByRole(allRoutes, role);
export default allRoutes;