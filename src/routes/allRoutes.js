import { Stadisticas } from "../screens/Stadisticas";
import { Home } from "../screens/Home";
import { Reportes } from "../screens/Reportes";
import { Usuarios } from "../screens/Usuarios";
import { UsersActions } from "../screens/UsersActions";
import { Work } from "../screens/Work";
import { WorkActions } from "../screens/WorkActions";
import { Equipment } from "../screens/Equipment";
import { Protocolos } from "../screens/Protocolos";
import { ProtocolCreate } from "../screens/Protocolos/ProtocolCreate";
import { ProtocolUpdate } from "../screens/Protocolos/ProtocolUpdate";
import { TareasRealizadas } from "screens/Reportes/TareasRealizadas";
import { AdminTarea } from "screens/Reportes/AdminTarea";
//Icons
import { AiOutlineHome, AiOutlineFileSearch } from "react-icons/ai";
import { IoMdStats } from "react-icons/io";
import { GoCalendar } from "react-icons/go";
import { FiUsers } from "react-icons/fi";

export const allRoutes = [
  {
    id: "Home",
    pathComponent: "screens/Home",
    path: "/",
    component: Home,
    exact: true,
    BtnPush: AiOutlineHome,
    isButtonNav: false,
  },
  {
    id: "Estadisticas",
    pathComponent: "screens/Stadisticas",
    path: "/estadisticas",
    component: Stadisticas,
    exact: true,
    BtnPush: IoMdStats,
    isButtonNav: true,
  },
  {
    id: "Usuarios",
    path: "/administrar/usuarios",
    pathComponent: "screens/Usuarios",
    component: Usuarios,
    exact: true,
    BtnPush: FiUsers,
    isButtonNav: true,
  },
  {
    id: "UsuariosToDo",
    pathComponent: "screens/UsersActions",
    path: "/administrar/:typeAdministration/:toDo", // listUsers or createUsers
    component: UsersActions,
    exact: true,
    isButtonNav: false,
  },
  {
    id: "Equipos",
    pathComponent: "screens/Work",
    path: "/work",
    component: Work,
    exact: true,
    BtnPush: AiOutlineFileSearch,
    isButtonNav: true,
  },
  {
    id: "WorkToDo",
    pathComponent: "screens/WorkActions",
    path: "/work/:toDo", // assignWork or createWork
    component: WorkActions,
    exact: true,
    isButtonNav: false,
  },
  {
    id: "EquipmentToDo",
    pathComponent: "screens/Equipment",
    path: "/equipment/:toDo", // CRUD
    component: Equipment,
    exact: true,
    isButtonNav: false,
  },
  {
    id: "EquipmentToDo",
    pathComponent: "screens/Equipment",
    path: "/equipment/:toDo/:idEquipment", // CRUD
    component: Equipment,
    exact: true,
    isButtonNav: false,
  },
  {
    id: "Reportes",
    pathComponent: "screens/Reportes",
    path: "/reportes",
    component: Reportes,
    exact: true,
    BtnPush: GoCalendar,
    isButtonNav: true,
  },
  {
    id: "AdminTarea",
    pathComponent: "screens/Reportes",
    path: "/reportes/AdminTarea",
    component: AdminTarea,
    exact: true,
    BtnPush: GoCalendar,
    isButtonNav: false,
  },
  {
    id: "TareasRealizadas",
    pathComponent: "screens/Reportes",
    path: "/reportes/TareasRealizadas",
    component: TareasRealizadas,
    exact: true,
    BtnPush: GoCalendar,
    isButtonNav: false,
  },
  {
    id: "Protocolos",
    pathComponent: "screens/Protocolos",
    path: "/protocolos",
    component: Protocolos,
    exact: true,
    BtnPush: GoCalendar,
    isButtonNav: true,
  },
  {
    id: "ProtocolosCreate",
    pathComponent: "screens/Protocolos",
    path: "/protocolos/create",
    component: ProtocolCreate,
    exact: true,
    BtnPush: GoCalendar,
    isButtonNav: false,
  },
  {
    id: "ProtocolosUpdate",
    pathComponent: "screens/Protocolos",
    path: "/protocolos/update",
    component: ProtocolUpdate,
    exact: true,
    BtnPush: GoCalendar,
    isButtonNav: false,
  },
];
