// hooks.js
import { useState, useEffect } from "react";

function useStateCustom(data, name) {
  const [stateCustom, setState] = useState({});
  setState({ ...stateCustom, [name]: data });
  return [stateCustom];
}

export { useStateCustom };
