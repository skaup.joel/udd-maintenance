import { useState, useEffect } from "react";

export const useHandlerApiFunction = (callFromApi) => {
  const [error, setStateNull] = useState(null);
  const [data, setStateData] = useState(null);
  const [isLoading, setStateIsLoading] = useState(true);
  useEffect(() => {
    const response = async () => {
      const res = await callFromApi;
      if (res.Status === "OK") {
        setStateNull(null);
        setStateData(res);
        setStateIsLoading(false);
      } else {
        setStateNull("Tuvimos un error con el servidor");
        setStateData(null);
        setStateIsLoading(false);
      }
    };
    response();
  // eslint-disable-next-line react-hooks/exhaustive-deps
  }, []);
  return [error, data, isLoading];
};
