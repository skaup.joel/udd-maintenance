import React from "react";
import { Conatiner, WrapperButtoons } from "./styled";
import { useHistory } from "react-router-dom";
import { ButtonCustom as Button } from "./../../components/ButtonCustom";
import { SnackBars } from "../../components/SnackBars";
import { useSelector } from "react-redux";
import { ClienteC, ClienteB } from "routes/allRoles";
export const Work = (props) => {
  const Perfil = useSelector((state) => state.sessionReducer.userData.Perfil);
  let history = useHistory();
  const goTo = (ruta) => history.push(ruta, props.location.state);
  // if (Perfil === ClienteC || Perfil === ClienteB) {
  //   goTo("/work/assignwork");
  // }

  return (
    <Conatiner className="Global_ContainerRoute">
      <WrapperButtoons>
        {(Perfil !== ClienteC && Perfil !== ClienteB) && (
          <div>
            <Button onClick={() => goTo("/equipment/listarEquipos")}>
              <span>Administrar Equipos</span>
            </Button>
          </div>
        )}
        <div>
          <Button onClick={() => goTo("/work/assignwork")}>
            <span>Asignar Tarea</span>
          </Button>
        </div>
      </WrapperButtoons>
      {props.location.state && <SnackBars {...props.location.state} />}
    </Conatiner>
  );
};
