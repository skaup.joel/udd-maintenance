import React, { useState, useEffect } from "react";
import { GetClientesSucursales, TiposProtocolos, TipoEquipo, TiposDeRespuestas, detalle_protocolo } from "api";
import { ProtocolCreate } from "screens/Protocolos/ProtocolCreate";
import { useSelector } from "react-redux";

export const ProtocolUpdate = (props) => {
  const userSession = useSelector((state) => state.sessionReducer.userData.User);
  const { location } = props;
  const [detailProtocol, setstateDatailProtocol] = useState(null);
  const [allPromises, setdataAllPromises] = useState(null);
  const [dataReady, setdataReady] = useState(null);

  useEffect(() => {
    detalle_protocolo(userSession, { id: location.state.protocolSelected.Id })
      .then((data) => setstateDatailProtocol(data))
      .catch((err) => console.log(err));

    const promises = [GetClientesSucursales(userSession), TiposProtocolos(userSession), TipoEquipo(userSession), TiposDeRespuestas(userSession)].map((promesa) => {
      return promesa.then((item) => item.json());
    });
    let primses = [];
    Promise.all(promises)
      .then((res) =>
        res.forEach(async (item) => {
          const { Data } = await item;
          primses.push(Data);
        })
      )
      .catch((err) => console.log({ err }))
      .finally(() => {
        setdataAllPromises(primses);
      });
    // eslint-disable-next-line react-hooks/exhaustive-deps
  }, []);

  const finding = (array, toFind, name) => {
    const datos = array.find(({ Id }) => Id === toFind);
    return { ...datos, label: datos[name], value: datos.Id };
  };

  useEffect(() => {
    if (detailProtocol && allPromises) {
      const [allClientes, allTiposDeProtocolos, allTiposDeEquipos, allTiposDeRespuestas] = allPromises;
      const { Protocolo: nombreDelProtocolo } = detailProtocol.Data[0];
      let aa = {};
      detailProtocol.Data.forEach((item, i) => {
        const { Capitulo } = item;
        aa[Capitulo] = {
          ...aa[Capitulo],
          [i]: { ...item },
        };
      });
      aa = Object.keys(aa).map((item, i) => {
        // const capitulos = Object.keys(item);
        const key = Object.keys(aa[item])[0];
        return {
          descripcion: detailProtocol.Data[key].DescCapitulo,
          inputs: Object.keys(aa[item]).map((i) => {
            const { Capitulo, Captura, Id_TipoRespuesta } = aa[item][i];
            return { cap: Capitulo - 1, inputState: Captura, selecState: JSON.stringify(finding(allTiposDeRespuestas, Id_TipoRespuesta, "TipoEquipo")) };
          }),
        };
      });

      const defaultDataProtocol = {
        capitulos: aa, //ok
        cliente: finding(allClientes, detailProtocol.Id_Cliente, "Cliente"), //{ Id: 3, Cliente: "MINERA", label: "MINERA", value: 3 }, // no
        descripcionProtocolo: nombreDelProtocolo, //ok
        tipeDeEquipo: finding(allTiposDeEquipos, detailProtocol.Id_TipoEquipo, "TipoEquipo"), // { Id: 1, TipoEquipo: "ATAQUE RAPIDO", label: "ATAQUE RAPIDO", value: 1 }, // no
        tipoDeProtocolo: finding(allTiposDeProtocolos, detailProtocol.Id_TipoProtocolo, "TipoProtocolo"), //{ Id: 1, TipoProtocolo: "Servicio de Inspeccion", label: "Servicio de Inspeccion", value: 1 }, // no
      };
      setdataReady(defaultDataProtocol);
    }
  }, [detailProtocol, allPromises]);

  return <>{dataReady && detailProtocol && allPromises && <ProtocolCreate IpProtocolo={location.state.protocolSelected.Id} dataReady={dataReady}></ProtocolCreate>}</>;
};
