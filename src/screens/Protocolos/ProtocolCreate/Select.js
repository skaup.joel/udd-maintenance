import React from "react";
import Select from "@material-ui/core/Select";
import MenuItem from "@material-ui/core/MenuItem";

export default function Selects(props) {
  props = { ...mocksSelects, ...props };
  const { options, defaultValue, className, id, style } = props;
  const [optionSelected, setOptionSelected] = React.useState(defaultValue || JSON.stringify(options[0]));
  const [ops] = React.useState(props.ops || options);

  const handleChange = (event) => {
    setOptionSelected(event.target.value);
  };

  return (
    <Select style={style} labelId={id} className={className} id={id} value={optionSelected} onChange={handleChange} displayEmpty>
      {ops.map((item, i) => (
        <MenuItem key={`${i}-Select-cap`} value={JSON.stringify(item)}>
          {item.label}
        </MenuItem>
      ))}
    </Select>
  );
}

const mocksSelects = {
  style:{},
  options: [
    { Id: "preuba", TipoEquipo: "ptueba", label: "pruen", value: 1 },
    { Id: "preuba", TipoEquipo: "ptueba", label: "pruen", value: 1 },
    { Id: "preuba", TipoEquipo: "ptueba", label: "pruen", value: 1 },
  ],
};
