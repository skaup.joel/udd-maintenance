import React, { useEffect, useState } from "react";
import { Modal } from "components/Modal";
import {
  GetClientesSucursales,
  TiposProtocolos,
  TipoEquipo,
  callCrearProtocolo,
} from "api";
import { useSelector } from "react-redux";
import Selects from "components/ReactSelect";
import TextField from "@material-ui/core/TextField";
import { useHistory } from "react-router-dom";

const toOr = (array) => {
  var find = ",";
  var re = new RegExp(find, "g");
  const newArr = array.map((item) => (item ? item : "no descrito"));
  return `${newArr}`.replace(re, "|");
};

const access = [
  {
    key: "Descricion",
    placeholder: "Nombre del protocolo",
    value: null,
  },
  {
    key: "Cliente",
    placeholder: "Selecciona un Cliente",
    value: null,
  },
  {
    key: "TipoProtocolo",
    placeholder: "Selecciona un TipoProtocolo",
    value: null,
  },
  {
    key: "TipoEquipo",
    placeholder: "Selecciona un TipoEquipo",
    value: null,
  },
];

const setOptions = (obj, name) => {
  return obj.map((item) => {
    return { ...item, label: item[name], value: item.Id };
  });
};

const ModalCreateProtocol = (props) => {
  props = { ...mocks, ...props };
  let history = useHistory();

  const { getCapitulos, stateInitialProtocol } = props;
  const userSession = useSelector(
    (state) => state.sessionReducer.userData.User
  );
  const {
    descripcionProtocolo,
    cliente,
    tipoDeProtocolo,
    tipeDeEquipo,
  } = stateInitialProtocol;

  const [state_descripcionProtocolo] = useState(descripcionProtocolo || null);
  const [state_cliente, setstate_cliente] = useState(cliente || null);
  const [state_tipoDeProtocolo, setstate_tipoDeProtocolo] = useState(
    tipoDeProtocolo || null
  );
  const [state_tipeDeEquipo, setstate_tipeDeEquipo] = useState(
    tipeDeEquipo || null
  );

  const prevData = [
    state_descripcionProtocolo,
    state_cliente,
    state_tipoDeProtocolo,
    state_tipeDeEquipo,
  ];
  const setStates = [
    null,
    setstate_cliente,
    setstate_tipoDeProtocolo,
    setstate_tipeDeEquipo,
  ];
  const [Arrays, setArrays] = useState({
    isLoading: true,
    data: null,
    error: null,
  });

  useEffect(() => {
    let llamadasIniciales = [
      GetClientesSucursales,
      TiposProtocolos,
      TipoEquipo,
    ];
    let promesas = llamadasIniciales.map(async (promesa) => {
      let response = await promesa(userSession);
      return response.json();
    });
    Promise.all(promesas)
      .then((arraysData) => {
        const datos = {
          error: null,
          isLoading: false,
          data: arraysData.map((item) => item.Data),
        };
        setArrays(datos);
      })
      .catch((error) => setArrays({ isLoading: false, error, data: null }));
  }, [userSession]);

  const crearProtocolo = async (dataProtocolo, IpProtocolo) => {
    const {
      tipoDeProtocolo,
      descripcionProtocolo,
      tipeDeEquipo,
      cliente,
      capitulos,
    } = dataProtocolo;
    const creteProt = {
      id: IpProtocolo || 0,
      tipoprotocolo: tipoDeProtocolo.Id,
      descripcion: descripcionProtocolo
        .replace(/[;,:]/g, "/")
        .replace(/[']/g, "´"),
      tipoequipo: tipeDeEquipo.Id,
      cliente: cliente.Id,
      ncapitulos: capitulos.length,
      desccapitulo: toOr(
        capitulos.map((item) =>
          item.descripcion.replace(/[;,:]/g, "/").replace(/[']/g, "´")
        )
      ),
      capitulo_esvarios: toOr(capitulos.map(() => "0")),
      ncapturas: capitulos.flatMap((item) => item.inputs).length,
      desccaptura: toOr(
        capitulos
          .flatMap((item) => item.inputs)
          .map((i) => i.inputState.replace(/[;,:]/g, "/").replace(/[']/g, "´"))
      ),
      capitulocaptura: toOr(
        capitulos.flatMap((item) => item.inputs).map((i) => i.cap + 1)
      ),
      respuestacaptura: toOr(
        capitulos
          .flatMap((item) => item.inputs)
          .map((i) => JSON.parse(i.selecState).Id)
      ),
    };
    let todoBn;
    callCrearProtocolo(userSession, creteProt)
      .then((res) => res.json())
      .then(() => {
        todoBn = true;
      })
      .catch((error) => {
        todoBn = false;

        console.error("Error:", error);
      })
      .finally(() => {
        history.push({
          pathname: "/protocolos",
          state: {
            isOpen: true,
            type: todoBn ? "success" : "error",
            message: todoBn
              ? `Protocolo asignado con exito `
              : "Protocolo no asignado intente mas tarde",
          },
        });
      });
  };

  const handlerPostProtocol = () => {
    const dataFinal = {
      capitulos: getCapitulos(),
      cliente: state_cliente,
      descripcionProtocolo: document.getElementById("nombreDelProtocolo").value,
      tipeDeEquipo: state_tipeDeEquipo,
      tipoDeProtocolo: state_tipoDeProtocolo,
    };
    crearProtocolo(dataFinal, props.IpProtocolo);
  };
  if (Arrays.isLoading) return "";
  return (
    <>
      <Modal
        className="btn btn-warning"
        fn={handlerPostProtocol}
        showCancelBtn={true}
        btnIsDisabled={/*btnIsDisabled*/ false}
        inicialStateModal={false}
        textButton="Crear Protocolo"
        ModalTitle={`Informacion general de Protocolo`}
        TextToDoBtn="Aplicar"
      >
        <div
          style={{
            height: "14em",
            display: "flex",
            flexDirection: "column",
            justifyContent: "space-evenly",
          }}
        >
          <TextField
            id={`nombreDelProtocolo`}
            label={access[0].placeholder}
            variant="outlined"
            defaultValue={prevData[0]}
          />
          {Arrays.data.map((array, i) => (
            <Selects
              value={prevData[i + 1]}
              isMulti={false}
              fn={(data) => setStates[i + 1](data)}
              options={setOptions(array, access[i + 1].key)}
              defaultOptions={setOptions(array, access[i + 1].key)}
              placeholder={access[i + 1].placeholder}
            />
          ))}
        </div>
      </Modal>
    </>
  );
};

const mocks = {
  getCapitulos: () => null,
};

export default ModalCreateProtocol;
