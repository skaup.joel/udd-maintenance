import React, { useState, useEffect } from "react";
import TextField from "@material-ui/core/TextField";
import Selects from "./Select";
import { TiposDeRespuestas } from "api";
import { v4 as uuidv4 } from "uuid";
import ModalCreateProtocol from "./ModalCreateProtocol";
import { useSelector } from "react-redux";

const creandoProtocolo = {
  capitulos: [
    {
      descripcion: "",
      inputs: [
        {
          cap: 0,
          inputState: "",
          selecState: '{"Id":2,"TipoEquipo":"Fecha","label":"Fecha","value":2}',
        },
      ],
    },
  ],
  cliente: null,
  descripcionProtocolo: null,
  tipeDeEquipo: null,
  tipoDeProtocolo: null,
};

export const ProtocolCreate = (props) => {
  const [stateInitialProtocol, setStateProtocol] = useState(props.dataReady || creandoProtocolo);
  const [stateCap, setstateCap] = useState(0);
  const handlerAddCap = () => {
    const newstateInitialProtocol = { ...stateInitialProtocol };
    const numberNewCap = newstateInitialProtocol.capitulos.length + 1;
    const capStateInicial = { descripcion: "", inputs: [{ cap: numberNewCap, inputState: "", selecState: '{"Id":2,"TipoEquipo":"Fecha","label":"Fecha","value":2}' }] };
    const newsCapitulos = [...newstateInitialProtocol.capitulos, capStateInicial];
    newstateInitialProtocol.capitulos = newsCapitulos;
    setStateProtocol(newstateInitialProtocol);
  };

  const handlerDeleteCap = () => {
    if (stateCap === 0) return;
    const newstateInitialProtocol = { ...stateInitialProtocol };
    newstateInitialProtocol.capitulos = handlerGetData();
    let newsCapitulos = [];
    newstateInitialProtocol.capitulos.forEach((cap, i) => {
      if (i === stateCap) return 
      return newsCapitulos.push(cap);
    });
    newstateInitialProtocol.capitulos = newsCapitulos;
    setstateCap(stateCap === 0 ? 0 : stateCap - 1);
    setStateProtocol(newstateInitialProtocol);
  };
  const userSession = useSelector((state) => state.sessionReducer.userData.User);
  const [ops, setOps] = React.useState(null);

  useEffect(() => {
    TiposDeRespuestas(userSession)
      .then((res) => res.json())
      .then(({ Data }) => {
        const options = Data.map((item) => {
          const { TipoEquipo, Id } = item;
          return { ...item, label: TipoEquipo, value: Id };
        });
        setOps(options);
      });
  }, [userSession]);

  const handlerGetData = () => {
    const cantidadDeCapitulo = [...document.querySelectorAll(".caps")];
    const capitulos = cantidadDeCapitulo.map((item, indexCapitulo) => {
      const $base = item;
      const descripcion = $base.querySelector(`#inputTitleCap-${indexCapitulo}`).value;
      const valoresInputs = [...$base.querySelectorAll(".inputState>div>input")].map((item) => item.value);
      const valoresSelcts = [...$base.querySelectorAll(".selectState>input")].map((item) => item.value);

      const inputs = valoresInputs.map((value, index) => {
        return {
          cap: indexCapitulo,
          inputState: value,
          selecState: valoresSelcts[index],
        };
      });

      return { descripcion, inputs };
    });

    return capitulos;
  };
  const handlerDeleteInput = (index, arryDaddy, indexDady) => {
    if (arryDaddy.length === 1) return;
    let newstateInitialProtocol = { ...stateInitialProtocol };
    newstateInitialProtocol.capitulos = handlerGetData();

    let newInputs = [];
    newstateInitialProtocol.capitulos[indexDady].inputs.forEach((input, i) => {
      if (i === index) return;
      newInputs.push(input);
    });
    newstateInitialProtocol.capitulos[indexDady].inputs = newInputs;
    setStateProtocol(newstateInitialProtocol);
  };

  const handlerAddInput = (item, index) => {
    let newstateInitialProtocol = { ...stateInitialProtocol };
    newstateInitialProtocol.capitulos = handlerGetData();
    const dataNew = { cap: stateCap, inputState: "", selecState: `{"Id":2,"TipoEquipo":"Fecha","label":"Fecha","value":2}` };
    const newInputs = [...newstateInitialProtocol.capitulos[index].inputs, dataNew];
    newstateInitialProtocol.capitulos[index].inputs = newInputs;
    setStateProtocol(newstateInitialProtocol);
  };
  return (
    <div className="Global_ContainerRoute">
      <h4>Editando {stateInitialProtocol.descripcionProtocolo || "Nuevo Protocolo"}</h4>
      <div style={{ display: "flex", justifyContent: "flex-end" }}>
        <div>
          <button className="btn btn-primary" onClick={handlerAddCap}>
            Crear nuevo Capitulo
          </button>
        </div>
        <div className="mr-2 ml-2">
          <button className="btn btn-danger" onClick={handlerDeleteCap}>
            Elimina este Capitulo
          </button>
        </div>
        <div>
          <ModalCreateProtocol IpProtocolo={props.IpProtocolo} stateInitialProtocol={stateInitialProtocol} getCapitulos={handlerGetData} />
        </div>
      </div>
      <ul className="nav nav-tabs">
        {stateInitialProtocol.capitulos.map((item, i) => {
          return (
            <li className="nav-item" key={`${i}-liCap`}>
              <div className={`nav-link btn ${stateCap === i && " active"}`} onClick={() => setstateCap(i)}>
                {`Capitulo ${i + 1}`}
              </div>
            </li>
          );
        })}
      </ul>

      {stateInitialProtocol.capitulos.map((item, i) => {
        const { descripcion } = item;
        return (
          <div key={`${JSON.stringify(item)}`} style={{ flexDirection: "column" }} className={`${stateCap === i ? "d-flex" : "d-none"} caps capitulo${i}`}>
            <div>
              <button className="btn btn-primary mt-2" onClick={() => handlerAddInput(item.inputs, i)}>
                Añadir input
              </button>
            </div>
            <div className="mt-4 mb-2">
              <TextField id={`inputTitleCap-${i}`} label="Titulo de capitulo" defaultValue={descripcion} />
              {item.inputs.map((input, subI, arryDaddy) => {
                let { inputState, selecState } = input;
                return (
                  <div
                    className="mt-3"
                    style={{
                      height: "4em",
                      display: "flex",
                      alignItems: "baseline",
                    }}
                    key={`${JSON.stringify(input)}-${subI}`}
                    id={`inputsStateCap-${subI}`}
                  >
                    <TextField style={{ width: "30em", marginRight: "2em" }} className="inputState" id={`${uuidv4()}`} label="Descripcion de input" defaultValue={inputState} />
                    {ops && <Selects style={{ width: "30em" }} id={`${uuidv4()}`} className="selectState" ops={ops} defaultValue={selecState} />}
                    <button className="btn btn-warning ml-5 " onClick={() => handlerDeleteInput(subI, arryDaddy, i)}>
                      Eliminar
                    </button>
                  </div>
                );
              })}
            </div>
          </div>
        );
      })}
    </div>
  );
};
