import React, { useState, useEffect } from "react";
import WithPromises from "components/ReactSelect";
import { Modal } from "components/Modal";
import { getAllProtocolos, TiposProtocolos, duplicaProtocolo, listarClientes } from "api";
import { useSelector } from "react-redux";
import { SnackBars } from "../../components/SnackBars";

const AllProtocolsOptionsChildrenModal = (props) => {
  const { data, allTypeProtocol } = props;
  const [stateFilterTypeProtocol, setstateFilterTypeProtocol] = useState([1, 2, 3]);
  const [opts, setstateopts] = useState([]);
  const [protSelected, setprotSelected] = useState(null);
  const [stateClientesOtions, setstateClienteOptions] = useState(null);
  const [showOptionReplicar, setshowOptionReplicar] = useState(false);
  const [isLoading, setisLoading] = useState({
    isLoading: false,
    mensaje: "Replicar",
  });

  useEffect(() => {
    props.fn(null);
  // eslint-disable-next-line react-hooks/exhaustive-deps
  }, [stateFilterTypeProtocol])

  const [ClientToReplicar, setstateClientToReplicar] = useState(null);
  const handleChangeCliente = (data) => {
    console.log(data);
    setstateClientToReplicar(data);
  };

  const userSession = useSelector((state) => state.sessionReducer.userData.User);

  useEffect(() => {
    listarClientes(userSession).then(({ Data }) => {
      const data = Data.map((item) => {
        return { ...item, value: item.Id, label: item["Cliente"] };
      });
      setstateClienteOptions(data);
    });
    // eslint-disable-next-line react-hooks/exhaustive-deps
  }, []);
  useEffect(() => {
    let options = [];
    data.Data.forEach((item) => {
      const { Id_Tipo } = item;
      let isIn = stateFilterTypeProtocol.indexOf(Id_Tipo);
      if (isIn === -1) return;
      options.push({ ...item, label: item["Descripcion"], value: item.Id });
    });
    setstateopts(options);
  }, [stateFilterTypeProtocol, data]);

  const handlerCheck = (idToFilter) => {
    const indexDeArray = stateFilterTypeProtocol.indexOf(idToFilter);
    if (indexDeArray === -1) {
      setstateFilterTypeProtocol([...stateFilterTypeProtocol, idToFilter]);
    } else {
      let newArray = [];
      stateFilterTypeProtocol.forEach((item, index) => {
        if (index === indexDeArray) return;
        newArray.push(item);
      });
      setstateFilterTypeProtocol(newArray);
    }
  };
  const handlerReplicarProtocolo = () => {
    setisLoading({
      isLoading: true,
      mensaje: "Cargando ...",
    });
    duplicaProtocolo(userSession, { idprotocolo: protSelected.Id, cliente: ClientToReplicar.Id })
      .then(() => {
        setisLoading({
          isLoading: false,
          mensaje: "Listo!",
        });
      })
      .catch(() => {
        setisLoading({
          isLoading: false,
          mensaje: "Fallo !",
        });
      });
  };
  return (
    <>
      <span>Filtros por tipo de protocolo</span>
      <div style={{ display: "flex", flexDirection: "column", margin: "1em 0em" }}>
        {allTypeProtocol.map((item, index) => (
          <label>
            {item.TipoProtocolo}
            <input
              type="checkbox"
              defaultChecked={stateFilterTypeProtocol.find((i) => i === item.Id)}
              onChange={() => {
                handlerCheck(item.Id);
              }}
            />
          </label>
        ))}
      </div>
      {opts && (
        <WithPromises
          key={JSON.stringify(opts)}
          options={opts}
          defaultOptions={opts}
          fn={(data) => {
            setprotSelected(data);
            props.fn(data);
          }}
        />
      )}
      <label>
        Replicar Protocolo
        <input
          type="checkbox"
          defaultChecked={false}
          onChange={() => {
            setshowOptionReplicar(!showOptionReplicar);
          }}
        />
      </label>
      {showOptionReplicar && stateClientesOtions && protSelected && (
        <>
          <WithPromises key={JSON.stringify(stateClientesOtions)} options={stateClientesOtions} defaultOptions={stateClientesOtions} fn={handleChangeCliente} />
          {ClientToReplicar && (
            <button disabled={isLoading.isLoading} className="btn btn-warning" onClick={handlerReplicarProtocolo}>
              {isLoading.mensaje}
            </button>
          )}
        </>
      )}
    </>
  );
};

export const Protocolos = (props) => {
  const userSession = useSelector((state) => state.sessionReducer.userData.User);
  const cliente = useSelector((state) => (state.ClienteSelected ? state.ClienteSelected.Id : 0));

  const [allProtocols, setstateAllProtocols] = useState(null);
  const [protocolSelected, setstateProtocolSelected] = useState(null);
  const [allTypeProtocol, setstateAllTypeProtocol] = useState(null);
  const goTo = (path, state = {}) => {
    props.history.push(path, state);
  };
  useEffect(() => {
    getAllProtocolos(userSession, `&tipo=0&cliente=${cliente}`) //extra = login=admin&tipo=0&cliente=0
      .then((res) => res.json())
      .then((data) => setstateAllProtocols(data))
      .catch((err) => console.log(err));
    // eslint-disable-next-line react-hooks/exhaustive-deps
    TiposProtocolos(userSession)
      .then((res) => res.json())
      .then((data) => setstateAllTypeProtocol(data.Data))
      .catch((err) => console.log(err));
  }, [cliente, userSession]);

  return (
    <div
      className="Global_ContainerRoute"
      style={{
        display: "flex",
        justifyContent: "space-evenly",
        alignItems: "center",
      }}
    >
      <button className="btn btn-outline-secondary" onClick={() => goTo("protocolos/create")}>
        Crear Protocolo
      </button>
      <Modal
        className="btn btn-outline-secondary"
        fn={() => goTo("protocolos/update", { protocolSelected })}
        textButton={"Editar un protocolo"}
        ModalTitle={"Seleccione un protocolo para editar"}
        TextToDoBtn={"Editar"}
        TextToCancel={"Cancelar"}
        children={allProtocols && allTypeProtocol && <AllProtocolsOptionsChildrenModal allTypeProtocol={allTypeProtocol || []} fn={setstateProtocolSelected} data={allProtocols} />}
        showBtn={true}
        showCancelBtn={true}
        btnIsDisabled={!protocolSelected}
        btnOpenModalIsDisabled={!allProtocols ? true : false}
      />
      {props.location.state && <SnackBars {...props.location.state} />}
    </div>
  );
};
