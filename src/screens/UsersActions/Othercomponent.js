import React, { useEffect, useState } from "react";
import { useSelector } from "react-redux";
import { GetAllGerencia } from "api";
import { FiltroAreaGeneral } from "api/filtros_api";
import { SelectCustom } from "components/SelectCustom";

export default function Othercomponent({ gerneciaAndArea, cb }) {
  const userSession = useSelector(state => state.sessionReducer.userData.User);
  const [allOptionGerencias, setOptionGerencias] = useState(null);
  const [gerenciaSelected, setGerenciaSelected] = useState(null);

  const [allOptionArea, setOptionArea] = useState(null);
  const [areaSelected, setAreaSelected] = useState(null);

  useEffect(() => {
    cb(gerenciaSelected, areaSelected);
    // eslint-disable-next-line react-hooks/exhaustive-deps
  }, [gerenciaSelected, areaSelected]);

  useEffect(() => {
    if (gerenciaSelected) {
      if (gerenciaSelected.Id) {
        setOptionArea(null);
        FiltroAreaGeneral(userSession, { gerencia: gerenciaSelected.Id }).then(
          setOptionArea
        );
      }
    }
  }, [gerenciaSelected, userSession]);

  useEffect(() => {
    GetAllGerencia(userSession)
      .then(res => res.json())
      .then(setOptionGerencias);
  }, [userSession]);

  if (!allOptionGerencias) return <h1>Cargando</h1>;
  return (
    <div>
      <SelectCustom
        objKey="Gerencia"
        label="Gerencia"
        options={(allOptionGerencias && allOptionGerencias.Data) || []}
        handleChange={setGerenciaSelected}
      />
      {allOptionArea && gerneciaAndArea && (
        <SelectCustom
          objKey="Area"
          label="Area"
          options={(allOptionArea && allOptionArea.Data) || []}
          handleChange={setAreaSelected}
        />
      )}
    </div>
  );
}
