import React, { Fragment, useState, useEffect } from "react";
import { UserFormCreate } from "../../components/UserFormCreate";
import { UsersList } from "../../components/UsersList";
import { ClientList as TablaList } from "../../components/ClientList";
import { Forms } from "../../components/Forms";
import { SelectCustom } from "components/SelectCustom";
import { useHistory } from "react-router-dom";
import { FiltroAreaGeneral as filtrarArea } from "../../api/filtros_api";
import { useSelector } from "react-redux";
import Othercomponent from "./Othercomponent";
import {
  actualizarCrearCliente,
  borrarClientes,
  listarClientes,
  mantenerdorGerencia_USER,
  eliminarGerencia_USER,
  actualizarGerencia_USER,
  mantenerdorArea_USER,
  eliminarArea_USER,
  actualizarArea_USER,
  mantenerdorSectores_USER,
  actualizarSectores_USER,
  eliminarSectores_USER,
  GetAllGerencia,
} from "../../api";

const SelecteArea = props => {
  const userSession = useSelector(state => state.sessionReducer.userData.User);

  const [seleccionoGerencia, setSeleccionogerencia] = useState(false);
  const [seleccionoGerencia2, setSeleccionogerencia2] = useState(false);
  const handleChangeCliente = data => setSeleccionogerencia(data);
  const handleChangeCliente2 = data => setSeleccionogerencia2(data);

  const [stateAreaOtions, setstateAreaOptions] = useState(null);

  useEffect(() => {
    let selectedString = seleccionoGerencia.Id;
    filtrarArea(userSession, { gerencia: selectedString }).then(({ Data }) => {
      // {Id: 1, Gerencia: "GADM", Id_Cliente: 1, Cliente: "Andina"}

      const data = Data.map(item => {
        const { Id, Area } = item;
        return { Id, Area };
      });
      setstateAreaOptions(data);
    });
    // eslint-disable-next-line react-hooks/exhaustive-deps
  }, [seleccionoGerencia]);
  return (
    <>
      <SelectCustom
        {...props}
        style={{ width: "100%" }}
        defaultValue={props.defaultValue || props.stateGerenciaOtions[0]}
        handleChange={handleChangeCliente}
        label={"Selecciona un gerencia"}
        objKey={"Gerencia"}
        options={props.stateGerenciaOtions}
      />
      {props.show2select && stateAreaOtions && (
        <>
          <SelectCustom
            {...props}
            style={{ width: "100%" }}
            // defaultValue={stateAreaOtions[0]}
            handleChange={handleChangeCliente2}
            label={"Selecciona un area"}
            objKey={"Area"}
            options={stateAreaOtions}
          />
          <span style={{ zIndex: -5 }} id="areaSelectect-123">
            {seleccionoGerencia2 && JSON.stringify(seleccionoGerencia2)}
          </span>
        </>
      )}
    </>
  );
};

export const UsersActions = props => {
  const toDo = props.match.params.toDo;
  const typeAdministration = props.match.params.typeAdministration;
  const userSession = useSelector(state => state.sessionReducer.userData.User);
  const clienteSelected = useSelector(state => state.ClienteSelected);

  const [stateGerenciaOtions, setstateClienteOptions] = useState(null);
  useEffect(() => {
    GetAllGerencia(userSession)
      .then(res => res.json())
      .then(({ Data }) => {
        // {Id: 1, Gerencia: "GADM", Id_Cliente: 1, Cliente: "Andina"}

        const data = Data.map(item => {
          const { Id, Gerencia } = item;
          return { Id, Gerencia };
        });
        setstateClienteOptions(data);
      });
    // eslint-disable-next-line react-hooks/exhaustive-deps
  }, []);

  //gerencia y area

  let history = useHistory();

  const processFunction = async fn => {
    const res = await fn;
    history.push({
      pathname: "/administrar/usuarios",
      state: {
        isOpen: true,
        type: res.Status === "OK" ? "success" : "error",
        message:
          res.Status === "OK"
            ? "La operación fue un exito"
            : "Tuvimos problemas con la operación",
      },
    });
  };

  return (
    <div className="Global_ContainerRoute">
      {typeAdministration === "usuarios" && (
        <Fragment>
          {toDo === "userCreate" && (
            <UserFormCreate
              type="CREATE"
              showBtn={[false, true, false]}
              title={"Crear Usuario"}
              OtherComponent={Othercomponent}
            />
          )}
          {toDo === "upDataUser" && (
            <UserFormCreate
              type="UPDATE"
              showBtn={[true, false, true]}
              title={"Actualizar o Eliminar Usuario"}
            />
          )}
          {toDo === "userList" && <UsersList />}
        </Fragment>
      )}
      {typeAdministration === "clientes" && (
        <Fragment>
          {toDo === "clientList" && (
            <TablaList
              headers={[
                "Cant_Gerencias",
                "Cliente",
                "Contacto",
                "Direccion",
                "Email",
                "RUT",
                "Telefono",
              ]}
              pathCreate="/administrar/clientes/clientCreate"
              pathUpdate="/administrar/clientes/clientUpdate"
              fn={listarClientes(userSession)}
            />
          )}
          {toDo === "clientCreate" && (
            <Forms
              type="CREATE"
              functions={[actualizarCrearCliente, borrarClientes]}
              formNotShow={CLIENTE_formNotShow}
              fromBase={CLIENTE_createBase}
            />
          )}
          {toDo === "clientUpdate" && (
            <Forms
              type="UPDATE"
              functions={[actualizarCrearCliente, borrarClientes]}
              formNotShow={CLIENTE_formNotShow}
            />
          )}
        </Fragment>
      )}
      {typeAdministration === "sucursales" && (
        <Fragment>
          {toDo === "gerenciaList" && (
            <TablaList
              headers={[
                "Gerencia",
                "Detalle",
                "Contacto",
                "Telefono",
                "Email",
                "Referencia",
                "Cliente",
                "Cant_Equipos",
              ]}
              pathCreate="/administrar/sucursales/gerenciatCreate"
              pathUpdate="/administrar/sucursales/gerenciaUpdate"
              fn={mantenerdorGerencia_USER(userSession)}
            />
          )}
          {toDo === "gerenciatCreate" && (
            <Forms
              type="CREATE"
              newFunctionMaster={data => {
                const { finalFunction, stateForm } = data;
                stateForm["Id_Cliente"] = clienteSelected.Id;
                stateForm["idcliente"] = clienteSelected.Id;
                processFunction(finalFunction(userSession, stateForm));
              }}
              functions={[actualizarGerencia_USER, eliminarGerencia_USER]}
              formNotShow={GERENCIA_formNotShow}
              fromBase={GERENCIA_createBase}
            ></Forms>
          )}
          {toDo === "gerenciaUpdate" && (
            <Forms
              type="UPDATE"
              functions={[actualizarGerencia_USER, eliminarGerencia_USER]}
              formNotShow={GERENCIA_formNotShow}
            />
          )}
        </Fragment>
      )}
      {typeAdministration === "areas" && (
        <Fragment>
          {toDo === "areasList" && (
            <TablaList
              headers={["Area", "Cant_Equipos", "Gerencia"]}
              pathCreate="/administrar/areas/areaCreate"
              pathUpdate="/administrar/areas/areaUpdate"
              fn={mantenerdorArea_USER(userSession)}
            />
          )}
          {toDo === "areaCreate" && (
            <Forms
              type="CREATE"
              newFunctionMaster={data => {
                const { finalFunction, stateForm } = data;
                stateForm["codigo"] = stateForm.Codigo;
                let selectedString = document.querySelectorAll(
                  "form .MuiSelect-select em"
                )[0].textContent;
                processFunction(
                  finalFunction(userSession, {
                    ...stateForm,
                    idgerencia: stateGerenciaOtions.find(
                      ({ Gerencia }) => selectedString === Gerencia
                    ).Id,
                  })
                );
              }}
              functions={[actualizarArea_USER, eliminarArea_USER]}
              formNotShow={AREA_formNotShow}
              fromBase={AREA_createBase}
            >
              {stateGerenciaOtions && (
                <SelecteArea stateGerenciaOtions={stateGerenciaOtions} />
              )}
            </Forms>
          )}
          {toDo === "areaUpdate" && stateGerenciaOtions && (
            <Forms
              type="UPDATE"
              newFunctionMaster={data => {
                const { finalFunction, stateForm } = data;
                let selectedString = document.querySelectorAll(
                  "form .MuiSelect-select em"
                )[0].textContent;
                stateForm["idgerencia"] = stateGerenciaOtions.find(
                  ({ Gerencia }) => selectedString === Gerencia
                ).Id;
                stateForm["Gerencia"] = stateGerenciaOtions.find(
                  ({ Gerencia }) => selectedString === Gerencia
                ).Gerencia;
                stateForm["codigo"] = stateForm.Codigo;
                processFunction(finalFunction(userSession, stateForm));
              }}
              functions={[actualizarArea_USER, eliminarArea_USER]}
              formNotShow={AREA_formNotShow}
              Other={props => {
                console.log({ props, stateGerenciaOtions });
                const dafaultValue = stateGerenciaOtions.find(
                  ({ Id }) => Id === props.stateForm.Id_Gerencia
                );
                return (
                  <SelecteArea
                    defaultValue={dafaultValue}
                    stateGerenciaOtions={stateGerenciaOtions}
                  />
                );
              }}
            ></Forms>
          )}
        </Fragment>
      )}
      {typeAdministration === "sectores" && (
        <Fragment>
          {toDo === "sectoresList" && (
            <TablaList
              headers={["Sector", "Cant_Equipos", "Area", "Gerencia"]}
              pathCreate="/administrar/sectores/sectorCreate"
              pathUpdate="/administrar/sectores/sectorUpdate"
              fn={mantenerdorSectores_USER(userSession)}
            />
          )}
          {toDo === "sectorCreate" && (
            <Forms
              type="CREATE"
              newFunctionMaster={data => {
                const { finalFunction, stateForm } = data;
                // let selectedString = document.querySelectorAll("form .MuiSelect-select em")[0].textContent;
                let selectedString2 = document.querySelectorAll(
                  "#areaSelectect-123"
                )[0].textContent;
                processFunction(
                  finalFunction(userSession, {
                    ...stateForm,
                    idarea: JSON.parse(selectedString2).Id,
                  })
                );
              }}
              functions={[actualizarSectores_USER, eliminarSectores_USER]}
              formNotShow={SECTORES_formNotShow}
              fromBase={SECTORES_createBase}
            >
              {stateGerenciaOtions && (
                <SelecteArea
                  show2select={true}
                  stateGerenciaOtions={stateGerenciaOtions}
                />
              )}
            </Forms>
          )}
          {/* Area: "SALADILLO"
              Cant_Equipos: 46
              Id_Gerencia: "GADM"
              Id: 1
              Id_Area: 10
              Id_Gerencia: 1
              Sector: "BODEGA A001" */}
          {toDo === "sectorUpdate" && (
            <Forms
              type="UPDATE"
              newFunctionMaster={data => {
                const { finalFunction, stateForm } = data;
                let selectedString = document.querySelectorAll(
                  "form .MuiSelect-select em"
                )[0].textContent;
                stateForm["Id_Gerencia"] = stateGerenciaOtions.find(
                  ({ Gerencia }) => selectedString === Gerencia
                ).Id;
                stateForm["Gerencia"] = stateGerenciaOtions.find(
                  ({ Gerencia }) => selectedString === Gerencia
                ).Gerencia;

                let selectedString2 = document.querySelectorAll(
                  "#areaSelectect-123"
                )[0].textContent;
                stateForm["Id_Area"] = JSON.parse(selectedString2).Id;
                stateForm["idarea"] = JSON.parse(selectedString2).Id;
                stateForm["Area"] = JSON.parse(selectedString2).Area;

                processFunction(finalFunction(userSession, stateForm));
              }}
              functions={[actualizarSectores_USER, eliminarSectores_USER]}
              formNotShow={SECTORES_formNotShow}
            >
              {stateGerenciaOtions && (
                <SelecteArea
                  show2select={true}
                  stateGerenciaOtions={stateGerenciaOtions}
                />
              )}
            </Forms>
          )}
        </Fragment>
      )}
    </div>
  );
};

// (((((((((((((((((((((())))))))))))))))))))))
const CLIENTE_createBase = {
  Cliente: null,
  Contacto: null,
  Direccion: null,
  Email: null,
  RUT: null,
  Telefono: null,
};
const CLIENTE_formNotShow = ["Cant_Gerencias", "Id", "Codigo"];
// (((((((((((((((((((((())))))))))))))))))))))
const GERENCIA_createBase = {
  Gerencia: null,
  Detalle: null,
  Contacto: null,
  Telefono: null,
  Email: null,
  Referencia: null,
  Id_Cliente: null,
  Cliente: null,
};
const GERENCIA_formNotShow = ["Cant_Equipos", "Id", "Id_Cliente", "Cliente"];
// (((((((((((((((((((((())))))))))))))))))))))
const AREA_createBase = {
  Area: null,
  Codigo: null,
};
const AREA_formNotShow = ["Cant_Equipos", "Id", "Id_Gerencia", "Gerencia"];
// (((((((((((((((((((((())))))))))))))))))))))
const SECTORES_createBase = {
  Sector: null,
};
const SECTORES_formNotShow = [
  "Cant_Equipos",
  "Id",
  "Id_Area",
  "Area",
  "Id_Gerencia",
  "Gerencia",
];
// (((((((((((((((((((((())))))))))))))))))))))
