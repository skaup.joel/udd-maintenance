import React, { useEffect, useState } from "react";
import { Pie } from "react-chartjs-2";
import { colorsArray } from "Generals/css";
import { ExcelDownload } from "components/ExcelDownload";
import { SelectMesAnno } from "./SelectMesAno";
import { excel_cumpliger } from "api";
import { useSelector } from "react-redux";
import { dateSplit } from "Generals";

export default function CantidadEquiposPorCliente(props) {
  const userSession = useSelector((state) => state.sessionReducer.userData.User);
  const [dataRepote2, setstatedataRepote2] = useState(null);
  const [dataCantidadEquipoPorCiente, setsDataCantidadEquipoPorCiente] = useState(null);
  const [dataExcel, setsDataExcel] = useState(null);
  const [mesAnno, setmesAnno] = useState({ anno: dateSplit().anno, mes: dateSplit().mes });

  const hadlerSubmit = (anno, mes) => {
    setmesAnno({ anno, mes });
  };
  useEffect(() => {
    excel_cumpliger(userSession, { gerencias: 0, mes: mesAnno.mes, anno: mesAnno.anno })
      .then((res) => res.json())
      .then((data) => setstatedataRepote2(data))
      .catch((err) => console.log(err));
  }, [mesAnno.anno, mesAnno.mes, userSession]);

  useEffect(() => {
    dataRepote2 &&
      (() => {
        const { Data } = dataRepote2;

        const labels = Object.keys(Data); // Tilutos de cada dato
        const data = labels.map((i) => Data[i].Equipos); // numero o info de cada titutulo
        const colors = colorsArray.map((n, i) => colorsArray[i]);
        const dataFinal = {
          total: data.reduce((a, b) => a + b),
          labels,
          datasets: [
            {
              data,
              backgroundColor: colors,
              hoverBackgroundColor: colors,
            },
          ],
        };
        setsDataCantidadEquipoPorCiente(dataFinal);

        //Data excel
        //headers
        const headers = Object.keys(Data).map((item) => {
          return { label: item, value: item };
        });
        //rows
        let rows = {};
        Object.keys(Data).forEach((item) => {
          rows = { ...rows, [item]: Data[item].Equipos };
        });
        setsDataExcel({ headers, rows: [rows] });
      })();
  }, [dataRepote2]);

  return (
    <div>
      {dataCantidadEquipoPorCiente && dataExcel && (
        <div style={{ width: "30em", height: "25em", textAlign: "center" }}>
          <SelectMesAnno defaultMes={mesAnno.mes} defaultAnno={mesAnno.anno} hadlerSubmit={hadlerSubmit} />
          <h4>Cantidad de equipos por Cliente</h4>
          <h6>Total de equipos {dataCantidadEquipoPorCiente.total}</h6>
          <Pie data={dataCantidadEquipoPorCiente} />
          <ExcelDownload headers={dataExcel.headers} rows={dataExcel.rows} />
        </div>
      )}
    </div>
  );
}
