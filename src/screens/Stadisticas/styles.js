import styled from "styled-components";

export const Container = styled.div`
  display: flex;
  justify-content: center;
  align-items: center;
  flex-direction:column;
`;

export const WrapperCharts = styled.div`
  display: flex;
  width: 100%;
  height:100%;
  justify-content: space-around;
  align-items:center;
`;

export const WrapperText = styled.div`
  display: flex;
  width: 100%;
  height:30%;
  justify-content: space-around;
  text-align:center;
  & > h4 {
    width:50%;
  }
`;
