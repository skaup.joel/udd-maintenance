import React, { useEffect, useState } from "react";
import { ExcelDownload } from "components/ExcelDownload";
import { SelectMesAnno } from "./SelectMesAno";
import { excel_cumpliger } from "api";
import { useSelector } from "react-redux";
import { dateSplit } from "Generals";
const OperatividadSCI = () => {
  const userSession = useSelector((state) => state.sessionReducer.userData.User);
  const [dataRepote2, setstatedataRepote2] = useState(null);
  const [mesAnno, setmesAnno] = useState({ anno: dateSplit().anno, mes: dateSplit().mes });
  const [dataExcel, setdataExcel] = useState(null);

  // const [dataCantidadEquipoPorCiente, setsDataCantidadEquipoPorCiente] = useState(null);
  useEffect(() => {
    excel_cumpliger(userSession, { gerencias: 0, mes: mesAnno.mes, anno: mesAnno.anno })
      .then((res) => res.json())
      .then((data) => setstatedataRepote2(data))
      .catch((err) => console.log(err));
  }, [mesAnno, userSession]);

  useEffect(() => {
    dataRepote2 &&
      (() => {
        const { Data } = dataRepote2;
        const gerencias = Object.keys(Data);
        let rows = [];
        gerencias.forEach((item, i) => {
          const Objet = Data[item];
          rows.push({
            gerencias: item,
            tareasPlanificada: Objet.Planificada.CantEquipos,
            tareasDescargada: Objet.Descargada.CantEquipos,
            tareasAnulada: Objet.Anulada.CantEquipos,
            tareasTerminada: Objet['Terminada validada'].CantEquipos,
            tareasTerminadaSinValidar: Objet["Terminada sin validar"].CantEquipos,
            tareasNoRealizada: Objet["No Realizada"].CantEquipos,
            totalDeEquipos: Objet.Equipos,
          });
        });
        setdataExcel(rows);
      })();
  }, [dataRepote2]);
  const hadlerSubmit = (anno, mes) => {
    setmesAnno({ anno, mes });
  };

  if (!dataExcel) return "";
  return (
    <div>
      <SelectMesAnno defaultMes={mesAnno.mes} defaultAnno={mesAnno.anno} hadlerSubmit={hadlerSubmit} />
      <ExcelDownload headers={header} TextButton={"Descargar Excel General"} rows={dataExcel} />
    </div>
  );
};

export default OperatividadSCI;

const header = [
  { label: "Gerencias", value: "gerencias" },
  { label: "Tareas Planificadas", value: "tareasPlanificada" },
  { label: "Tareas Descargada", value: "tareasDescargada" },
  { label: "Tareas Anulada", value: "tareasAnulada" },
  { label: "Tareas validada", value: "tareasTerminada" },
  { label: "Tareas sin validar", value: "tareasTerminadaSinValidar" },
  { label: "Tareas No Realizada", value: "tareasNoRealizada" },
  { label: "Total de equipos", value: "totalDeEquipos" },
];
