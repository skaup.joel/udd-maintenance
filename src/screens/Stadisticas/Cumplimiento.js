import React, { useEffect, useState } from "react";
import { colorsArray } from "Generals/css";
import { Pie } from "react-chartjs-2";
import { SelectMesAnno } from "./SelectMesAno";
import { excel_cumpliger } from "api";
import { useSelector } from "react-redux";
import { dateSplit } from "Generals";

const Cumplimineto = () => {
  const userSession = useSelector((state) => state.sessionReducer.userData.User);
  const [dataRepote2, setstatedataRepote2] = useState(null);
  const [pies, setstatePies] = useState(null);
  const [pieSelected, setDataPie] = useState(null);
  const [totalEquipos, settotalEquipos] = useState(0);
  const [mesAnno, setmesAnno] = useState({ anno: dateSplit().anno, mes: dateSplit().mes });
  const [reff] = useState(React.createRef());

  useEffect(() => {
    excel_cumpliger(userSession, { gerencias: 0, mes: mesAnno.mes, anno: mesAnno.anno })
      .then((res) => res.json())
      .then((data) => setstatedataRepote2(data))
      .catch((err) => console.log(err));
  }, [mesAnno, userSession]);

  useEffect(() => {
    dataRepote2 &&
      (() => {
        const { Data } = dataRepote2;
        const options = Object.keys(Data);
        let datos = [];
        options.forEach((item, i) => {
          const Objet = Data[item];
          const labels = Object.keys(Objet);
          labels.shift();

          let data = [];
          labels.forEach((item, i) => {
            return data.push(Objet[item].CantEquipos);
          });
          datos.push({
            labels,
            data,
          });
        });

        const datosPies = datos.map(({ labels, data }) => {
          const colors = colorsArray.map((n, i) => colorsArray[i]);
          const dataFinal = {
            labels,
            datasets: [
              {
                data,
                backgroundColor: colors,
                hoverBackgroundColor: colors,
              },
            ],
          };
          return dataFinal;
        });
        const allSelects = options.map((nameOption, i) => {
          return {
            totalEquipos: Data[options[i]].Equipos,
            nameOption,
            datosPie: datosPies[i],
          };
        });

        setstatePies(allSelects);
      })();
  }, [dataRepote2]);

  useEffect(() => {
    pies &&
      (() => {
        setDataPie(JSON.stringify(pies[0]));
        settotalEquipos(pies[0].totalEquipos);
        reff.current.value = JSON.stringify(pies[0]);
      })();
  }, [pies, reff]);

  const hadlerSubmit = (anno, mes) => {
    setmesAnno({ anno, mes });
  };

  return (
    <div style={{ width: "30em", height: "20em", textAlign: "center" }}>
      <SelectMesAnno defaultMes={mesAnno.mes} defaultAnno={mesAnno.anno} hadlerSubmit={hadlerSubmit} />
      {pies && (
        <div style={{ display: "flex" }}>
          <h4 className="mr-3">Cumplimiento de tareas por </h4>
          <select
            name="select"
            ref={reff}
            onChange={(e) => {
              settotalEquipos(JSON.parse(e.target.value).totalEquipos);
              setDataPie(e.target.value);
            }}
          >
            {pies.map((data, i) => (
              <option value={JSON.stringify(data)} selected={i === 0 ? true : false}>
                {data.nameOption}
              </option>
            ))}
          </select>
        </div>
      )}
      {pieSelected && (
        <>
          <Pie data={JSON.parse(pieSelected).datosPie} />
          <span>Total de equipos {totalEquipos}</span>
        </>
      )}
    </div>
  );
};

export default Cumplimineto;
