import React from "react";
import { Container } from "./styles";
import CantidadEquiposPorCliente from "./CantidadEquiposPorCliente";
import OperatividadSCI from "./OperatividadSCI";
import Cumplimiento from "./Cumplimiento";
import ExecelGeneral from "./ExecelGeneral";

export const Stadisticas = (props) => {
  return (
    <Container className="Global_ContainerRoute">
      <div style={{width:'100%', height:'inherit',alignItems:'center', justifyContent:'center',display:'flex'}}>
        <Cumplimiento />
        <CantidadEquiposPorCliente />
        <OperatividadSCI />
      </div>
      <div>
      <ExecelGeneral />
      </div>

    </Container>
  );
};
