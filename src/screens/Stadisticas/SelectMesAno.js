import React, { useState } from "react";
import { Meses, annos } from "Generals";

const mock = { hadlerSubmit: () => null };

export const SelectMesAnno = (props) => {
  props = { ...mock, ...props };
  const { hadlerSubmit, defaultMes = 1, defaultAnno = 2022 } = props;

  const [stateSelectMes, setstateSelectMes] = useState(defaultMes);
  const [stateSelectYear, setstateSelectYear] = useState(defaultAnno);

  const submit = (e) => {
    e.preventDefault();
    let anno = parseInt(stateSelectYear);
    let mes = parseInt(stateSelectMes);
    hadlerSubmit(anno, mes);
  };

  return (
    <form onSubmit={submit}>
      <select onChange={(e) => setstateSelectMes(e.target.value)}>
        {Meses.map(({ mes, number }) => (
          <option selected={number === defaultMes ? true : false} value={number}>
            {mes}
          </option>
        ))}
      </select>

      <select className="m-3" onChange={(e) => setstateSelectYear(e.target.value)}>
        {annos.map(({ anno, number }) => (
          <option selected={number === defaultAnno ? true : false} value={number}>
            {anno}
          </option>
        ))}
      </select>
      <button className="btn btn-primary"> Buscar </button>
    </form>
  );
};
