import React, { useEffect, useState } from "react";
import { colorsArray } from "Generals/css";
import { Pie } from "react-chartjs-2";
import { DatePicker } from "components/DatePicker";
import { rep_operatividad } from "api";
import { useSelector } from "react-redux";
import { dateSplit } from "Generals";

const OperatividadSCI = () => {
  const { dia: diaInitial, mes: mesInitial, anno: annoInitial } = dateSplit();
  const userSession = useSelector((state) => state.sessionReducer.userData.User);
  const [dataRepote2, setstatedataRepote2] = useState(null);
  const [pies, setstatePies] = useState(null);
  const [pieSelected, setDataPie] = useState(null);
  const [fechaFetch, setfechaFetch] = useState(`${annoInitial}-${mesInitial}-${diaInitial}`);
  // const [dataCantidadEquipoPorCiente, setsDataCantidadEquipoPorCiente] = useState(null);
  useEffect(() => {
    rep_operatividad(userSession, { fechaFetch })
      .then((res) => res.json())
      .then((data) => setstatedataRepote2(data))
      .catch((err) => console.log(err));
  }, [fechaFetch, userSession]);

  useEffect(() => {
    dataRepote2 &&
      (() => {
        const { Data } = dataRepote2;
        const options = Object.keys(Data);

        const datos = options.map((item) => {
          const Objet = Data[item];
          const labels = Object.keys(Objet);
          const data = labels.map((i) => Objet[i]);
          return {
            labels,
            data,
          };
        });
        const datosPies = datos.map(({ labels, data }) => {
          const colors = colorsArray.map((n, i) => colorsArray[i]);
          const dataFinal = {
            labels,
            datasets: [
              {
                data,
                backgroundColor: colors,
                hoverBackgroundColor: colors,
              },
            ],
          };
          return dataFinal;
        });
        // setsDataCantidadEquipoPorCiente(datosPies[4]);
        const allSelects = options.map((nameOption, i) => {
          return {
            nameOption,
            datosPie: datosPies[i],
          };
        });

        setstatePies(allSelects);
      })();
  }, [dataRepote2]);

  useEffect(() => {
    pies && setDataPie(JSON.stringify(pies[0].datosPie));
  }, [pies]);

  const setNewFecha = (fetcha) => {
    const año = fetcha.getFullYear();
    const mes = fetcha.getMonth() + 1;
    const dia = fetcha.getDate();
    setfechaFetch(`${año}-${mes}-${dia}`);
  };

  return (
    <div style={{ width: "30em", height: "25em", textAlign: "center" }}>
      <DatePicker fn={setNewFecha} />
      <span>Operatividad del ultimo mes</span>
      {pies && (
        <div style={{ display: "flex" }}>
          <h4 className="mr-3">Operatividad SCI de </h4>
          <select name="select" onChange={(e) => setDataPie(e.target.value)}>
            {pies.map(({ nameOption, datosPie }, i) => (
              <option value={JSON.stringify(datosPie)} selected={i === 0 ? true : false}>
                {nameOption}
              </option>
            ))}
          </select>
        </div>
      )}
      {pieSelected && (
        <>
          <Pie data={JSON.parse(pieSelected)} />
          <p>
            {`SOP: Sistema Operativo.\nSOCO: Sistema Operativo con Observaciones. SNO: Sistema No Operativo. SSR: Sistema sin Revisar. SFS: Sistema Fuera de Servicio., SC: Sin Contestar. SI: Sin Informacion `}{" "}
          </p>
        </>
      )}
    </div>
  );
};

export default OperatividadSCI;
