import styled from 'styled-components'

export const Conatiner = styled.div`
display:flex;
justify-content:flex-start;
flex-direction:column;
align-items:center;
& > h1 {
  padding: 3em;
}
`

export const WrapperButtoons = styled.div`
  display:flex;
  justify-content: space-around;
  width:100%;
  & > div > button {
    width: 20em;
  }
  & > div {
    width: 20em;
    display:flex;
    flex-direction:column;
    justify-content:center;
    align-items:center;
  }
`

