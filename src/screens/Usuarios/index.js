import React from "react";
import { Conatiner, WrapperButtoons } from "./styled";
import { useHistory } from "react-router-dom";
import { ButtonCustom as Button } from "./../../components/ButtonCustom";
import { SelectCustom } from "./../../components/SelectCustom";
import { SnackBars } from "../../components/SnackBars";

export const Usuarios = (props) => {
  let history = useHistory();
  const goTo = (ruta) => history.push(ruta, props.location.state);
  return (
    <Conatiner className="Global_ContainerRoute">
      <h1>Gestor de usuario</h1>
      <WrapperButtoons>
        <div>
          <SelectCustom
            style={{ width: "100%" }}
            handleChange={(data) => goTo(data.rute)}
            label="Administrar Cliente - Area - Sucursal"
            objKey="randomData"
            options={[
              {
                rute: "/administrar/clientes/clientList",
                randomData: "Administrar Clientes",
              },
              {
                rute: "/administrar/sucursales/gerenciaList",
                randomData: "Administrar Gerencias",
              },
              {
                rute: "/administrar/areas/areasList",
                randomData: "Administrar Area",
              },
              {
                rute: "/administrar/sectores/sectoresList",
                randomData: "Administrar sectores",
              },
            ]}
          />
        </div>
        <div>
          <Button onClick={() => goTo("/administrar/usuarios/userList", null)}>
            <span>
              Administrar <br /> Usuarios
            </span>
          </Button>
        </div>
      </WrapperButtoons>
      {props.location.state && <SnackBars {...props.location.state} />}
    </Conatiner>
  );
};
