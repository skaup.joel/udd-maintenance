import React, { useEffect, useState } from "react";
import { Conatiner } from "./styled";
import { useParams } from "react-router-dom";
import { EquipmentList } from "./../../components/EquipmentList";
import { FormCreateEquipment } from "./../../components/FormCreateEquipment";
import { FormUpdateEquipment } from "./../../components/FormUpdateEquipment";
import { GetAllGerenciaAction } from "../../state/actions/allGerencia";
import { GetAreaGeneralAction } from "../../state/actions/areaGeneral";
import { GetAllAreaAction } from "../../state/actions/allArea";
import { useSelector, useDispatch } from "react-redux";
import { TipoEquipoAction } from "../../state/actions/tipoEquipo";
import { GetAllMarcaModeloAction } from "../../state/actions/AllMarcaModelo";
import { GetAllProtocolosAction } from "../../state/actions/allProtocolos";
import { getOneEquitmentDetail } from "../../api";

export const Equipment = (props) => {
  const userSession = useSelector((state) => state.sessionReducer.userData.User);
  const dispatch = useDispatch();

  useEffect(() => {
    GetAllGerenciaAction(dispatch, { userSession });
    GetAreaGeneralAction(dispatch, { userSession });
    GetAllAreaAction(dispatch, { userSession });
    TipoEquipoAction(dispatch, { userSession });
    GetAllMarcaModeloAction(dispatch, { userSession });
    GetAllProtocolosAction(dispatch, { userSession });

    // eslint-disable-next-line react-hooks/exhaustive-deps
  }, []);
  let { toDo, idEquipment = 0 } = useParams();

  const [detailEquitment, setDetailEquitment] = useState(null);

  const getOneEquitment = () => {
    getOneEquitmentDetail(userSession, { id: idEquipment })
      .then((response) => response.json())
      .then((datos) => setDetailEquitment(datos.Data));
  };

  useEffect(() => {
    idEquipment !== 0 && getOneEquitment();
    // eslint-disable-next-line react-hooks/exhaustive-deps
  }, [idEquipment]);

  return (
    <Conatiner className="Global_ContainerRoute">
      {/* <button onClick={() => getOneEquitment()}>getuno</button> */}
      {toDo === "actualizarEquipos" && detailEquitment && <FormUpdateEquipment detailEquitment={detailEquitment}/>}
      {toDo === "crearEquipos" && <FormCreateEquipment />}
      {toDo === "listarEquipos" && <EquipmentList />}
    </Conatiner>
  );
};
