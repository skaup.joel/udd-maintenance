import React from "react";

export function ErrorPage() {
  return (
    <div style={{ ...styles.divContainer }}>
      <h1 style={{ ...styles.text }}>{"404 page not found 😅"}</h1>
    </div>
  );
}
const styles = {
  divContainer: {
    position: "absolute",
    left: 0,
    right: 0,
    top: 0,
    bottom: 0,
    background: "#00000090",
    justifyContent: "center",
    alignItems: "center"
  },
  text: {
    color: "white"
  }
};
