import React, { useState, useEffect } from "react";
import { Switch, Route } from "react-router-dom";
import { LeftNavComponent } from "./LeftNavComponent";
import { Header } from "./Header";
import { getRoutes } from "../../routes";
import { ErrorPage } from "../ErrorPage";
import { LayoutContainer, ContainerRotes } from "./styles";
import { StylesProvider } from "@material-ui/core/styles";
import { listarClientes } from "api";
import { useSelector, useDispatch } from "react-redux";
import { CHANGEUSER } from "state/types/sessionType";
import { ADMIN } from "routes/allRoles";

export const Layout = (props) => {
  const { Perfil } = props.state;
  const routes = getRoutes(Perfil);
  const dispatch = useDispatch();

  const userSession = useSelector((state) => state.sessionReducer.userData.User);
  const newAdminCallApi = useSelector((state) => state.sessionReducer.userData);
  const PerfilSession = useSelector((state) => state.sessionReducer.userData.Perfil);

  const [stateClientesOtions, setstateClienteOptions] = useState(null);
  useEffect(() => {
    listarClientes(userSession).then(({ Data }) => {
      const data = Data.map((item) => {
        const { Id, Cliente } = item;
        return { Id, Cliente };
      });
      if (ADMIN === PerfilSession)
        dispatch({
          type: CHANGEUSER,
          payload: { ...newAdminCallApi, User: `${userSession}&idCliente=${Data[0].Id || 0}` },
        });
      setstateClienteOptions(data);
    });
    // eslint-disable-next-line react-hooks/exhaustive-deps
  }, []);

  return (
    <StylesProvider injectFirst>
      <LayoutContainer>
        <LeftNavComponent routes={routes} />
        <ContainerRotes>
          {stateClientesOtions && (
            <>
              <Header stateClientesOtions={stateClientesOtions} />
              {switchRoutes(routes)}
            </>
          )}
        </ContainerRotes>
      </LayoutContainer>
    </StylesProvider>
  );
};

const switchRoutes = (routes) => (
  <Switch>
    {routes.map((prop, key) => {
      const { component: Componente } = prop;
      return <Route data={"prop"} path={prop.path} component={Componente} exact={prop.exact} key={key}></Route>;
    })}
    <Route component={ErrorPage} />
  </Switch>
);
