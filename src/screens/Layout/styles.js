import styled from "styled-components";
import BottomNavigation from "@material-ui/core/BottomNavigation";
import BottomNavigationAction from "@material-ui/core/BottomNavigationAction";

import { Colors } from "../../Generals/css";
export const LayoutContainer = styled.div`
  display: flex;
  width: 100%;
  height: 100vh;
`;

export const LeftNav = styled.div`
  overflow: auto;
  display: flex;
  flex-direction: column;
  justify-content: space-between;
  align-items: center;
  border-right: 1px solid rgba(0, 0, 0, 0.314);
  background-color: ${Colors.azul};
  width: 120px;
  padding: 3em 0em;
`;

export const BottomNavigationActionStyled = styled(BottomNavigationAction)`
  color: #fafafa;
`;

export const BottomNavigationStyled = styled(BottomNavigation)`
  display: flex;
  flex-direction: column;
  height: 50%;
  background-color: transparent;
  width: 100%;
`;

export const ContainerRotes = styled.div`
  display: flex;
  width: 100%;
  justify-content: space-between;
  align-items: center;
  flex-direction: column;
  background-color: ${Colors.blanco};
  overflow: auto;
  padding: 3em 2em;
`;

export const HeaderPageRouter = styled.div`
  display: flex;
  justify-content: space-between;
  width: 100%;
  color: white;
  & :nth-child(2) {
    display: flex;
  }
`;
