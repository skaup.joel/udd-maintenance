import React from "react";
import { IoMdCloseCircle } from "react-icons/io";
import { LeftNav, BottomNavigationStyled, BottomNavigationActionStyled } from "./styles";
import { useHistory } from "react-router-dom";
import { setTitlePage } from "./../../state/actions/generalLayout";
import { useDispatch } from "react-redux";

export const LeftNavComponent = (props) => {
  let history = useHistory();
  const dispatch = useDispatch();
  const routes = props.routes;
  const [value, setValue] = React.useState(1);

  return (
    <LeftNav>
      <IoMdCloseCircle
        size={30}
        color="white"
        onClick={async () => {
          sessionStorage.clear();
          history.push(`/`);
          window.location.reload();
        }}
      />
      <BottomNavigationStyled
        value={value}
        onChange={(event, newValue) => {
          setValue(newValue);
        }}
        showLabels
      >
        {routes.map(({ BtnPush, isButtonNav, id, path, pathComponent }, i) => {
          if (i === 0) return null;
          if (!isButtonNav) return null;
          return (
            <BottomNavigationActionStyled
              key={path}
              onClick={() => {
                history.push(`${path}`);
                setTitlePage(dispatch, { titlePage: id, pathComponent });
              }}
              label={id}
              icon={<BtnPush size={"2em"} />}
            />
          );
        })}
      </BottomNavigationStyled>
    </LeftNav>
  );
};
