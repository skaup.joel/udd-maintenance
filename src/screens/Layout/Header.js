import React from "react";
import { HeaderPageRouter } from "./styles";
import { useSelector, useDispatch } from "react-redux";
import { SelectCustom } from "components/SelectCustom";
import { setClient } from "state/reducers/ClienteSelected";
import { ADMIN } from "routes/allRoles";
import { CHANGEUSER } from "state/types/sessionType";
export const Header = (props) => {
  const dispatch = useDispatch();
  const { sessionReducer } = useSelector((state) => state);
  const newAdminCallApi = useSelector((state) => state.sessionReducer.userData);
  const userSession = useSelector((state) => state.sessionReducer.userData.User);

  const Perfil = useSelector((state) => state.sessionReducer.userData.Perfil);
  const Cliente = useSelector((state) => state.sessionReducer.userData.Cliente);

  const { userData } = sessionReducer;
  const handleChangeCliente = (clientSelected) => {
    setClient(dispatch, clientSelected);
    dispatch({
      type: CHANGEUSER,
      payload: { ...newAdminCallApi, User: `${userSession.match(/\D+/)[0]}${clientSelected.Id || 0}` },
    });
  };
  return (
    <HeaderPageRouter>
      <div style={{ width: "30%" }}>
        {ADMIN === Perfil && (
          <SelectCustom
            style={{ width: "100%" }}
            defaultValue={props.stateClientesOtions[0]}
            handleChange={handleChangeCliente}
            label={"Selecciona un cliente"}
            objKey={"Cliente"}
            options={props.stateClientesOtions}
          />
        )}
      </div>
      <div>
        <h4 style={{ margin: "0px 1em 0px 0px", color: "#555" }}>{`${userData.Perfil} - ${Cliente}`}</h4>
      </div>
    </HeaderPageRouter>
  );
};
