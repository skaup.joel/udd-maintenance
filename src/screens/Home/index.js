import React, { useEffect } from "react";
import { useHistory } from "react-router-dom";
import { useSelector } from "react-redux";
import { ADMIN, PLANER, ClienteA, ClienteB, ClienteC, Tecnico } from "routes/allRoles";
export const Home = (props) => {
  let history = useHistory();
  const { userData } = useSelector((state) => state.sessionReducer);
  useEffect(() => {
    switch (userData.Perfil) {
      case ADMIN:
        history.push(`/estadisticas`);
        break;
      case PLANER:
        history.push(`/reportes`);
        break;
      case ClienteA:
        history.push(`/reportes`);
        break;
      case ClienteB:
        history.push(`/reportes`);
        break;
      case ClienteC:
        history.push(`/estadisticas`);
        break;
      case Tecnico:
        // history.push(`/estadisticas`);
        break;
      default:
        history.push(`/error`);
        break;
    }
  });
  return <></>;
};
