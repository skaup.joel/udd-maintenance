import styled from "styled-components";

export const Container = styled.div`
  background: rgb(151, 10, 68);
  flex-direction: column;
  display: flex;
  height: 100%;
  padding: 1em 0em;
`;

export const Header = styled.div`
  & > img {
    width: 10em;
    margin: 0em 5em;
  }
`;

export const BodyContainer = styled.div`
  display: flex;
  flex-direction: column;
  justify-content: space-around;
  align-items: center;
  height: 100%;
`;

export const ButtonRole = styled.button`
  margin: 0em 2em;
  width: 13em;
  height: 3em;
  justify-content: center;
  align-items: center;
  color: black;
`;
