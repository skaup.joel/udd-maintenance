import React from "react";
import logo from "../../media/logo.png";
import { LoginForm } from "../../components/LoginForm";
import { Container, Header, BodyContainer } from "./styles";
import { useHistory } from "react-router-dom";

export const Login = (props) => {
  let history = useHistory();
  return (
    <Container>
      <Header>
        <img src={logo} alt="logo" />
      </Header>
      <BodyContainer>
        <div>
          <LoginForm
            submit={(credentials) => {
              sessionStorage.setItem("session", JSON.stringify({ credentials }));
              history.push("/");
            }}
          />
        </div>
      </BodyContainer>
    </Container>
  );
};
