import React, { useEffect, useState } from "react";
import { GetTareaPorFecha, GetTareaPorId } from "api";
import { useSelector } from "react-redux";
import styled from "styled-components";
import { DatePicker } from "components/DatePicker";
import { dateSplit } from "Generals";
const WrappDetailFicha = styled.div`
  width: inherit;
  padding: 1em;
  height: inherit;
  overflow: auto;

  .wrapper-ficha-capitulo {
    display: flex;
    flex-wrap: wrap;
  }
  .call_respuesta {
    width: 33%;
  }
`;
const WrapperListDoneWork = styled.div`
  height: inherit;
  overflow: auto;
  .one-work-done {
    cursor: pointer;
  }
`;
const { anno } = dateSplit();
export const TareasRealizadas = () => {
  const getFormat = (date = new Date()) =>
    `${date.getFullYear()}-${date.getMonth() + 1}-${date.getDate()}`;
  const userSession = useSelector(state => state.sessionReducer.userData.User);

  const [stateDataSelected, setDataSelected] = useState(getFormat(new Date()));
  const [stateWorksFinded, setWorksFinded] = useState([]);
  const [opionPrint, setStateOpionPrint] = useState(null);

  const [selectedId, setSelectedId] = useState(null);
  const [detailWork, setDetailWork] = useState([]);
  const [imgs, setImgs] = useState([]);
  useEffect(() => {
    selectedId &&
      GetTareaPorId(userSession, { id_tarea: selectedId }).then(
        ({ Data, Imagenes }) => {
          setDetailWork(Data);
          setImgs(Imagenes);
        }
      );
  }, [selectedId, userSession]);

  useEffect(() => {
    setWorksFinded([]);
    setDetailWork([]);
    GetTareaPorFecha(userSession, {
      fecha: stateDataSelected,
    }).then(({ Data }) => setWorksFinded(Data));
  }, [stateDataSelected, userSession]);
  const handlerPrint = () => {
    const elem1 =
      document.querySelector(`#id-select-header-print${opionPrint}`) || ``;
    const elem2 =
      document.querySelector(`#id-select-body-print${opionPrint}`) || "";
    var mywindow = window.open("", "PRINT", "height=400,width=600");
    mywindow.document.write("<html><head><title>Sercoing</title>");
    mywindow.document.write("</head><body >");
    mywindow.document.write(elem1.innerHTML || "no entrodado 1");
    mywindow.document.write(elem2.innerHTML || "no entrodado 2");
    imgs.forEach(img => {
      mywindow.document.write(
        `<img width='350px' src=${img} alt="imgen de tarea"></img>`
      );
    });
    mywindow.document.write("</body></html>");
    mywindow.document.close(); // necessary for IE >= 10
    mywindow.focus(); // necessary for IE >= 10*/
    mywindow.print();
  };
  return (
    <div
      style={{
        display: "flex",
        justifyContent: "space-evenly",
        alignItems: "center",
      }}
      className="Global_ContainerRoute"
    >
      <WrapperListDoneWork>
        <DatePicker
          defaultData={new Date(anno, 7, 4)}
          // defaultData={new Date(anno, mes, dia)}
          fn={data => {
            JSON.stringify(data) !== "null" && setDataSelected(getFormat(data));
          }}
        />
        {stateWorksFinded.map((tarea, i) => {
          return (
            <div
              id={`id-select-header-print${tarea.Id}`}
              className="one-work-done"
              onClick={() => {
                setStateOpionPrint(`${tarea.Id}`);
                setSelectedId(tarea.Id);
              }}
              style={{
                border: `${
                  stateWorksFinded.map(item => item.Id).indexOf(selectedId) ===
                  i
                    ? "solid red 1px"
                    : "transparent"
                }`,
                display: "flex",
                flexDirection: "column",
                width: "fit-content",
                margin: "1em 0em 2em 0em",
              }}
            >
              {
                <React.Fragment>
                  <div>{`Usuario:  ${tarea.Usuario}`}</div>
                  <div>{`Protocolo:  ${tarea.Protocolo}`}</div>
                  <div>{`Equipo:  ${tarea.Equipo}`}</div>
                  <div>{`Id:${tarea.Id}`}</div>
                  <div>{`(${tarea.TipoTarea})`}</div>
                </React.Fragment>
              }
            </div>
          );
        })}
      </WrapperListDoneWork>
      <WrappDetailFicha id={`id-select-body-print${opionPrint}`}>
        {[...new Set(detailWork.map(item => item.NCapitulo))]
          .map(capitulo => {
            return detailWork.filter(({ NCapitulo }) => capitulo === NCapitulo);
          })
          .map((primerNvl, i) => {
            return (
              <React.Fragment>
                <h4 style={{ margin: "1em 0em 0em 0em" }}>
                  {primerNvl[0].DescCapitulo}{" "}
                </h4>
                <div className="wrapper-ficha-capitulo">
                  {primerNvl.map(segundiNvl => {
                    return (
                      <div className="call_respuesta">{`${segundiNvl.DescCorrelativo}: ${segundiNvl.Respuesta}`}</div>
                    );
                  })}
                </div>
              </React.Fragment>
            );
          })}
      </WrappDetailFicha>
      <div
        style={{
          width: "10%",
          display: "flex",
          flexDirection: "column",
          padding: ".1em",
          alignItems: "center",
          height: "inherit",
          overflow: "auto",
        }}
      >
        {opionPrint && (
          <button
            className="btn btn-small btn-primary m-1"
            onClick={handlerPrint}
          >
            Imprimir
          </button>
        )}
        {imgs.length !== 0 && (
          <>
            {imgs.map((url, i) => (
              <button
                className="btn btn-small btn-success m-1"
                onClick={() => window.open(url)}
              >
                imagen {i + 1}
              </button>
            ))}
          </>
        )}
      </div>
    </div>
  );
};
