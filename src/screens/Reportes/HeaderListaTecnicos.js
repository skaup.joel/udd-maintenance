import React from "react";
import styled from "styled-components";
import { DatePicker } from "../../components/DatePicker";
import { ModalModificarTarea } from "./ModalModificarTarea";
import { dateSplit } from "Generals";
const { dia, mes, anno } = dateSplit();
const Container = styled.div`
  width: inherit;
  display: flex;
  justify-content: space-between;
  height: 4em;
  align-items: center;
  & .MuiFormControl-root {
    margin-left: 1em;
  }
`;

export const HeaderListaTecnicos = (props) => {
  const getFormat = (date = new Date()) => `${date.getFullYear()}-${date.getMonth() + 1}-${date.getDate()}`;

  return (
    <Container>
      <div>
        <span>Desde:</span>
        <DatePicker
          defaultData={new Date(anno, mes - 2, dia)}
          fn={(data) => {
            JSON.stringify(data) !== "null" && props.setDataSelectedDesde(getFormat(data));
          }}
        />
        <span style={{ marginLeft: "2em" }}>Hasta:</span>
        <DatePicker
          defaultData={new Date(anno, mes - 1, dia)}
          fn={(data) => {
            JSON.stringify(data) !== "null" && props.setDataSelectedHasta(getFormat(data));
          }}
        />
        <button className="btn btn-primary ml-4" onClick={props.getData}>
          Buscar
        </button>
      </div>
      {props.stateTareaSelected && props.stateTareaSelected.Id_Estado === 1 && <ModalModificarTarea itemSelected={props.stateTareaSelected} allTecnicos={props.dataListTecnicos} />}
    </Container>
  );
};
