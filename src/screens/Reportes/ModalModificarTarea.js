import React, { useState, useEffect } from "react";
import { Modal } from "../../components/Modal";
import { DatePicker } from "../../components/DatePicker";
import WithPromises from "../../components/ReactSelect";
import { modificarTarea } from "../../api";
import { useSelector } from "react-redux";

export const ModalModificarTarea = props => {
  const userSession = useSelector(state => state.sessionReducer.userData.User);

  const { itemSelected, allTecnicos } = props;
  const [stateDate, setstateDate] = useState(null);
  const [stateTecnicos, setstateTecnicos] = useState(null);
  useEffect(() => {
   console.log({stateTecnicos});
  }, [stateTecnicos])
  const getFormat = (date = new Date()) =>
    `${date.getFullYear()}-${date.getMonth() + 1}-${date.getDate()}`;
  const fnModificarTarea = async () => {
    modificarTarea(userSession, {
      idTarea: itemSelected["Id"],
      fecha: stateDate,
      idTecnicos: [stateTecnicos.Id]//`${stateTecnicos.map(item => item.Id)}`,
    });
  };
  return (
    <React.Fragment>
      <Modal
        className="btn btn-success"
        textButton="Modificar"
        ModalTitle={`Modificando Tarea Asignada`}
        TextToDoBtn="Realizar Cambios"
        fn={fnModificarTarea}
      >
        <h4>Información:</h4>
        <h6>{` -Fecha asignada: ${itemSelected.Fecha}`}</h6>
        <h6>{` -Usuario: ${itemSelected.Usuario}`}</h6>
        <h6>{` -Equipo: ${itemSelected.Equipo}`}</h6>
        <h6
          style={{
            display: " flex",
            paddingTop: "1em",
            alignItems: "center",
            justifyContent: "space-between",
          }}
        >
          {` -Asignar nueva fecha:`}
          <DatePicker
            defaultData={new Date()}
            fn={data => {
              JSON.stringify(data) !== "null" && setstateDate(getFormat(data));
            }}
          />
        </h6>
        <h6
          style={{
            display: " flex",
            paddingTop: " 1em",
            alignItems: " center",
            justifyContent: " space-between",
          }}
        >
          {` -Asignar nuevo tecnico(s):`}
          <WithPromises
            fn={datos => setstateTecnicos(datos)}
            options={allTecnicos.Data.map(item => {
              return { ...item, label: item.Nombre, value: item.Id };
            })}
            defaultOptions={allTecnicos.Data.map(item => {
              return { ...item, label: item.Nombre, value: item.Id };
            })}
            placeholder={"Selecciona Equipo(s)"}
          />
        </h6>
      </Modal>
    </React.Fragment>
  );
};
