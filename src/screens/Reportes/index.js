import React, { useState, useEffect } from "react";
import { TareasRealizadas } from "./TareasRealizadas";
import { AdminTarea } from "./AdminTarea";
import { ClienteA, ClienteB, ClienteC } from "routes/allRoles";
import { useSelector } from "react-redux";
import { useHistory } from "react-router-dom";
export const Reportes = props => {
  const history = useHistory();
  const Perfil = useSelector(state => state.sessionReducer.userData.Perfil);
  const [state, setstate] = useState(null);
  useEffect(() => {
    if (ClienteA === Perfil || ClienteB === Perfil || ClienteC === Perfil) {
      setstate("TareasRealizadas");
    }
  }, [Perfil]);
  return (
    <div
      style={{
        display: "flex",
        justifyContent: "space-evenly",
        alignItems: "center",
        height: "100%",
        width: "100%",
      }}
    >
      {!state && (
        <>
          <button
            className="btn btn-outline-secondary"
            onClick={() => {
              // setstate("AdminTarea");
              history.push("/reportes/AdminTarea");
            }}
          >
            Administrar tareas
          </button>
          <button
            className="btn btn-outline-secondary"
            onClick={() => {
              // setstate("TareasRealizadas");
              history.push("/reportes/TareasRealizadas");
            }}
          >
            Ver Tareas Realizadas
          </button>
        </>
      )}
      {state === "AdminTarea" && <AdminTarea />}
      {state === "TareasRealizadas" && <TareasRealizadas />}
    </div>
  );
};
