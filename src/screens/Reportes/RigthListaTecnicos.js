import React from "react";
import styled from "styled-components";

const Container = styled.div`
  width: 100%;
  overflow: auto;
  display: flex;
  flex-wrap: wrap;
  justify-content: space-evenly;
`;

const CardStyled = styled.div`
  margin: 1em;
  display: flex;
  flex-direction: column;
  background: ${({ editable }) => (!editable ? "#2B76D250" : "#f6f6f6")};
  cursor: ${({ editable }) => (!editable ? "pointer" : "no-drop")};
  border-radius: 5px;
  padding: 10px;
  width: 25%;
  max-height: 20em;
`;

export const RigthListaTecnicos = (props) => {
  const habdlerClick = (item) => {
    props.setStateTareaSelected(item);
  };
  return (
    <Container>
      {props.stateTareasObtenidas.map((item) => {
        const { Id_Estado } = item;
        return (
          <CardStyled onClick={() => habdlerClick(item)} editable={Id_Estado !== 1 ? true : false}>
            {/* Imprimir todos los datos del JSON */}
            {Object.keys(item).map((i) => ([ "Id_Protocolo", "Id_Equipo", "Id_Estado"].find((item) => i === item) ? "" : <span>{`${i}: ${item[i]}`}</span>))}
            {/*Fin de => Imprimir todos los datos del JSON */}
          </CardStyled>
        );
      })}
      {props.stateTareasObtenidas.length === 0 && <h1>No hay datos</h1>}
    </Container>
  );
};
