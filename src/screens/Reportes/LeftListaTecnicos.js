import React, { useState } from "react";
import styled from "styled-components";

const ToggleDivColor = (props) => {
  const [state, setstate] = useState(false);
  const handlerFunction = () => {
    setstate(!state);
    props.onClick();
  };

  return (
    <div onClick={handlerFunction} style={{ border: `${state ? " solid lime 1px" : "none"}` }}>
      {props.children}
    </div>
  );
};

const Const = styled.div`
  width: auto;
  overflow: auto;
`;

export const LeftListaTecnicos = (props) => {
  return (
    <Const>
      <h6>Lista de Tecnicos</h6>
      {props.dataListTecnicos.Data.map((item) => {
        return (
          <ToggleDivColor
            onClick={() => {
              props.addOrRemoveTecnicoArray(item.Id);
            }}
          >
            <div style={{ padding: ".5em 1em", display: "flex", flexDirection: "column" }}>
              <span>{`${"Nombre"}: ${item["Nombre"]}`}</span>
            </div>
          </ToggleDivColor>
        );
      })}
    </Const>
  );
};
