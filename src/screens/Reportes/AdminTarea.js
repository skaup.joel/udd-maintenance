import React, { useEffect, useState } from "react";
import styled from "styled-components";
import { useSelector } from "react-redux";
import { ListarTecnicos2, listarTareasPorFechasyTecnicos } from "../../api";
import { useHandlerApiFunction } from "../../hooks/stateCalled";
import { RigthListaTecnicos } from "./RigthListaTecnicos";
import { LeftListaTecnicos } from "./LeftListaTecnicos";
import { HeaderListaTecnicos } from "./HeaderListaTecnicos";
import { Spiner } from "./../../components/Spiner";

const Container = styled.div`
  width: 100%;
  height: inherit;
  .adminTareas-body {
    height: inherit;
    display: flex;
    width: 100%;
  }
`;

export const AdminTarea = () => {
  const userSession = useSelector((state) => state.sessionReducer.userData.User);

  const [stateDataSelectedDesde, setDataSelectedDesde] = useState(null);
  const [stateDataSelectedHasta, setDataSelectedHasta] = useState(null);
  const [stateTecnicosIdSelect, setStateTecnicosIdSelect] = useState([]);

  const [stateTareasObtenidas, setStateTareasObtenidas] = useState([]);

  const [stateTareaSelected, setStateTareaSelected] = useState(null);

  const getData = () => {
    if (stateDataSelectedHasta && stateDataSelectedDesde && stateTecnicosIdSelect) {
      const data = async () => {
        const res = await listarTareasPorFechasyTecnicos(userSession, { stateDataSelectedHasta, stateDataSelectedDesde, tecnicos: `${stateTecnicosIdSelect}` });
        setStateTareasObtenidas(res.Data);
      };
      data();
    }
  };

  const [errorListTecnicos, dataListTecnicos, isLoadingListTecnicos] = useHandlerApiFunction(ListarTecnicos2(userSession));
  useEffect(() => {
  }, [stateDataSelectedHasta, stateDataSelectedDesde, stateTecnicosIdSelect]);

  const addOrRemoveTecnicoArray = (Id) => {
    const index = stateTecnicosIdSelect.indexOf(Id);
    let stadoArray = [...stateTecnicosIdSelect];
    if (index > -1) {
      stadoArray.splice(index, 1);
    } else {
      stadoArray.push(Id);
    }
    setStateTecnicosIdSelect(stadoArray);
  };
  if (isLoadingListTecnicos) return <Spiner />;
  if (errorListTecnicos) return <h1>Error con el sevidor</h1>;
  return (
    <div
    style={{
      display: "flex",
      justifyContent: "space-evenly",
      alignItems: "center",
    }}
    className="Global_ContainerRoute"
  >
      {dataListTecnicos && (
        <Container>
          <HeaderListaTecnicos
            stateTareaSelected={stateTareaSelected}
            dataListTecnicos={dataListTecnicos}
            getData={getData}
            setDataSelectedHasta={setDataSelectedHasta}
            setDataSelectedDesde={setDataSelectedDesde}
          />
          <div className="adminTareas-body">
            <LeftListaTecnicos dataListTecnicos={dataListTecnicos} addOrRemoveTecnicoArray={addOrRemoveTecnicoArray} />
            <RigthListaTecnicos setStateTareaSelected={setStateTareaSelected} stateTareasObtenidas={stateTareasObtenidas} />
          </div>
        </Container>
      )}
    </div>
  );
};
