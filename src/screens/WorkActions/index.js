import React, { useEffect } from "react";
import { UserFormCreate } from "../../components/UserFormCreate";
import { AssignWork } from "../../components/AssignWork";
import { TipoEquipoAction } from "../../state/actions/tipoEquipo";

import { TiposProtocolosAction } from "../../state/actions/tiposProtocolos";
import { GetAllGerenciaAction } from "../../state/actions/allGerencia";
import { GetAreaGeneralAction } from "../../state/actions/areaGeneral";
import { GetAllAreaAction } from "../../state/actions/allArea";
import { GetAllProtocolosAction } from "../../state/actions/allProtocolos";

import { GetEquiposParaAsignarTareasAction } from "../../state/actions/EquiposParaAsignarTareas";
import { useSelector, useDispatch } from "react-redux";

export const WorkActions = (props) => {
  const userSession = useSelector((state) => state.sessionReducer.userData.User);
  const dispatch = useDispatch();
  useEffect(() => {
    GetAllGerenciaAction(dispatch, { userSession });
    GetAreaGeneralAction(dispatch, { userSession });
    GetAllAreaAction(dispatch, { userSession });
    
    TiposProtocolosAction(dispatch, { userSession });
    GetAllProtocolosAction(dispatch, { userSession });

    TipoEquipoAction(dispatch, { userSession });
    GetEquiposParaAsignarTareasAction(dispatch, { userSession });
    // eslint-disable-next-line react-hooks/exhaustive-deps
  }, []);

  const toDo = props.match.params.toDo;
  return (
    <div className="Global_ContainerRoute">
      {toDo === "assignwork" && <AssignWork />}
      {toDo === "createWork" && <UserFormCreate />}
    </div>
  );
};
