import React, { useEffect } from "react";
import { Login } from "../screens/Login";
import { Layout } from "../screens/Layout";
import { Router } from "react-router";
import { useDispatch } from "react-redux";
import { createBrowserHistory } from "history";
import { useSelector } from "react-redux";
import * as actions from "../state/actions/sessionAction";
import { GlobalStyle } from "./../Generals/css/GlobalStyledComponents";
const history = createBrowserHistory();
const credentials = sessionStorage.getItem("session") && JSON.parse(sessionStorage.getItem("session")).credentials;

export const App = () => {
  const { userData } = useSelector((state) => state.sessionReducer);
  const dispatch = useDispatch();
  useEffect(() => {
    credentials && actions.login({ dispatch, credentials });
  }, [dispatch]);

  return (
    <Router history={history}>
      <GlobalStyle />
      {userData ? <Layout state={userData} /> : <Login />}
    </Router>
  );
};
